#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/variables.sh

function generate_environment_file () {
    if [ -z "$ENV_JSON" ] ; then
        echo "Error ENV_JSON is not set" >&2
        return 1
    fi
    curr_env=$(cat $ENV_JSON)
    if [ -z "$PULSAR_PORT" ] ; then
        echo "Error PULSAR_PORT is not set, using default" >&2
        PULSAR_PORT=6650
    fi
    if [ -z "$PULSAR_HOST" ] ; then
        echo "Error PULSAR_HOST is not set" >&2
        PULSAR_HOST="localhost"
    fi
    if [ -z "$PULSAR_USER" ] ; then
        echo "Error PULSAR_USER is not set" >&2
        PULSAR_USER="pulsar"
    fi
    if [ -z "$MONGO_HOST" ] ; then
        echo "Error MONGO_HOST is not set" >&2
        MONGO_HOST="localhost"
    fi
    if [ -z "$MONGO_PORT" ] ; then
        echo "Error MONGO_PORT is not set, using default" >&2
        MONGO_PORT=27017
    fi
    if [ -z "$MONGO_USER" ] ; then
        echo "Error MONGO_USER is not set" >&2
        return 1
    fi
    if [ -z "$MONGO_PASSWORD" ] ; then
        echo "Error MONGO_PASSWORD is not set" >&2
        return 1
    fi
    if [ -z "$HUNTDOWN_VAR" ] ; then
        echo "Error HUNTDOWN_VAR is not set, using default" >&2
        HUNTDOWN_VAR=/var/huntdown
    fi

    echo ""
    if [ ! -d "$HUNTDOWN_CONFIG" ] ; then
        $sudo mkdir $HUNTDOWN_CONFIG
    fi
    
    echo ""
    print_header Generating environment file
    curr_env=${curr_env//'%MONGO_HOST%'/$MONGO_HOST}
    curr_env=${curr_env//'%MONGO_PORT%'/$MONGO_PORT}
    curr_env=${curr_env//'%MONGO_USER%'/$MONGO_USER}
    curr_env=${curr_env//'%MONGO_PASSWORD%'/$MONGO_PASSWORD}
    curr_env=${curr_env//'%PULSAR_USER%'/$PULSAR_USER}
    curr_env=${curr_env//'%PULSAR_HOST%'/$PULSAR_HOST}
    curr_env=${curr_env//'%PULSAR_PORT%'/$PULSAR_PORT}
    curr_env=${curr_env//'%HUNTDOWN_VAR%'/$HUNTDOWN_VAR}
    curr_env=${curr_env//'%GITLABTOKEN'/$GITLABTOKEN}
    curr_env=${curr_env//'%GITLABPASS'/$GITLABPASS}
    curr_env=${curr_env//'%DOCKERHOST%'/$DOCKERHOST}
    curr_env=${curr_env//'%DOCKERSSHKEY%'/$DOCKERSSHKEY}
    curr_env=${curr_env//'%DOCKERUSER%'/$DOCKERUSER}

    echo "$curr_env" | $sudo tee $HUNTDOWN_CONFIG/environment.json > /dev/null
    $sudo chmod o-rwx $HUNTDOWN_CONFIG/environment.json
    if [ "$1" != "aws" ] ; then
        $sudo chown "$HUNTDOWN_USER:$HUNTDOWN_USER" $HUNTDOWN_CONFIG/environment.json
    fi

    export MONGO_USER
    export MONGO_HOST
    export MONGO_PASSWORD
    export MONGO_PORT
    export PULSAR_USER
    export PULSAR_HOST
    export PULSAR_PORT
    export GITLABTOKEN
    export GITLABPASS
    export DOCKERHOST
    export DOCKERSSHKEY
    export DOCKERUSER
}
