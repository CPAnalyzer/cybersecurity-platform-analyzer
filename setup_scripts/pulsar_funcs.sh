#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/variables.sh

function start_pulsar_docker () {
    cat "utils/pulsar-standalone/systemd/$PULSAR_UNIT_NAME.in" | $sudo tee "/lib/systemd/system/$PULSAR_UNIT_NAME" > /dev/null

    print_header Starting Pulsar...
    $sudo systemctl enable "$PULSAR_UNIT_NAME" >> "$LOG_FILE" 2>&1
    $sudo systemctl start "$PULSAR_UNIT_NAME" >> "$LOG_FILE" 2>&1

    #Pulsar healthcheck
    until [[ $($sudo docker exec pulsar-service /pulsar/bin/pulsar-admin broker-stats destinations 2> /dev/null) ]]
        do
            sleep 1
        done
    echo "Pulsar running successfully!"
}

function install_pulsar () {
    if [ ! -d "$APACHE_PULSAR_PREFIX" ] ; then
        print_header Installing pulsar
        wget -q $PULSAR_SERVER_URL -O - 2>> "$LOG_FILE" | $sudo tar -C /usr/local -xzf -
        $sudo apt install default-jre-headless -y >> "$LOG_FILE" 2>&1
    else
        print_header "Skipping pulsar install"
    fi
}

function configure_pulsar () {
    print_header Configuring Pulsar
    if ! getent passwd "$PULSAR_USER" > /dev/null 2>&1 ; then
        $sudo useradd -r -s /bin/false "$PULSAR_USER"
    fi
    sed s~%APACHE_PULSAR_PREFIX%~${APACHE_PULSAR_PREFIX}~ < "utils/pulsar-standalone/systemd/$PULSAR_UNIT_NAME.in" | \
        sed s~%PULSAR_USER%~${PULSAR_USER}~ | \
        sed s~%PULSAR_GROUP%~${PULSAR_GROUP}~ | \
        sed s~%PULSAR_LOG_DIR%~${PULSAR_LOG_DIR}~ | \
        $sudo tee "/lib/systemd/system/$PULSAR_UNIT_NAME" > /dev/null
    if [ ! -d "$PULSAR_LOG_DIR" ] ; then
        $sudo mkdir -p "$PULSAR_LOG_DIR"
    fi
    sed "s~%PULSAR_LOG_DIR%~${PULSAR_LOG_DIR}~" < config/pulsar/log4j2.yaml.in | \
        $sudo tee "$APACHE_PULSAR_PREFIX/conf/log4j2.yaml" > /dev/null
    $sudo mkdir -p "$APACHE_PULSAR_PREFIX/logs"
    $sudo chown $PULSAR_USER:$PULSAR_USER "$PULSAR_LOG_DIR" -R
    $sudo chown $PULSAR_USER:$PULSAR_USER "$APACHE_PULSAR_PREFIX/logs" -R

    if [ ! -d "$PULSAR_DATA_DIR" ] ; then
        $sudo mkdir -p "$PULSAR_DATA_DIR"
    fi
    $sudo chown $PULSAR_USER:$PULSAR_USER "$PULSAR_DATA_DIR" -R

    print_header Starting Pulsar...
    $sudo systemctl enable "$PULSAR_UNIT_NAME" >> "$LOG_FILE" 2>&1
    $sudo systemctl start "$PULSAR_UNIT_NAME" >> "$LOG_FILE" 2>&1

    until [[ $($APACHE_PULSAR_PREFIX/bin/pulsar-admin broker-stats destinations 2> /dev/null) ]]
        do
            sleep 1
        done
    echo "Pulsar running successfully!"
}

tryInstallPulsarNodejs(){
    if dpkg -l "$PULSAR_CLIENT" > /dev/null 2>&1 ; then
        print_header Installing Pulsar Nodejs Library
        PULSAR_CLIENT="apache-pulsar-client.deb"
        PULSAR_CLIENT_DEV="apache-pulsar-client-dev.deb"
        { wget -q "$PULSAR_BASE_URL/DEB/$PULSAR_CLIENT" -O "/tmp/$PULSAR_CLIENT" ; \
          wget -q "$PULSAR_BASE_URL/DEB/$PULSAR_CLIENT_DEV" -O "/tmp/$PULSAR_CLIENT_DEV"; \
          $sudo dpkg -i "/tmp/$PULSAR_CLIENT" "/tmp/$PULSAR_CLIENT_DEV" ; \
          rm -f "/tmp/$PULSAR_CLIENT" "/tmp/$PULSAR_CLIENT_DEV"; } >> "$LOG_FILE" 2>&1
    fi
}

function get_pulsar_data() {
    if [ -z "$PULSAR_USER" ] ; then
        PULSAR_USER="pulsar"
    fi
    if [ -z "$PULSAR_HOST" ] ; then
        if [ "$1" == "aws" ] ; then
            PULSAR_HOST="54.210.98.173"
        else
            PULSAR_HOST="localhost"
        fi
    fi
    if [ -z "$PULSAR_PORT" ] ; then
        PULSAR_PORT="6650"
    fi
}
