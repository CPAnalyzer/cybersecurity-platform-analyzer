#!/bin/bash

echo "[!] START HUNTDOWN services..."

# list of services in the good order
# services=("mongod" "pulsar-standalone" "huntdown-database" "huntdown-instantiation" "huntdown-attack-manager" "huntdown-workflow")
services=("pulsar-standalone" "huntdown-database" "huntdown-instantiation" "huntdown-workflow")

for service in "${services[@]}"; do
    echo "[*] Starting service: $service"
    sudo systemctl start "$service"

    # wait for service to be fully loaded and active
    while true; do
        # check if service is active
        active=$(systemctl is-active "$service")

        if [[ $active == "active" ]]; then
            echo "[+] Running: $service"
            # break to start next service
            break
        else
            echo "[*] Waiting for: $service"
            # wait before next check
            sleep 3
        fi
    done
done

echo "[*] Starting service: pm2 backend"

cd /var/huntdown/ui/backend/
sudo -u huntdown pm2 start --cwd /var/huntdown/ui/backend/ --name backend npm -- run start-backend

sleep 3

echo "[*] Starting service: pm2 frontend"

cd /var/huntdown/ui
sudo -u huntdown pm2 start --cwd /var/huntdown/ui/ --name frontend npm -- run start-frontend

sleep 2

echo "[+] All services running!"
