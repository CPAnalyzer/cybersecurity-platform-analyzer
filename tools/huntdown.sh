#!/bin/bash

# define services and environment
PULSAR_SERVICE="pulsar-standalone"
HUNTDOWN_SERVICES="huntdown-database huntdown-instantiation huntdown-attack-manager huntdown-workflow"
# /usr/local/bin/pm2
PM2_BINARY="/usr/bin/pm2"
UI_SERVICES="Frontend Backend"
ENVIRONMENT_FILE="/etc/huntdown/environment.json"
HUNTDOWN_USER="huntdown"

# helper functions for colored output
function print_code {
    echo -n "[*] "
    echo -e "\033[93m$*\033[0m"
}

function print_ok {
    echo -n "[OK] "
    echo -e "\033[32m$*\033[0m"
}

function print_nok {
    echo -n "[NOK] "
    echo -e "\033[31m$*\033[0m Exited: $1."
}

# get huntdown user from environment file
function get_huntdown_user {
    if [ ! -z $HUNTDOWN_USER ] ; then
        return
    fi
    if [ -f "$ENVIRONMENT_FILE" ] ; then
        HUNTDOWN_USER=$(sudo cat "$ENVIRONMENT_FILE" | jq .huntdown_user | sed s/\"//g)
    fi
}

# check and print service status
function check_service_status {
    service="$1"
    systemctl is-active "$service" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        print_ok "$service active"
    else
        print_nok "$?"
    fi
}

# start a service with status check
function start_service {
    service="$1"
    print_code "Starting $service..."
    systemctl start "$service"
    # to be adjusted
    sleep 5 
    check_service_status "$service"
}

# stop a service with status check
function stop_service {
    service="$1"
    print_code "Stopping $service..."
    systemctl stop "$service"
    # to be adjusted
    sleep 2
    check_service_status "$service"
}

# check if PM2 is installed
function check_pm2_installed {
    if [ ! -f $PM2_BINARY ]; then
        print_nok "$?" "PM2 Binary not found."
        exit 1
    fi
}

# start or stop PM2 services
function pm2_manage_services {
    operation="$1"
    get_huntdown_user
    check_pm2_installed

    if [ "$operation" == "start" ]; then
        # for service in $UI_SERVICES; do
        print_code "Starting PM2 service: $service..."
        sudo -u $HUNTDOWN_USER $PM2_BINARY start --name "$service"
        check_service_status "$service"
        # done
    elif [ "$operation" == "stop" ]; then
        print_code "Stopping all PM2 services..."
        sudo -u $HUNTDOWN_USER $PM2_BINARY delete all
        print_ok "All PM2 services stopped."
    fi
}

# switch for command line argument
case "$1" in
    "on")
        echo "[!] START HuntDown..."
        start_service "mongod"
        start_service $PULSAR_SERVICE
        for service in $HUNTDOWN_SERVICES; do
            start_service "$service"
        done
        pm2_manage_services "start"
        ;;
    "off")
        echo "[!] STOP HuntDown..."
        pm2_manage_services "stop"
        for service in $HUNTDOWN_SERVICES; do
            stop_service "$service"
        done
        stop_service $PULSAR_SERVICE
        stop_service "mongod"
        ;;
    "check")
        echo "[*] Checking all services..."
        check_service_status "mongod"
        check_service_status $PULSAR_SERVICE
        for service in $HUNTDOWN_SERVICES; do
            check_service_status "$service"
        done
        ;;
    *)
        echo "Invalid argument. Please use 'on', 'off', or 'check'."
        ;;
esac
