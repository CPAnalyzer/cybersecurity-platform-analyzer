#!/bin/bash

echo "[!] CHECK HUNTDOWN services..."

# list of services
services=("mongod" "pulsar-standalone" "huntdown-database" "huntdown-instantiation" "huntdown-attack-manager" "huntdown-workflow")

for service in "${services[@]}"; do
    state=$(systemctl is-active "$service")

    # display state
    if [[ $state == "active" ]]; then
        echo "[+] active: $service"
    elif [[ $state == "failed" ]]; then
        echo "[-] failed: $service"
    else
        echo "[-] inactive: $service"
    fi
done

echo "[*] Checking pm2 services:"
sudo -u huntdown pm2 list
