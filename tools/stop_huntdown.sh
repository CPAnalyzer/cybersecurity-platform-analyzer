#!/bin/bash

echo "[!] STOP HUNTDOWN services..."

# list of services
services=("huntdown-workflow" "huntdown-attack-manager" "huntdown-instantiation" "huntdown-database" "pulsar-standalone")

echo "[*] Stopping pm2 services..."
sudo -u huntdown pm2 del all

for service in "${services[@]}"; do
    echo "[*] Stopping service: $service..."
    sudo systemctl stop "$service"

    # wait for service to be fully inactive
    while true; do
        # check if service is inactive
        inactive=$(systemctl is-active "$service")
        
        if [[ $inactive == "inactive" || $inactive == "failed" ]]; then
            echo "[+] Stopped: $service"
            # break to stop next service
            break
        else
            echo "[*] Waiting for: $service to stop"
            # wait before next check
            sleep 3
        fi
    done
done

echo "[+] All services stopped!"
