#!/bin/bash

HUNTDOWN_SERVICES="huntdown-database huntdown-instantiation huntdown-attack-manager"

function print_code {
    echo -n "$1"
    shift
    echo -e " \033[93m$*\033[0m"
}

function print_ok {
    echo -n "$1"
    shift
    echo -e " \033[32m$*\033[0m"
}

function print_nok {
    echo -n "$1"
    shift
    echo -e " \033[31m$*\033[0m"
}

function reinstall_message {
    echo If this doesn\'t work you can reinstall Huntdown
}

function check_enabled_service {
    service="$1"
    status=$(systemctl is-enabled "$service" 2> /dev/null)
    if [ "$status" = "enabled" ] ; then
        print_ok "$service" Enabled
        return 0
    else
        print_nok "$service" Not enabled
        echo You can fix this running:
        print_code systemctl enable "$service"
        return 1
    fi
}

function check_active_service {
    service="$1"
    status=$(systemctl is-active "$service")
    if [ "$status" = "active" ] ; then
        print_ok "$service" Active
        return 0
    else
        print_nok "$service" Failed
        echo You can fix this running:
        print_code systemctl start "$service"
        return 1
    fi
}

for service in $HUNTDOWN_SERVICES ; do
    check_enabled_service "$service"
    if [ $? -eq 1 ] ; then
        reinstall_message
    else
        check_active_service "$service"
    fi
done