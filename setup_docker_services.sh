#!/bin/bash

export sudo="sudo"
source installation_funcs.sh
#source vagrant_funcs.sh

function make_install_docker_services () {
    # It does not install the attacks!
    cd src
    print_header Starting HD Docker services
    if ! $sudo make install_docker_services >> "$LOG_FILE" 2>&1 ; then
        echo "Error while starting HD Docker services" >&2
        echo "Check ${LOG_FILE} for more information" >&2
        exit 1
    fi
    cd ..
}

if [[ -z $1  || "$1" != "-b" ]] ; then
    welcome_message
fi

# Mongo and Pulsar are already installed on an EC2 instance, check it is up and running
get_mongo_data aws
get_pulsar_data aws

create_base_dirs
copy_base_files
create_user
generate_environment_file

update_packages
#vagrant_install
#ansible_install
tryInstallNpmAndNode

make_install_docker_services
enable_services huntdown-database-docker huntdown-attack-manager-docker huntdown-instantiation-docker
start_services huntdown-database-docker huntdown-attack-manager-docker huntdown-instantiation-docker
install_check_script main