#!/bin/bash

export sudo=""
LOG_FILE=$(pwd)/tmp/log/huntdown_install-$(date +%Y-%m-%d-%H-%M-%S).log
HUNTDOWN_USER=root
HUNTDOWN_GROUP=root

source setup_scripts/variables.sh
source setup_scripts/installation_funcs.sh
source setup_scripts/compile.sh
source setup_scripts/mongo_funcs.sh
source setup_scripts/pulsar_funcs.sh

# Mongo and Pulsar are already installed on an EC2 instance, check it is up and running
get_mongo_data docker
get_pulsar_data docker

create_base_dirs
copy_base_files
generate_environment_file aws

make_all
make_install