#!/bin/bash

command=""
flag="-c"
time_set="no"
duration=-1
url=""
for i in "$@"; do
    # Detect if -c flag is active and get the duration
    if [[ "$i" == "$flag" ]]; then
        time_set="yes"
    elif [[ "$time_set" == "yes" ]]; then
        duration="$i"
        time_set="no"
    # Extract URL if it begins with 'http'
    elif [[ $i == http* ]]; then
        url=$i
        command+="${i} "
    else
        command+="${i} "
    fi
done

# Create a new process group
set -m
# Launch goldeneye in the new process group
goldeneye "$command" &
pid=$!
sleep "$duration"

# Kill the process group
kill -- -$pid

# Extract the domain and port from the URL
protocol=$(echo $url | awk -F[/:] '{print $1}')
domain=$(echo $url | awk -F[/:] '{print $4}')
port=$(echo $url | awk -F[/:] '{print $5}')
# If no port specified, use default based on protocol
if [ -z "$port" ]; then
    if [ "$protocol" == "http" ]; then
        port=80
    else
        port=443
    fi
fi
# Get the IP from the domain
ip=$(dig +short $domain)
# Execute ncat
nc -zvn -w 3 $ip $port
