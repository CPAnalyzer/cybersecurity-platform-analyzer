package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type Detected struct {
	Template string `json:"template,omitempty"`
	Type     string `json:"type,omitempty"`
	Severity string `json:"severity,omitempty"`
	URL      string `json:"url,omitempty"`
	Info     string `json:"info,omitempty"` // Cambiado a string
}

type Results struct {
	Detected []Detected `json:"detected"`
}

func parseInput(input string) Results {
	var results Results

	if input == "" {
		return results
	}

	lines := strings.Split(input, "\n")
	for _, line := range lines {
		if line == "" {
			continue
		}

		parts := strings.Fields(line)
		if len(parts) < 4 {
			continue
		}

		cveID := strings.TrimSuffix(strings.TrimPrefix(parts[0], "["), "]")

		var info string
		if len(parts) >= 5 {
			info = strings.Join(parts[4:], " ") // Unir todas las partes desde la posición 4 hasta el final
		}

		result := Detected{
			Template: cveID,
			Type:     parts[1],
			Severity: parts[2],
			URL:      parts[3],
			Info:     info,
		}

		results.Detected = append(results.Detected, result)
	}

	return results
}

func saveToJSON(filename string, data interface{}) error {
	jsonData, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, jsonData, 0644)
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: go run main.go <input_filename>")
		return
	}

	inputFileName := os.Args[1]

	// Read input from file
	inputData, err := ioutil.ReadFile(inputFileName)
	if err != nil {
		fmt.Println("Error reading input file:", err)
		return
	}

	input := string(inputData)

	results := parseInput(input)

	// Save to JSON file
	outputFileName := "output_nuclei.json"
	err = saveToJSON(outputFileName, results)
	if err != nil {
		fmt.Println("Error saving to JSON file:", err)
		return
	}

	fmt.Printf("Data saved to %s\n", outputFileName)
}
