package main

import (
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	lgk "hd-database/libGenKey"
	"helpers/DatabaseHelper"
	"helpers/GeneralHelper"
	"log"
	"os"
	"strconv"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

var comm_client = PulsarLib.InitClient("pulsar://127.0.0.1:6650")
var username string
var password string

func getSessionID(user string, password string) string {
	log.Println("Connecting to pulsar")
	login_json := make(map[string]interface{})
	login_json["username"] = user
	h_pass := lgk.GenKey(password)
	login_json["hash"] = hex.EncodeToString(h_pass)

	res := PulsarLib.BuildMessage(login_json)
	jsonMachine := PulsarLib.SendRequestSync(comm_client, "ui-db.login", res)

	var jsonData map[string]interface{}
	err := json.Unmarshal([]byte(jsonMachine), &jsonData)
	if err != nil {
		log.Fatal(err)
	}

	sessionID := jsonData["session_id"].(string)
	return sessionID
}

func FillDBConnection(db_conn *dbd.DB_Connection) {
	db_conn.Host = GeneralHelper.ConfigEnvironment.DatabaseHost
	db_conn.Username = GeneralHelper.ConfigEnvironment.DatabaseUsername
	db_conn.Password = GeneralHelper.ConfigEnvironment.DatabasePassword
	db_conn.Port = GeneralHelper.ConfigEnvironment.DatabasePort
}

func main() {
	//"marc.iglesias@hotmail.com", "kigle1998"
	var mongo_host *string
	var mongo_user *string
	var mongo_pass *string
	var pulsar_host *string
	var pulsar_port *int
	var mongo_port *int

	GeneralHelper.LoadConfig()

	mongo_host = flag.String("mongo", "", "MongoDB host: e.g. 192.168.122.168")
	mongo_user = flag.String("mongo-user", "", "MongoDB user, overrides config file")
	mongo_pass = flag.String("mongo-pass", "", "MongoDB password, overrides config file")
	pulsar_host = flag.String("pulsar", "", "Pulsar host: e.g. 192.168.122.168")
	pulsar_port = flag.Int("pulsar-port", 6650, "Pulsar port: e.g. 6650")
	mongo_port = flag.Int("mongo-port", 27017, "Mongo port: e.g. 27017")
	flag.Parse()

	var pulsarHostURI string
	if *pulsar_host == "" && GeneralHelper.ConfigEnvironment.PulsarHost == "" {
		GeneralHelper.ConfigEnvironment.PulsarHost = "localhost"
	} else if *pulsar_host != "" {
		GeneralHelper.ConfigEnvironment.PulsarHost = *pulsar_host
	}

	if GeneralHelper.ConfigEnvironment.PulsarPort == "" {
		GeneralHelper.ConfigEnvironment.PulsarPort = strconv.Itoa(*pulsar_port)
	}
	pulsarHostURI = fmt.Sprintf("pulsar://%s:%s", GeneralHelper.ConfigEnvironment.PulsarHost, GeneralHelper.ConfigEnvironment.PulsarPort)

	if *mongo_host == "" && GeneralHelper.ConfigEnvironment.DatabaseHost == "" {
		GeneralHelper.ConfigEnvironment.DatabaseHost = "localhost"
	} else if *mongo_host != "" {
		GeneralHelper.ConfigEnvironment.DatabaseHost = *mongo_host
	}

	if GeneralHelper.ConfigEnvironment.DatabasePort == "" {
		GeneralHelper.ConfigEnvironment.DatabasePort = strconv.Itoa(*mongo_port)
	}

	if GeneralHelper.ConfigEnvironment.DatabaseUsername == "" || *mongo_user != "" {
		GeneralHelper.ConfigEnvironment.DatabaseUsername = *mongo_user
	}
	if GeneralHelper.ConfigEnvironment.DatabasePassword == "" || *mongo_pass != "" {
		GeneralHelper.ConfigEnvironment.DatabasePassword = *mongo_pass
	}

	var db_conn dbd.DB_Connection
	var categories []string
	var attack_names []string
	var presets []map[string]interface{}
	//var solution [][]string
	FillDBConnection(&db_conn)
	//Create the Pulsar Client
	log.Println(pulsarHostURI)
	client := PulsarLib.InitClient(pulsarHostURI)
	defer (*client.Client).Close()
	lenght := len(os.Args)
	if lenght > 2 {
		username = os.Args[1]
		password = os.Args[2]
	}
	if lenght > 3 {
		goal := os.Args[len(os.Args)-1]
		for _, category := range os.Args[3:] {
			log.Println(category)
			attack_names = queryByCategory(username, password, category)
			for _, attackName := range attack_names {
				log.Println("Attack name: ", attackName)
				categories = append(categories, attackName)
				log.Println("All categories: ", categories)
				preset := queryByPresets(username, password, attackName, goal)
				if len(preset) > 0 {
					presets = append(presets, preset[0])
				}

			}
		}
	}

	jsonDataNice, _ := json.MarshalIndent(presets, "", "  ")
	log.Println(string(jsonDataNice))
}

func queryByPresets(username string, password string, attack_name string, category_name string) []map[string]interface{} {
	session_id := getSessionID(username, password)
	filter := DatabaseHelper.Filter{Field: "goal", Value: category_name, Op: "$eq"}
	filter2 := DatabaseHelper.Filter{Field: "attackName", Value: attack_name, Op: "$eq"}
	sessionId, _ := strconv.Atoi(session_id)
	query := DatabaseHelper.DatabaseQuery{Session_id: sessionId,
		Database:   "general",
		Collection: "attacksPresets", Filters: []DatabaseHelper.Filter{filter, filter2}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	result := PulsarLib.SendRequestSync(comm_client, "db.newQuery", to_send)
	var data []map[string]interface{}

	err := json.Unmarshal(result, &data)
	if err != nil {
		return data
	}

	return data
}

func queryByCategory(username string, password string, category_name string) []string {
	var attackNames []string
	session_id := getSessionID(username, password)
	filter := DatabaseHelper.Filter{Field: category_name, Value: "true", Op: "$ex"}
	sessionId, _ := strconv.Atoi(session_id)
	query := DatabaseHelper.DatabaseQuery{Session_id: sessionId,
		Database:   "general",
		Collection: "Classification", Filters: []DatabaseHelper.Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	result := PulsarLib.SendRequestSync(comm_client, "db.newQuery", to_send)
	var data []map[string]interface{}

	err := json.Unmarshal(result, &data)

	if err != nil {
		log.Println("Error:", err)
		return attackNames
	}

	for _, item := range data {

		classification := item["classification"].(map[string]interface{})

		// Recorrer la clasificación
		for key, value := range classification {
			if key == category_name {
				switch v := value.(type) {
				case map[string]interface{}:
					// Manejar objetos anidados
					for k, val := range v {
						if k == "name" {
							attackNames = append(attackNames, val.(string))
						}
					}
				default:
					log.Printf("Should not happen: %v\n", v)
				}
			}
		}
	}
	return attackNames
}

func queryByName(username string, password string, attack_name string) []string {
	session_id := getSessionID(username, password)
	log.Println(session_id)
	id_session, err := strconv.Atoi(session_id)
	log.Println(err)
	data, _ := DatabaseHelper.GetGeneralEntryByName(id_session, &comm_client, "Classification", "classId", attack_name)

	var categories []string
	log.Println(data)

	if classification, ok := data["classification"].(map[string]interface{}); ok {
		// Recorrer las claves dentro de "classification"
		for key, _ := range classification {
			categories = append(categories, key)
		}
	} else {
		log.Println("No se encontró la clave 'classification' en el JSON decodificado")
	}

	return categories
}
