package main

import (
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"

	lgk "hd-database/libGenKey"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

type WorkflowMessage struct {
	Session_id string `json:"session_id"`
	Workflow   []byte `json:"workflow"`
}

type StartWorkflow struct {
	Session_id  string `json:"session_id"`
	Workflow_id string `json:"workflow_id"`
}

type GetAttackPresets struct {
	Session_id  string `json:"session_id"`
	Attack_name string `json:"attack_name"`
}

// variables globales
var client = PulsarLib.InitClient("pulsar://127.0.0.1:6650")

// Función para obtener el sessionid
func getSessionID(user string, password string) string {
	login_json := make(map[string]interface{})
	login_json["username"] = user
	h_pass := lgk.GenKey(password)
	login_json["hash"] = hex.EncodeToString(h_pass)

	res := PulsarLib.BuildMessage(login_json)
	jsonMachine := PulsarLib.SendRequestSync(client, "ui-db.login", res)

	var jsonData map[string]interface{}
	err := json.Unmarshal([]byte(jsonMachine), &jsonData)
	if err != nil {
		log.Fatal(err)
	}

	sessionID := jsonData["session_id"].(string)
	return sessionID
}

func saveWorkflow(sessionID string, jsonFile string) {
	// Reed workflow.json
	workflow, err := os.ReadFile(jsonFile)
	if err != nil {
		log.Fatal(err)
	}

	//Send workflow to workflow-manager
	data := WorkflowMessage{
		Session_id: sessionID,
		Workflow:   workflow,
	}

	// Converting the WorkflowMessage structure into a map
	dataJson, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}

	var dataMap map[string]interface{}
	err = json.Unmarshal(dataJson, &dataMap)
	if err != nil {
		panic(err)
	}

	//Saving workflow to the database
	res := PulsarLib.BuildMessage(dataMap)
	workflow_id := PulsarLib.SendRequestSync(client, "ui-wm.saveWorkflow", res)
	log.Println("Message from saveWorkflow: " + string(workflow_id))
}

func startWorkflow(sessionID string, workflowID string) {
	//Starting workflow attack
	startWorkflow := StartWorkflow{
		Session_id:  sessionID,
		Workflow_id: workflowID,
	}

	sndMessage, err := json.Marshal(startWorkflow)
	if err != nil {
		panic(err)
	}

	var startgeWorkflowMap map[string]interface{}
	err = json.Unmarshal(sndMessage, &startgeWorkflowMap)
	if err != nil {
		panic(err)
	}

	res := PulsarLib.BuildMessage(startgeWorkflowMap)
	response := PulsarLib.SendRequestSync(client, "ui-wm.startWorkflow", res)
	log.Println("Message from startWorkflow: " + string(response))
}

func getWorkflow(sessionID string, workflowID string) {

	getWorkflow := StartWorkflow{
		Session_id:  sessionID,
		Workflow_id: workflowID,
	}

	// Convert getWorkflow to a map
	getWorkflowJson, err := json.Marshal(getWorkflow)
	if err != nil {
		panic(err)
	}

	var getWorkflowMap map[string]interface{}
	err = json.Unmarshal(getWorkflowJson, &getWorkflowMap)
	if err != nil {
		panic(err)
	}

	res := PulsarLib.BuildMessage(getWorkflowMap)
	response := PulsarLib.SendRequestSync(client, "ui-wm.getWorkflow", res)
	log.Println("Message from getWorkflow: " + string(response))
}

func deleteWorkflow(sessionID string, workflowID string) {

	deleteWorkflow := StartWorkflow{
		Session_id:  sessionID,
		Workflow_id: workflowID,
	}

	// Convert deleteWorkflow to a map
	deleteWorkflowJson, err := json.Marshal(deleteWorkflow)
	if err != nil {
		panic(err)
	}

	var deleteWorkflowMap map[string]interface{}
	err = json.Unmarshal(deleteWorkflowJson, &deleteWorkflowMap)
	if err != nil {
		panic(err)
	}

	res := PulsarLib.BuildMessage(deleteWorkflowMap)
	response := PulsarLib.SendRequestSync(client, "ui-wm.deleteWorkflow", res)
	log.Println("Message from getWorkflow: " + string(response))
}

func getAllWorkflows(sessionID string) {

	getAllMap := make(map[string]interface{})
	getAllMap["session_id"] = interface{}(sessionID)

	res := PulsarLib.BuildMessage(getAllMap)
	response := PulsarLib.SendRequestSync(client, "ui-wm.getAllWorkflows", res)
	log.Println("Message from getAllWorkflows: " + string(response))
}

func getAttackPresets(sessionID string, attackName string) {

	getAttackPresets := GetAttackPresets{
		Session_id:  sessionID,
		Attack_name: attackName,
	}

	// Convert getAttackPresets to a map
	getAttackPresetsJson, err := json.Marshal(getAttackPresets)
	if err != nil {
		panic(err)
	}

	var getAttackPresetsMap map[string]interface{}
	err = json.Unmarshal(getAttackPresetsJson, &getAttackPresetsMap)
	if err != nil {
		panic(err)
	}

	res := PulsarLib.BuildMessage(getAttackPresetsMap)
	response := PulsarLib.SendRequestSync(client, "ui-wm.getAttackPresets", res)
	log.Println("Message from presets: " + string(response))
}

// New functions for storing required structures in the front end
func saveConstruct(sessionID string, jsonFile string) {

	// Reed construct.json
	construct, err := os.ReadFile(jsonFile)
	if err != nil {
		log.Fatal(err)
	}

	ConstructMessage := struct {
		Session_id string `json:"session_id"`
		Construct  []byte `json:"construct"`
	}{
		Session_id: sessionID,
		Construct:  construct,
	}

	// Converting the ConstructMessage structure into a map
	dataJson, err := json.Marshal(ConstructMessage)
	if err != nil {
		panic(err)
	}

	var dataMap map[string]interface{}
	err = json.Unmarshal(dataJson, &dataMap)
	if err != nil {
		panic(err)
	}

	//Saving construct to the database
	res := PulsarLib.BuildMessage(dataMap)
	workflow_id := PulsarLib.SendRequestSync(client, "ui-wm.saveConstruct", res)
	log.Println("Message from saveConstruct: " + string(workflow_id))
}

func getConstruct(sessionID string, constructID string) {

	ConstructMessage := struct {
		Session_id   string `json:"session_id"`
		Construct_id string `json:"construct_id"`
	}{
		Session_id:   sessionID,
		Construct_id: constructID,
	}

	// Convert ConstructMessage to a map
	getConstructJson, err := json.Marshal(ConstructMessage)
	if err != nil {
		panic(err)
	}

	var getConstructMap map[string]interface{}
	err = json.Unmarshal(getConstructJson, &getConstructMap)
	if err != nil {
		panic(err)
	}

	res := PulsarLib.BuildMessage(getConstructMap)
	response := PulsarLib.SendRequestSync(client, "ui-wm.getConstruct", res)
	log.Println("Message from getConstruct: " + string(response))
}

func deleteConstruct(sessionID string, constructID string) {

	ConstructMessage := struct {
		Session_id   string `json:"session_id"`
		Construct_id string `json:"construct_id"`
	}{
		Session_id:   sessionID,
		Construct_id: constructID,
	}

	// Convert ConstructMessage to a map
	getConstructJson, err := json.Marshal(ConstructMessage)
	if err != nil {
		panic(err)
	}

	var getConstructMap map[string]interface{}
	err = json.Unmarshal(getConstructJson, &getConstructMap)
	if err != nil {
		panic(err)
	}

	res := PulsarLib.BuildMessage(getConstructMap)
	response := PulsarLib.SendRequestSync(client, "ui-wm.deleteConstruct", res)
	log.Println("Message from deleteConstruct: " + string(response))
}

func getAllConstructs(sessionID string) {

	getAllMap := make(map[string]interface{})
	getAllMap["session_id"] = interface{}(sessionID)

	res := PulsarLib.BuildMessage(getAllMap)
	response := PulsarLib.SendRequestSync(client, "ui-wm.getAllConstructs", res)
	log.Println("Message from getAllConstructs: " + string(response))
}

// handleWorkflow handles the start, get, and delete operations for workflows.
func handleWorkflow(operation, sessionID, id string) {
	switch operation {
	case "startWorkflow":
		startWorkflow(sessionID, id)
	case "getWorkflow":
		getWorkflow(sessionID, id)
	case "deleteWorkflow":
		deleteWorkflow(sessionID, id)
	default:
		fmt.Println("Invalid workflow operation. Valid operations: startWorkflow, getWorkflow, deleteWorkflow")
		os.Exit(2)
	}
}

// handleConstruct handles the get and delete operations for constructs.
func handleConstruct(operation, sessionID, id string) {
	switch operation {
	case "getConstruct":
		getConstruct(sessionID, id)
	case "deleteConstruct":
		deleteConstruct(sessionID, id)
	default:
		fmt.Println("Invalid construct operation. Valid operations: getConstruct, deleteConstruct")
		os.Exit(2)
	}
}

func main() {
	userPtr := flag.String("u", "", "Username")
	passPtr := flag.String("p", "", "Password")
	optionPtr := flag.String("o", "", "Option")
	filePtr := flag.String("file", "", "Json File")
	idPtr := flag.String("id", "", "ID")
	attackPtr := flag.String("attack", "", "Attack Name")

	flag.Usage = func() {
		fmt.Println("Usage: ./uiSimulator -u <username> -p <password> -o <option> [-file <json file>] [-id <id>] [-attack <attack name>]")
		fmt.Println("Options:")
		fmt.Println("  saveWorkflow: Save a workflow. Requires -file.")
		fmt.Println("  startWorkflow: Start a workflow. Requires -id.")
		fmt.Println("  getWorkflow: Get a workflow. Requires -id.")
		fmt.Println("  deleteWorklow: Delete a workflow. Requires -id.")
		fmt.Println("  allWorkflows: Get all workflows. No additional arguments required.")
		fmt.Println("  presets: Get attack presets. Requires -attack.")
		fmt.Println("  saveConstruct: Save a construct. Requires -file.")
		fmt.Println("  getConstruct: Get a construct. Requires -id.")
		fmt.Println("  deleteConstruct: Delete a construct. Requires -id.")
		fmt.Println("  allConstructs: Get all constructs. No additional arguments required.")
	}

	flag.Parse()

	// Check for required arguments
	if *userPtr == "" && *passPtr == "" && *optionPtr == "" {
		flag.Usage()
		os.Exit(1)
	}

	if *userPtr == "" {
		fmt.Println("Error: Must provide a username (-u)")
		os.Exit(1)
	}
	if *passPtr == "" {
		fmt.Println("Error: Must provide a password (-p)")
		os.Exit(1)
	}
	if *optionPtr == "" {
		fmt.Println("Error: Must provide an option (-o)")
		os.Exit(1)
	}

	switch *optionPtr {
	case "saveWorkflow":
		if *filePtr == "" {
			fmt.Println("Usage: ./uiSimulator -u <username> -p <password> -o saveWorkflow -file <json workflow file>")
			os.Exit(2)
		}
		sessionID := getSessionID(*userPtr, *passPtr)
		saveWorkflow(sessionID, *filePtr)

	case "startWorkflow", "getWorkflow", "deleteWorkflow":
		if *idPtr == "" {
			fmt.Println("Usage: ./uiSimulator -u <username> -p <password> -o", *optionPtr, "-id <workflow ID>")
			os.Exit(2)
		}
		sessionID := getSessionID(*userPtr, *passPtr)
		handleWorkflow(*optionPtr, sessionID, *idPtr)

	case "allWorkflows":
		sessionID := getSessionID(*userPtr, *passPtr)
		getAllWorkflows(sessionID)

	case "presets":
		if *attackPtr == "" {
			fmt.Println("Usage: ./uiSimulator -u <username> -p <password> -o presets -attack <attack name>")
			os.Exit(2)
		}
		sessionID := getSessionID(*userPtr, *passPtr)
		getAttackPresets(sessionID, *attackPtr)

	case "saveConstruct":
		if *filePtr == "" {
			fmt.Println("Usage: ./uiSimulator -u <username> -p <password> -o saveConstruct -file <construct json file>")
			os.Exit(2)
		}
		sessionID := getSessionID(*userPtr, *passPtr)
		saveConstruct(sessionID, *filePtr)

	case "getConstruct", "deleteConstruct":
		if *idPtr == "" {
			fmt.Println("Usage: ./uiSimulator -u <username> -p <password> -o", *optionPtr, "-id <construct_id>")
			os.Exit(2)
		}
		sessionID := getSessionID(*userPtr, *passPtr)
		handleConstruct(*optionPtr, sessionID, *idPtr)

	case "allConstructs":
		sessionID := getSessionID(*userPtr, *passPtr)
		getAllConstructs(sessionID)

	default:
		fmt.Println("Invalid option")
		flag.Usage()
		os.Exit(2)
	}
}
