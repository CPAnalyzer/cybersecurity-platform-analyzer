package main

import (
	"fmt"
	"log"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func WorkflowHandler(workflow AttackWorkflow, session_id int, message PulsarLib.MessageResponse) {

	currentBlockId := workflow.AttackEntryPoint
	currentBlock := getBlock(currentBlockId, workflow.AttackWorkflow)

	for i := 0; i < len(workflow.AttackWorkflow); i++ {

		log.Println("CURRENT BLOCK")
		log.Println(currentBlock)

		if currentBlock.IterateOver != nil {
			log.Println("HANDLE IERATOR")
			handleIterator(currentBlock, session_id, message, workflow)
		} else if currentBlock.ExecuteIf != nil && currentBlock.IterateOver == nil {
			log.Println("HANDLE IF CONDITION")
			handleIfCondition(currentBlock, session_id, message, workflow)
		} else {
			log.Println("NO ITERATOR NOR IF CONDITION")
			executeBlock(*currentBlock, session_id, message.Id, workflow.WorkflowName)
		}

		var workflowFinished bool
		currentBlock, workflowFinished = handleBlock(currentBlock, workflow)

		if workflowFinished {
			log.Println("WORKFLOW FINISHED")
			break
		}

		/*
			// Check if there is a next block
			if currentBlock.NextBlockId == -1 {
				break
			}

			if currentBlock.ExecuteIf != nil {
				if currentBlock.ExecuteIf.IfTrue == -1 {
					break
				} else {
					currentBlock = getBlock(currentBlock.ExecuteIf.IfTrue, workflow.AttackWorkflow)
				}
			} else {
				// Moving to the next block
				currentBlock = getBlock(currentBlock.NextBlockId, workflow.AttackWorkflow)
			}
		*/
	}
}

func handleIterator(currentBlock *Block, session_id int, message PulsarLib.MessageResponse, workflow AttackWorkflow) {

	// HandleIterator
	fmt.Println("Block has IterateOver condition")
	list, err := getListFromGlobalData(currentBlock.IterateOver.OutputName)

	if err != nil {
		log.Println(err)
	}

	for _, value := range list {

		// Rene Comment FIXME
		// We have to allow arbitrary keys here, to do this we need to define the iteration variable's key to iterate over
		inputStructure := map[string]InputStructure{
			"IP/Host": {Value: value.(string)},
		}

		// Edit InputStructure
		currentBlock.Input = inputStructure
		initialOutput := currentBlock.Output
		currentBlock.Output = initialOutput + value.(string)

		// Check if ExecuteIf condition exists
		if currentBlock.ExecuteIf == nil {
			// If there is no ExecuteIf condition, execute the block
			executeBlock(*currentBlock, session_id, message.Id, workflow.WorkflowName)
		} else {
			initialOutputName := currentBlock.ExecuteIf.OutputName
			currentBlock.ExecuteIf.OutputName = initialOutputName + value.(string)
			handleIfCondition(currentBlock, session_id, message, workflow)
			// reset OutputName
			currentBlock.ExecuteIf.OutputName = initialOutputName
		}
		// reset currentBlock.Output
		currentBlock.Output = initialOutput
	}
}

func handleIfCondition(currentBlock *Block, session_id int, message PulsarLib.MessageResponse, workflow AttackWorkflow) {

	fmt.Println("Block has IF condition")
	var conditionResult bool
	value, err := getValueFromGlobalData(currentBlock.ExecuteIf.OutputName, currentBlock.ExecuteIf.DataKey)

	// If currentBlock.ExecuteIf.DataKey is not found in OutputName, conditionResult will be false
	if err != nil {
		conditionResult = false
		return
		// If currentBlock.ExecuteIf.DataKey is found in OutputName, evaluate the condition
	} else {
		conditionResult = evaluateCondition(currentBlock.ExecuteIf.Condition, value, currentBlock.ExecuteIf.Op)
	}

	log.Println("Condition result:", conditionResult)
	// If the condition is true, execute the block and move to the specified block
	if conditionResult {
		executeBlock(*currentBlock, session_id, message.Id, workflow.WorkflowName)
	}
}

func handleBlock(currentBlock *Block, workflow AttackWorkflow) (*Block, bool) {

	log.Println("HANDLE BLOCK")

	// Check if there is a next block
	if currentBlock.ExecuteIf != nil {
		if currentBlock.ExecuteIf.IfTrue == -1 {
			return currentBlock, true
		} else {
			// Moving to the next block
			currentBlock = getBlock(currentBlock.ExecuteIf.IfTrue, workflow.AttackWorkflow)
			log.Println("current block")
			log.Println(currentBlock)
			return currentBlock, false
		}
	}

	if currentBlock.IterateOver != nil {
		if currentBlock.IterateOver.IfTrue == -1 {
			return currentBlock, true
		} else {
			// Moving to the next block
			currentBlock = getBlock(currentBlock.IterateOver.IfTrue, workflow.AttackWorkflow)
			log.Println("current block")
			log.Println(currentBlock)
			return currentBlock, false
		}
	}

	if currentBlock.NextBlockId == -1 || currentBlock.NextBlockId == 0 {
		return currentBlock, true
	}

	// Moving to the next block
	currentBlock = getBlock(currentBlock.NextBlockId, workflow.AttackWorkflow)

	log.Println("CURRENT BLOCK")
	log.Println(currentBlock)

	return currentBlock, false
}
