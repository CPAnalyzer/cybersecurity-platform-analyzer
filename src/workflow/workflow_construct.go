package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"helpers/DatabaseHelper"
	"helpers/GeneralHelper"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func saveConstruct(constructToSave []byte) {
	var message PulsarLib.MessageResponse

	json.Unmarshal(constructToSave, &message)
	received_msg := message.Msg

	var session_id int
	var err error
	session_id, err = GeneralHelper.GetSession_id(received_msg)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	// Decode construct in base64
	construct, err := base64.StdEncoding.DecodeString(received_msg["construct"].(string))
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	var constructMap map[string]interface{}
	if err := json.Unmarshal(construct, &constructMap); err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	_, err = DatabaseHelper.InsertOneElement(session_id, comm_client, constructMap)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
	}

	//Send the message to workflowManager
	PulsarLib.SendMessage(*comm_client, message.Id, []byte("Construct saved."))
}

func getConstruct(msg []byte) {
	var message PulsarLib.MessageResponse

	json.Unmarshal(msg, &message)
	received_msg := message.Msg

	var session_id int
	var err error
	session_id, err = GeneralHelper.GetSession_id(received_msg)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var construct_id string
	err = GeneralHelper.GetMapElement(received_msg, "construct_id", &construct_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}
	result, err := DatabaseHelper.GetEntryByName(session_id, comm_client, "workflow_id", construct_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}
	b_result, err := json.Marshal(result)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}

	PulsarLib.SendMessage(*comm_client, message.Id, b_result)
}

func deleteConstruct(construct_id []byte) {
	var message PulsarLib.MessageResponse

	json.Unmarshal(construct_id, &message)
	received_msg := message.Msg

	var session_id int
	var err error
	session_id, err = GeneralHelper.GetSession_id(received_msg)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var ct_id string
	err = GeneralHelper.GetMapElement(received_msg, "construct_id", &ct_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	_, err = DatabaseHelper.DeleteOneElement(session_id, comm_client, "construct_id", ct_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}

	PulsarLib.SendMessage(*comm_client, message.Id, []byte("Construct deleted."))
}

func getAllConstructs(session []byte) {
	var message PulsarLib.MessageResponse

	json.Unmarshal(session, &message)
	received_msg := message.Msg

	//Validating session
	session_id, err := GeneralHelper.GetSession_id(received_msg)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	all_constructs, err := DatabaseHelper.GetAllEntriesByField(session_id, comm_client, "construct_id")
	if err != nil {
		log.Error("Error getting construct list ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
	} else if all_constructs == nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(errors.New("no constructs present"), GeneralHelper.UnknownError))
		return
	}
	b_all_constructs, err := json.Marshal(all_constructs)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(
			errors.New("error marshalling constructs"), GeneralHelper.UnknownError))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, b_all_constructs)
}
