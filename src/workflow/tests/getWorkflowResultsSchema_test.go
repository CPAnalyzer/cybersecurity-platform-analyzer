package getworkflowresultsschema

import (
	"confManager"
	"encoding/hex"
	"encoding/json"
	"flag"
	querymanager "hd-database/QueryManager"
	lgk "hd-database/libGenKey"
	"hd-database/rawHelpers"
	"helpers/GeneralHelper"
	"os"
	"reflect"
	"testing"

	log "github.com/sirupsen/logrus"

	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

const presets_collection = "WorkflowResultsSchema"
const preset_key = "workflow"

func TestWorkflowResultsSchema(t *testing.T) {
	env_json := flag.String("env", "/etc/huntdown/environment.json", "Environment json file: e.g. /etc/huntdown/environment.json")

	confEnvironment := confManager.LoadConfig(*env_json)

	pulsar_client := PulsarLib.InitClient("pulsar://" + confEnvironment.PulsarHost + ":" + confEnvironment.PulsarPort)
	defer (*pulsar_client.Client).Close()

	db_conn := rawHelpers.CreateDBConnection(confEnvironment)

	session_id, _ := dbw.Connect(db_conn)

	db_client, err := dbw.GetDBContext(session_id)
	if err != nil {
		log.Fatalf("Could not get database context: %v", err)
	}
	db_client.Database = rawHelpers.GeneralDB

	db_client.Collection = presets_collection

	filter := []dbd.Filters{}
	querymanager.ConstructBasicFilter(preset_key, "", "$ne", &filter)
	results, err := dbw.Get_documents(&db_client, &filter)
	if err != nil {
		log.Fatalf("Error getting Workflow from database: %v", err)
	}

	var db_result = []map[string]interface{}{}
	err = json.Unmarshal(results, &db_result)
	if err != nil {
		log.Fatalf("Error unmarshalling results: %v", err)
	}

	my_usercontext := loginUser(pulsar_client)

	user_session_id, err := GeneralHelper.GetSession_id(my_usercontext)
	if err != nil {
		log.Println(err)
		return
	}
	query := make(map[string]interface{})
	query["session_id"] = user_session_id
	query["workflow_name"] = db_result[0][preset_key]

	p_query := PulsarLib.BuildMessage(query)
	r_response := PulsarLib.SendRequestSync(pulsar_client, "ui-wm.getWorkflowResultsSchema", p_query)
	var pulsar_result map[string]interface{}
	json.Unmarshal(r_response, &pulsar_result)

	if !reflect.DeepEqual(pulsar_result, db_result[0]) {
		logout_user(pulsar_client, my_usercontext)
		t.Error("Fatal, strings are different:")
		t.Error("Pulsar", pulsar_result)
		t.Error("Database", db_result)
	} else {
		log.Info("Test Passed!")
		logout_user(pulsar_client, my_usercontext)
	}
}

func loginUser(client PulsarLib.PL_Client) map[string]interface{} {
	login_json := make(map[string]interface{})
	json_context := make(map[string]interface{})

	data, err := os.ReadFile("./user.json")
	if err != nil {
		log.Println("Error loading file: " + err.Error())
		return nil
	}
	json.Unmarshal([]byte(data), &login_json)

	h_pass := lgk.GenKey(login_json["password"].(string))
	login_json["hash"] = hex.EncodeToString(h_pass)

	res := PulsarLib.BuildMessage(login_json)
	user_context := PulsarLib.SendRequestSync(client, "ui-db.login", res)
	err = json.Unmarshal(user_context, &json_context)
	if err != nil {
		log.Println("Error unmarshalling user context: " + err.Error())
		return nil
	}
	return json_context
}

func logout_user(client PulsarLib.PL_Client, user_context map[string]interface{}) bool {
	res := PulsarLib.BuildMessage(user_context)
	PulsarLib.SendRequestSync(client, "ui-db.logout", res)
	return true
}
