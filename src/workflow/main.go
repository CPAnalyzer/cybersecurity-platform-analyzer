package main

import (
	"confManager"
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	as "helpers/AttackSchema"
	"helpers/DatabaseHelper"
	"helpers/GeneralHelper"
	of "helpers/OptionsFile"
	"math/rand"
	"strconv"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

var comm_client *PulsarLib.PL_Client

// GlobalStore stores the outputs of the attacks
type GlobalData struct {
	OutputName string                 `json:"Name"`
	Data       map[string]interface{} `json:"value"`
}

// GlobalStore stores the processed output of an attack.
var globalStore map[string]GlobalData

// Workflow structure
type Block struct {
	BlockId     int                       `json:"block_id"`
	AttackName  string                    `json:"attack_name"`
	Preset      string                    `json:"preset"`
	Input       map[string]InputStructure `json:"input"`
	Output      string                    `json:"output"`
	NextBlockId int                       `json:"next_block"`
	ExecuteIf   *ExecuteIfCondition       `json:"execute_if"`
	IterateOver *IterateOverCondition     `json:"iterate_over"`
}

type InputStructure struct {
	Value string `json:"value"`
}

type ExecuteIfCondition struct {
	OutputName string `json:"OutputName"`
	DataKey    string `json:"Datakey"`
	Op         string `json:"op"`
	Condition  string `json:"condition"`
	IfTrue     int    `json:"if_true"`
}

type IterateOverCondition struct {
	OutputName string `json:"OutputName"`
	DataKey    string `json:"Datakey"`
	IfTrue     int    `json:"if_true"`
}

type AttackWorkflow struct {
	WorkflowId          string  `json:"workflow_id"`
	WorkflowName        string  `json:"workflow_name"`
	WorkflowDescription string  `json:"workflow_description"`
	AttackEntryPoint    int     `json:"attack_entry_point"`
	AttackWorkflow      []Block `json:"attack_workflow"`
}

// JSON structure of the message in charge of saving a workflow in the database.
type WorkflowMessage struct {
	Session_id string `json:"session_id"`
	Workflow   []byte `json:"workflow"`
}

// AttackStatus structure
type AttackStatus struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Percent int    `json:"percent"`
	State   string `json:"state"`
	Status  string `json:"status"`
	Step    string `json:"step"`
}

func getValueFromGlobalData(outputName, dataKey string) (interface{}, error) {
	for _, globalData := range globalStore {
		if globalData.OutputName == outputName {
			if value, ok := globalData.Data[dataKey]; ok {
				return value, nil
			}
		}
	}
	return nil, fmt.Errorf("value not found for output name: %s and data key: %s", outputName, dataKey)
}

func getListFromGlobalData(outputName string) (map[string]interface{}, error) {

	fmt.Println("globalStore")
	fmt.Println(globalStore)
	for _, globalData := range globalStore {
		fmt.Println("globalData")
		fmt.Println(globalData)
		if globalData.OutputName == outputName {
			list := globalData.Data
			if list != nil {
				return list, nil
			}
		}
	}
	return nil, fmt.Errorf("value not found for output name: %s", outputName)
}

func evaluateCondition(condition string, value, op interface{}) bool {
	var err error
	var f_value float64
	var f_op float64
	var is_string bool
	var s_value string
	var s_op string

	f_value = 0
	f_op = 0
	is_string = false
	if tmp, ok := value.(float64); ok {
		f_value = tmp
	} else if tmp, ok := value.(int); ok {
		f_value = float64(tmp)
	} else if tmp, ok := value.(string); ok {
		f_value, err = strconv.ParseFloat(tmp, 64)
		if err != nil {
			is_string = true
			s_value = tmp
		}
	} else {
		log.Println("Unknown value type")
		return false
	}
	if is_string {
		if tmp, ok := op.(string); ok {
			s_op = tmp
			return evaluateConditionString(condition, s_value, s_op)
		}
		log.Println("Error different types in comparison")
		return false
	}
	if tmp, ok := op.(float64); ok {
		f_op = tmp
	} else if tmp, ok := op.(int); ok {
		f_op = float64(tmp)
	} else if tmp, ok := op.(string); ok {
		f_op, err = strconv.ParseFloat(tmp, 64)
		if err != nil {
			log.Println("Error different types in comparison")
			return false
		}
	} else {
		log.Println("Unknown op type")
		return false
	}
	return evaluateConditionFloat(condition, f_value, f_op)
}

func evaluateConditionFloat(condition string, value, op float64) bool {
	switch condition {
	case "==":
		return value == op
	case "!=":
		return value != op
	case ">":
		return value > op
	case "<":
		return value < op
	case ">=":
		return value >= op
	case "<=":
		return value <= op
	default:
		log.Println("Invalid condition:", condition)
		return false
	}
}

func evaluateConditionString(condition, value, op string) bool {
	switch condition {
	case "==":
		return value == op
	case "!=":
		return value != op
	case ">":
		return value > op
	case "<":
		return value < op
	case ">=":
		return value >= op
	case "<=":
		return value <= op
	default:
		log.Println("Invalid condition:", condition)
		return false
	}
}

func startWorkflow(startWorkflow []byte) {

	//Deleting information from GlobalStore
	globalStore = make(map[string]GlobalData)

	var message PulsarLib.MessageResponse

	json.Unmarshal(startWorkflow, &message)

	session_id, err := GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Println("Error malformed attack query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}
	var workflow_id string
	err = GeneralHelper.GetMapElement(message.Msg, "workflow_id", &workflow_id)

	var workflow AttackWorkflow
	local_getWorkflow(session_id, workflow_id, &workflow)
	/* TODO */
	// Handle Worflow - WorkflowHandling
	WorkflowHandler(workflow, session_id, message)

	globalDataJSON, err := json.Marshal(globalStore)
	if err != nil {
		log.Printf("Error al converting GlobalData in JSON: %v", err)
		return
	}

	var response PulsarLib.MessageResponse
	err = json.Unmarshal(globalDataJSON, &response.Msg)
	if err != nil {
		log.Printf("Error converting response: %v", err)
		return
	}
	responseBytes, err := json.Marshal(response)
	if err != nil {
		log.Printf("Error converting response to byte array: %v", err)
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, responseBytes)
}

func getBlock(blockId int, blocks []Block) *Block {
	for i := range blocks {
		if blocks[i].BlockId == blockId {
			return &blocks[i]
		}
	}
	return nil
}

func executeBlock(block Block, session_id int, messageId string, workflowName string) {

	attackName := block.AttackName
	fmt.Println("Executing attack: " + attackName)

	//Getting attack optionsFile
	getOptions := map[string]any{
		"name":       attackName,
		"session_id": session_id,
	}
	res := PulsarLib.BuildMessage(getOptions)
	OptionsFileB := PulsarLib.SendRequestSync(*comm_client, "ui-db.getAttackOptions", res)

	var optionsFileList []of.OptionsFile
	err := json.Unmarshal(OptionsFileB, &optionsFileList)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, messageId, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	pr, err := DatabaseHelper.GetGeneralEntryByName(session_id, comm_client, "attacksPresets", "preset", block.Preset)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, messageId, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}
	var Preset Preset
	err = GeneralHelper.MapToStruct(pr, &Preset)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, messageId, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	//Getting attack user options
	attackUserOptions := block.Input

	log.Println("attackUserOptions")
	log.Println(attackUserOptions)

	for _, option := range Preset.MandatoryUserOptions {
		if _, ok := attackUserOptions[option]; !ok {
			err := fmt.Sprintf("The mandatory value is missing: '%s'", option)
			PulsarLib.SendMessage(*comm_client, messageId, []byte(err))
			return
		}
	}

	// Filling OptionsFile with Preset values
	for key, value := range Preset.Options {
		for i := range optionsFileList[0].Options {
			if optionsFileList[0].Options[i].Title == key {
				//Whichever option, we set checkbox to true
				optionsFileList[0].Options[i].Checkbox = true
				//If the option requires text
				if optionsFileList[0].Options[i].TextInput {
					optionsFileList[0].Options[i].Value = value.Value
				}
			}
		}
	}

	// Filling OptionsFile with User Input values
	for key, ioStruct := range attackUserOptions {
		for i := range optionsFileList[0].Options {
			if optionsFileList[0].Options[i].Title == key {
				// Whichever option, we set checkbox to true
				optionsFileList[0].Options[i].Checkbox = true

				// If the option requires text
				if optionsFileList[0].Options[i].TextInput {
					value := ioStruct.Value // Get the value from ioStruct
					optionsFileList[0].Options[i].Value = value
				}
			}
		}
	}

	//Submitting attack to database manager
	AttackSubmission := map[string]interface{}{
		"options":      optionsFileList,
		"session_id":   session_id,
		"attack_name":  optionsFileList[0].Name,
		"attack_title": workflowName + "_" + attackName,
	}

	msg := PulsarLib.BuildMessage(AttackSubmission)
	msgBytes, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}

	PulsarLib.SendMessage(*comm_client, "ui-db.AttackSubmission", msgBytes)

	//Starting attack
	sessionMap := map[string]interface{}{"session_id": session_id}
	res = PulsarLib.BuildMessage(sessionMap)
	var attacksBytes []byte
	//The following loop waits for ui-db.AttackSubmission to complete and thus finish saving the attack files in the database
	for {
		attacksBytes = PulsarLib.SendRequestSync(*comm_client, "ui-db.getAttacks", res)

		if strings.Contains(string(attacksBytes), workflowName+"_"+attackName) {
			break
		}
		time.Sleep(1 * time.Second)
	}

	var attacks []map[string]interface{}
	err = json.Unmarshal(attacksBytes, &attacks)
	if err != nil {
		panic(err)
	}

	//Taking the first attack from the list of NotExecuted attacks
	attack := attacks[0]

	startAttack := map[string]interface{}{
		"attack_name": attack["attackname"],
		"session_id":  session_id,
		"_id":         attack["_id"],
	}
	msg = PulsarLib.BuildMessage(startAttack)
	_ = PulsarLib.SendRequestSync(*comm_client, "ui-db.startAttack", msg)

	processedResult := map[string]interface{}{
		"session_id": session_id,
		"attack_id":  attack["_id"],
	}
	msg = PulsarLib.BuildMessage(processedResult)
	attackResultJSON := PulsarLib.SendRequestSync(*comm_client, "ui-db.getResult", msg)

	log.Println("Attack result:", string(attackResultJSON))
	var attackResult as.AttackResult
	err = json.Unmarshal(attackResultJSON, &attackResult)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, messageId, []byte("Error unmarshalling attackResultJSON"))
	}

	log.Println("Valor del campo processedOutput:", attackResult.ProcessedOutput)

	//Saving attack results in GlobalData
	data := make(map[string]interface{})

	// Combine all the key-value pairs from attackOutput into a single map
	for _, outputMap := range attackResult.ProcessedOutput {
		for key, value := range outputMap {
			data[key] = value
		}
	}

	newEntry := GlobalData{
		OutputName: block.Output,
		Data:       data,
	}
	globalStore[block.Output] = newEntry
}

func createWorkflowWithID(workflowS string, messageId string) AttackWorkflow {

	// Decode workflow in base64
	workflowDecoded, err := base64.StdEncoding.DecodeString(workflowS)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, messageId, []byte("Error decoding workflow: "+err.Error()))
	}

	log.Println("workflowDecoded")
	log.Println(string(workflowDecoded))

	var workflow AttackWorkflow
	err = json.Unmarshal(workflowDecoded, &workflow)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, messageId, []byte("Error decoding workflow: "+err.Error()))
	}

	blockHashes := make([]string, len(workflow.AttackWorkflow))

	//Getting first block
	currentBlockId := workflow.AttackEntryPoint
	currentBlock := getBlock(currentBlockId, workflow.AttackWorkflow)

	for i := 0; i < len(workflow.AttackWorkflow); i++ {

		blockBytes, err := json.Marshal(currentBlock)
		if err != nil {
			PulsarLib.SendMessage(*comm_client, messageId, []byte("Error encoding block: "+err.Error()))
		}

		hashBytes := md5.Sum(blockBytes)

		blockHashes[i] = base64.StdEncoding.EncodeToString(hashBytes[:])

		log.Println("CURRENT BLOCK")
		log.Println(currentBlock)

		// Check if there is a next block
		if currentBlock.NextBlockId == -1 {
			break
		}

		// Moving to the next block
		currentBlock = getBlock(currentBlock.NextBlockId, workflow.AttackWorkflow)
	}

	workflow.WorkflowId = strconv.Itoa(rand.Intn(1000000))
	return workflow
}

func saveWorkflow(workflow []byte) {
	var err error
	var session_id int
	var message PulsarLib.MessageResponse

	json.Unmarshal(workflow, &message)
	received_msg := message.Msg

	session_id, err = GeneralHelper.GetSession_id(received_msg)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var workflow_id string
	err = GeneralHelper.GetMapElement(message.Msg, "workflow_id", &workflow_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	// Creating an identifier for the workflow
	workflow_with_id := createWorkflowWithID(workflow_id, message.Id)

	_, err = DatabaseHelper.GetEntryByName(session_id, comm_client, "workflow_id", workflow_with_id.WorkflowId)
	if err == nil { // Element exists, so we bail out
		err = errors.New("Workflow already exists")
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.AlreadyExists))
		return
	}

	workflowBytes, err := json.Marshal(workflow_with_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}
	var mapWorkflow map[string]interface{}
	err = json.Unmarshal(workflowBytes, &mapWorkflow)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}

	_, err = DatabaseHelper.InsertOneElement(session_id, comm_client, mapWorkflow)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, []byte(workflow_with_id.WorkflowId))
}

func local_getWorkflow(session_id int, workflow_id string, workflow *AttackWorkflow) error {
	wf, err := DatabaseHelper.GetEntryByName(session_id, comm_client, "workflow_id", workflow_id)
	if err != nil {
		return err
	}
	return GeneralHelper.MapToStruct(wf, &workflow)
}

func getWorkflow(workflow_id []byte) {
	var message PulsarLib.MessageResponse

	json.Unmarshal(workflow_id, &message)
	received_msg := message.Msg

	var session_id int
	var err error
	session_id, err = GeneralHelper.GetSession_id(received_msg)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var wf_id string
	err = GeneralHelper.GetMapElement(received_msg, "workflow_id", &wf_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}
	var workflow AttackWorkflow
	err = local_getWorkflow(session_id, wf_id, &workflow)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	b_result, err := json.Marshal(workflow)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, b_result)
}

func deleteWorkflow(workflow_id []byte) {
	var message PulsarLib.MessageResponse

	json.Unmarshal(workflow_id, &message)
	received_msg := message.Msg

	var session_id int
	var err error
	session_id, err = GeneralHelper.GetSession_id(received_msg)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var wf_id string
	err = GeneralHelper.GetMapElement(received_msg, "workflow_id", &wf_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}
	_, err = DatabaseHelper.DeleteOneElement(session_id, comm_client, "workflow_id", wf_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}
	PulsarLib.SendMessage(*comm_client, received_msg["id"].(string), []byte("Workflow with id: "+received_msg["workflow_id"].(string)+" deleted"))
}

func getAllWorkflows(session []byte) {
	var query DatabaseHelper.DatabaseQuery
	var message PulsarLib.MessageResponse

	json.Unmarshal(session, &message)
	status := message.Msg

	//Validating session
	session_id, err := GeneralHelper.GetSession_id(status)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	filter := DatabaseHelper.Filter{Field: "workflow_id", Value: "true", Op: "$exists"}
	query = DatabaseHelper.DatabaseQuery{Session_id: session_id,
		Filters: []DatabaseHelper.Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	tmp_map := []map[string]interface{}{}
	json.Unmarshal(response, &tmp_map)
	if len(tmp_map) == 0 {
		log.Debug("No Workflows found")
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(
			errors.New("no workflows present"), GeneralHelper.FieldNotFound))
	}
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}

func main() {
	confManager.LoadDefaultConfig()
	pulsar_host := confManager.GetValue("pulsar_host")
	pulsar_port := confManager.GetValue("pulsar_port")

	client := PulsarLib.InitClient("pulsar://" + pulsar_host + ":" + pulsar_port)
	defer (*client.Client).Close()
	comm_client = &client

	globalStore = make(map[string]GlobalData)
	var mainwg sync.WaitGroup
	mainwg.Add(11)

	PulsarLib.WaitEvent(client, "ui-wm.saveWorkflow", saveWorkflow)
	PulsarLib.WaitEvent(client, "ui-wm.startWorkflow", startWorkflow)
	PulsarLib.WaitEvent(client, "ui-wm.getWorkflow", getWorkflow)
	PulsarLib.WaitEvent(client, "ui-wm.deleteWorkflow", deleteWorkflow)
	PulsarLib.WaitEvent(client, "ui-wm.getAllWorkflows", getAllWorkflows)
	PulsarLib.WaitEvent(client, "ui-wm.getAttackPresets", getAttackPresets)

	PulsarLib.WaitEvent(client, "ui-wm.getWorkflowResultsSchema", getGeneralWorkflowResultsSchema)
	// New functions for storing required structures in the front end
	PulsarLib.WaitEvent(client, "ui-wm.saveConstruct", saveConstruct)
	PulsarLib.WaitEvent(client, "ui-wm.getConstruct", getConstruct)
	PulsarLib.WaitEvent(client, "ui-wm.deleteConstruct", deleteConstruct)
	PulsarLib.WaitEvent(client, "ui-wm.getAllConstructs", getAllConstructs)

	mainwg.Wait()
}
