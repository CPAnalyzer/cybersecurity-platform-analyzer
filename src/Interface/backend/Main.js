const http = require('http');
const https = require('https');
const fs = require('fs');
const express = require('express');
const app = express();
const path = require('path');
const session = require('express-session');
const Router = require('./Router');
const cookieParser = require("cookie-parser");
const { urlencoded } = require('express');

const environment = process.env.NODE_ENV || 'development';

if (environment !== "development") {
  app.use(express.static(path.join(__dirname, '..', 'src')));
} else { // Production
  app.use(express.static(path.join(__dirname, '../build')));
}
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(cookieParser());

const options = {
  key: fs.readFileSync('cert/sskey.pem'),
  cert: fs.readFileSync('cert/sscert.pem')
};

const oneDay = 1000 * 60 * 60 * 24;
app.use(session({
    secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
    saveUninitialized:false,
    cookie: { maxAge: oneDay },
    resave: false
}));

console.log("Welcome to HuntDown Backend.");
var extra = "";

if (environment === "development") {
  extra = "development"
} else {
  extra = "production";
}

console.log("We are running in " + extra + " mode.");

insecure_port = process.env.INSECURE_PORT || 3001;
secure_port = process.env.SECURE_PORT || 8443;

new Router(app /*,db*/);

if (environment !== "development") {
  app.get('*', function(req, res){
    res.sendFile(path.join(__dirname, '..', 'public', 'index.html'));
  });
}

var httpServer = http.createServer(app);
var httpsServer = https.createServer(options, app);

httpServer.listen(insecure_port);
httpsServer.listen(secure_port);
