const PulsarLib = require("./PulsarLib/PulsarLib.js");
const fs = require("fs");

(async () => {
    const client = PulsarLib.InitClient("pulsar://localhost:6650")
    function test(msg){
        res=msg.Msg+"!"
        PulsarLib.SendMessage(client,msg.Id,res)
    }
    await PulsarLib.WaitEvent(client, "test", test)
})();
  