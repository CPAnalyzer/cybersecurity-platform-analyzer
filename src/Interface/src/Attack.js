import React from 'react';
import UserStore from './stores/userStore';
import AttackStore from './stores/attackStore';
import SideMenu from './components/SideMenu';
import Search from './components/Search';
import AttackOP from './components/AttackOP';
import { runInAction, makeAutoObservable } from "mobx";

import { Navigate } from "react-router-dom";
import { observer } from 'mobx-react';
import attackStore from './stores/attackStore';

class Attack extends React.Component {

 constructor(props) {
    super(props);

    this.state = {
      items: [],
      DataisLoaded: false,
      Options: false,
      attackData: props.location?.state?.attackData || null,
    };
  }
  async componentDidMount() {
    try {
      let res = await fetch('/searchAttacks', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'

        }
      });
      let result =  await res.json();
      if (result && !result.success && "error" in result) {
        runInAction(() => {
          UserStore.isLoggedIn = false;
          UserStore.loading = false;
          UserStore.username = "";
          UserStore.session_id = "";
          UserStore.admin = "false";
        });
        window.location.href = "/";
        return;
      }
      this.setState({
        items: result,
        DataisLoaded: true
      });
    }
    catch (e) {
      this.setState({
        items: [{ id: '1', name: 'error' }],
        DataisLoaded: true
      });
    }
    try {
      let res = await fetch('/isLoggedIn', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'

        }
      });
      let result = await res.json();

      if (result && result.success) {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = true;
          UserStore.username = result.username;
          UserStore.admin = result.admin;
        });
      } else {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = false;
        });
      }
    }
    catch (e) {
      runInAction(() => {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      });
    }
  }

  componentWillUnmount() {
    // Rest of the store once the view is unmounted
    AttackStore.resetAttack();
    console.log("Attack store resetted");
  }

  render() {

    if (UserStore.isLoggedIn === false && UserStore.loading === false) { //TODO FIX
      return <Navigate to='/' />;
    }

    if (!this.state.DataisLoaded) {
      <div className='Attack'>
        <div className='menu-container'>
          <div className="grid">

            <SideMenu></SideMenu>
            <h1>Loading in progress.... </h1>;

          </div>
        </div>

      </div>
    }
    return (
      <div className='Attack'>
        <div className='menu-container'>
          <div className="grid">
            <SideMenu></SideMenu>
            {!AttackStore.isRedirectNull && attackStore.attackOptions != null ? (
              <div id="attackdiv">
                <AttackOP attack={attackStore.attackOptions}></AttackOP>
              </div>
            ) : (
              <Search posts={this.state.items}></Search>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default observer(Attack);
