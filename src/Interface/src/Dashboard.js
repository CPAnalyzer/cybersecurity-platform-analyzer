import React from 'react';
import './App.css';
import { observer } from 'mobx-react';
import { Button } from 'react-bootstrap';
import 'react-pro-sidebar/dist/css/styles.css';
import SideMenu from './components/SideMenu';
import UserStore from './stores/userStore';
import { Navigate } from "react-router-dom";
import DashboardCards from './components/DashboardCards'
import { runInAction, makeAutoObservable } from "mobx";

class Dashboard extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      items: [],
      DataisLoaded: false,
      Options: false,
      dashboard: []
    };
  }
  async componentDidMount() {

    try {
      let res = await fetch('/searchAttacks', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'

        }
      });
      let result = await res.json();
      if (result && !result.success && "error" in result) {
        runInAction(() => {
          UserStore.isLoggedIn = false;
          UserStore.loading = false;
          UserStore.username = "";
          UserStore.session_id = "";
          UserStore.admin = "false";
        });
        window.location.href = "/";
        return;
      } else if (result && "error" in result) {
        this.setState({
          items: result,
        });
        return;
      }
      this.setState({
        items: result,
      });
      //console.log(this.state.items)
      //console.log(JSON.stringify(this.state.items))

      this.state.items.map((post) => {
        const reqAttacks = {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            typeAttack: post.name,
          }),
        };
        fetch('/dashboard', reqAttacks)
          .then((res) => res.json())
          .then(res => {
            //console.log(JSON.stringify(res))
            if (res[0].avgtime !== "-") {
              this.setState({
                dashboard: [...this.state.dashboard, res[0]]
              });
              console.log(JSON.stringify(this.state.dashboard))
            }
          })
          .catch((err) => console.error(err));
      }).then(() => this.setState({ DataisLoaded: true }));
    }
    catch (e) {
      this.setState({
        items: [{ id: '1', name: 'error' }],
        DataisLoaded: true
      });
    }

    try {
      let res = await fetch('/isLoggedIn', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'

        }
      });
      let result = await res.json();

      if (result && result.success) {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = true;
          UserStore.username = result.username;
          UserStore.admin = result.admin;
        });
      } else {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = false;
        });
      }
    }
    catch (e) {
      runInAction(() => {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      });
    }
  }
  renderElement() {
    if (this.state.dashboard.length === 0)
      return <div>There is no data</div>;
    return;
  }
  render() {

    if (UserStore.isLoggedIn === false && UserStore.loading === false) { //TODO FIX

      // alert("Your session has expired or you have logged out, please log in again");
      return <Navigate to='/' />;
    }


    if (!this.state.DataisLoaded) {
      <div className='Attack'>
        <div className='menu-container'>
          <div class="grid">

            <SideMenu></SideMenu>
            <h1> Pleses wait some time.... </h1>;

          </div>
        </div>

      </div>
    }
    if (this.state.dashboard.length > 0) {
      return (
        <div className='Home'>
          <div className='menu-container'>
            <div class="grid">
              <SideMenu></SideMenu>
              <div class="dashboardView">
                
                <div class="cardsDashboard" id="MContent" tabIndex="-1">

                  {this.state.dashboard.map((post) => {
                    return (
                      <div class="card">
                        <DashboardCards name={post.name} executing={post.executing} executed={post.executed} error={post.error} avgtime={post.avgtime} attackmonth={post.attackmonth}></DashboardCards>
                      </div>
                    )
                  })
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    else {
      return (
        <div className='Home'>
          <div className='menu-container'>
            <div class="grid">
              <SideMenu></SideMenu>
              <div class="dashboardView">
                <div class="noDataDashboard" id="MContent" tabIndex="-1">
                  <div class="dataInfo">There is no data</div>
                  <button  className="btnsideNoData" /*onClick={refreshPage} */ href="/#/attack">
                    Start new Attack
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
export default observer(Dashboard);