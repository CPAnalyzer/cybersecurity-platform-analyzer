import { useState, useEffect } from "react";
import { MdDeleteForever } from 'react-icons/md';
import InputField from "./components/login/InputField";
import SubmitButton from "./components/login/SubmitButton";

export default function Users(posts) {

  let users = posts.posts;
  const [dataisloaded, setdata] = useState(false);
  const [query, setQuery] = useState("");
  let [items, setItem] = useState([]);
  const [style2, setStyle2] = useState("attack-border");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [pass2, setPass2] = useState("");

  if(!dataisloaded && users.length != 0){
      setItem(users);
      setdata(true);
  }

  const resetForm = () => {
    setEmail("")
    setPass("")
    setPass2("")
  };

  const createUser2 = () => {
    if (!email) {
      alert("You must introduce an email")
      return;
    }
    if (!pass) {
      alert("You must introduce a password")
      return;
    }
    if (!pass2) {
      alert("You must introduce the passwords twice")
      return;
    }
    if ( pass != pass2) {
      alert("Passwords are different")
      return;
    }

    const requestOptions = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: email,
        password: pass,
      })
    };
    return fetch('/createUser', requestOptions)
     .then(response => response.json())
  };

  function createUser() {
    createUser2().then((response) => {
      if (response.success) {
        items.push({name: response.user});
        resetForm();
        console.log(items)
        setItem(items);
      } else {
        alert(response.msg);
      }
    })
  }
  
  const clickDelete = (name) => {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ user: name })
    };
    fetch('/deleteUser', requestOptions)
      .then(response => {
        items = items.filter(m_user => m_user.name !== name);
        setItem(items);
        console.log(items)
        if (response.ok) {
          console.log("User deleted properly")
        } else {
          console.log("Error deleting user")
        }
      })
      .catch((error) => {
        console.log('Hubo un problema con la petición Fetch:' + error.message);
      })
  }

  return (
    <div>
      <div className = "adminform">
        <h1 className = "admintitle" >Create new user</h1>
        <InputField 
          id = "admininput"
          type="text"
          placeholder="Enter email"
          value={email ? email : ""}
          onChange={(val) => setEmail(val)}
        />  <InputField 
          id = "admininput"
          type="password"
          placeholder="Enter password"
          value={pass ? pass : ""}
          onChange={(val) => setPass(val)}
        />
        <InputField className = "admininput"
          id = "admininput"
          type="password"
          placeholder="Repeat password"
          value={pass2 ? pass2 : ""}
          onChange={(val) => setPass2(val)}
        />
        <SubmitButton className="adminbtn"
          text="CREATE USER"
          onClick={() => createUser()}
        />
      </div>

      <nav >
        <ul className="users">
          {
            items.filter(post => {
              if (query === '') {
                  return post;
              } else if (post.name.toLowerCase().includes(query.toLowerCase())) {
                  return post;
              }
          }).map((post) => (
              <div id={style2}>
                <div className='textadmin'>
                  <span>User {post.name}</span>
                </div>
                <div className='deleteBtn'>
                  <MdDeleteForever onClick={() => clickDelete(post.name)} className='delete-icon' />
                </div>
              </div>
            ))}
        </ul>
      </nav>
    </div >
  )
}




