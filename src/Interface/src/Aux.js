import React from 'react';
import { observer } from 'mobx-react';
import { Route, Routes} from 'react-router-dom';
import App from './App';
import Home from './Home'
import Admin from './admin';
import Result from './Result';
import Attack from './Attack';
import Dashboard from './Dashboard';
import ResultsPage from './ResultsPage';
import ControlPanel from './ControlPanel';
import ChainedAttack from './ChainedAttack';
import ConfiguredAttacks from './ConfiguredAttacks';
import Setting from './Setting';
import 'bootstrap/dist/css/bootstrap.css';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Aux extends React.Component {

  render() {
    return(
        <div className='Aux'>
        
          <Routes>            
            <Route path="/controlPanel" element={<ControlPanel/>}/>
            <Route path="/attack" element={<Attack/>}/>
            <Route path="/chainedAttack" element={<ChainedAttack/>}/>
            <Route path="/configuredAttacks"  element={<ConfiguredAttacks/>}/>    
            <Route path="/result" element={<Result/>}/>
            <Route path="/home" element={<Home/>}/>
            <Route path="/admin" element={<Admin/>}/>
            <Route path="/" element={<App/>}/>

            <Route path="/information" element={<ResultsPage />} />
            
            <Route path="/dashboard" element={<Dashboard/>}/>
            <Route path="/Setting" element={<Setting/>}/>
            <Route path="/controlPanel" element={<ControlPanel/>}/>
            <Route path="/" element={<App/>}/>              
          </Routes>
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick={false}
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="light"
          />
        </div>
        
    );
  }
}

export default observer(Aux);
