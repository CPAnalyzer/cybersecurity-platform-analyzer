/*
File for Common Utility functions
---------------------------------

Here belong functions that are used in multiple places in the codebase.
For the sake of reusability and maintainability.

*/

import userStore from "../stores/userStore";

//******** GETTERS ********//

/**
 * Obtains a list of attacks with the specified status.
 * 
 * @param {Array} statusArray - an array of strings containing the status of the attacks to filter.
 *                              Possible values are: "notExecuted", "deleted", "executed", "executing", "error".
 * 
 * @returns {Promise<JSON>} A JSON response containing a list of attacks with the specified status.
 */
export const getAttacksByState = async (statusArray) => {
    try {
        const sanitizedStatusArray = [...new Set(statusArray.filter(status => ["notExecuted", "deleted", "executed", "executing", "error"].includes(status)))];

        const result = await fetch('/results', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                state: sanitizedStatusArray,
                session_id: userStore.session_id
            })
        });
        
        const res_json= await result.json();
        if (!res_json|| res_json.success === false) {
            console.error(`An error ocurred: ${res_json.error}`); //ELIMINAR A PRODUCCIÓ
            return [{}];
        } else {
            return res_json;
        }
    } catch (error) {
        console.error('An error ocurred when obtaining the filtered attacks: ', error); //ELIMINAR A PRODUCCIÓ
        return [{}];
    }
};

/**
 * Gets the information of the attack with the specified id.
 * 
 * @param {int} attackId Id of the attack
 * 
 * @returns {Promise<JSON>} A JSON response containing the information for the attack with the specified id.
 */
export const getAttackInfo = async (attackId) => {
    try {
        const result = await fetch('/getAttack', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ attack_id: attackId,
                                   session_id: userStore.session_id
            }),
        });

        const res_json= await result.json();
        if (!res_json|| res_json.success === false) {
            console.error(`Error in /getAttackInfo: ${res_json.error}`);
            return null;
        }
        return res_json;

    } catch (error) {
        console.error(`An error occurred in getInfoAttack for ${attackId}:`, error);
        return null;
    }
};

/**
 * Sends a request to get dashboard statistics for attackType.
 * 
 * @param {string} attackType - The type of attack to get statistics for.
 * 
 * @returns {Promise<JSON>} - The JSON response containing filtered attack data.
 */
export const getDashboardAttack = async (attackType) => {
    try {
        const result = await fetch('/dashboard', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ typeAttack: attackType,
                                   session_id: userStore.session_id
            }),
        });

        const res_json= await result.json();
        if (!res_json|| res_json.success === false) {
            console.error(`Error in /dashboard for ${attackType}: ${res_json.error}`);
            return null;
        }

        return res_json[0];

    } catch (error) {
        console.error(`An error occurred in getDashboardAttack for ${attackType}:`, error);
        return null;
    }
};

/**
 * Sends a request to get all types of attacks that have been launched
 * and then sends requests to get dashboard statistics for each type of attack.
 * 
 * Just like the Dasboard View does.
 * 
 * 
 * @returns {array[Promise<JSON>]} - An array with a JSON response containing dashboard data of all executed attacks.
 */
export const getPopularAttacks = async () => {
    try {
        const result = await fetch('/searchAttacks', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            session_id: userStore.session_id
        });

        const res_json= await result.json();
        if (!res_json|| res_json.success === false) {
            console.error(`Error in /searchAttacks: ${res_json.statusText}`);
            return [];
        }

        //calling getDashboardAttack for each attackType obtained at the same time.
        const dashboardPromises = res_json.map(attack => getDashboardAttack(attack.name));
        const dashboardResults = await Promise.all(dashboardPromises);

        //filter undefined and null values...
        const validResults = dashboardResults.filter(item => item !== null && item !== undefined);

        //returning the valid results or an empty array
        return validResults.length > 0 ? validResults : [];

    } catch (error) {
        console.error('An error occurred when obtaining the popular attacks:', error);
        return [];
    }
};

export const getRemoteSecrets = async () => {
    const requestData = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        session_id: userStore.session_id
    };

    try {
        const response = await fetch('/listSecrets', requestData);
        if (!response.ok) {
            throw new Error(`Error fetching secrets: ${response.status}`);
        }
        const data = await response.json();
        return data;
    } catch (error) {
        console.error("Error in listKeys:", error.message);
        return [];
    }
};


/**
 * Gets the configured options for the specified id of the options of the attack.
 * 
 * @param {int} attackOptionsId The Id of the attack options to get.
 * 
 * @returns {Promise<JSON>} - The JSON response containing the configured options for the attack.
 */
export const getAttackOptionsById = async (attackOptionsId) => {
    try {
        const result = await fetch('/getAttackOptionsById', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ optionsId: attackOptionsId,
                                   session_id: userStore.session_id
             })
        });
        const result_json = await result.json();
        if (result_json._id !== attackOptionsId || result_json.success === false) {
            return false;
        } else {
            return result_json;
        }
    } catch (error) {
        console.error('An error occurred when obtaining the attack options:', error);
        return false;
    }
}

/**
 * Returns the empty options form for the attack type specified in attackType
 * 
 * @param {string} attackType the type of the attack
 * 
 * @returns {Promise<JSON>} - The JSON response containing the options form for the attack.
*/
export const getAttackOptionsForm = async (attackType) => {
    try {
        const result = await fetch('/getAttackOptions', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ nameAttack: attackType })
        });
        
        const res_json= await result.json();
        if (!res_json|| res_json.success === false) {
            return false;
        } else {
            return res_json;
        }
    } catch (error) {
        console.error('An error occurred when obtaining the attack options:', error);
        return false;
    }
}

//******** OTHERS ********//
/**
 * Submits a new attack with the specified name, type and options.
 * 
 * @param {string} attackType Type of the attack (nmap, fierce, ...) //attack_name
 * @param {string} attackName Name given to the attack //attack_title
 * @param {JSON} optionsJson Options JSON
 * 
 * @returns {attack_id} The attack_id of the attack newly submitted if successful, false otherwise.
 */
export const submitAttack = async (attackType, attackName, optionsJson) => {
    try {
        console.log(`Submitting attack: attackType: ${attackType}, attackName: ${attackName}, options: ${optionsJson}`);

        const result = await fetch("/submitAttack", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                attack: attackType,
                attackName: attackName,
                options: [optionsJson],
                session_id: userStore.session_id
            })});
        const result_json = await result.json();
        console.log(result_json);
        if (result_json.attack_id) {
            console.log("Attack submitted successfully");
            return result_json;
        } else {
            console.log("An error ocurred in submitAttack:");
            return false;
        }
    } catch (error) {
      console.error('An error occurred when submitting the attack:', error);
      return false;
    }
};

/**
 * Starts the attack with the specified Id.
 * 
 * @param {int} attackId The id of the attack to start
 * 
 * @returns {bool} true if the attack was started successfully, false otherwise.
 */
export const startAttack = async (attackId) => {
    try {
        //They are not used so I don't care
        const attackType = "dummy-type";
        const attackName = "dummy-name";

        const result = await fetch('/startAttack', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                nameAttack: attackType,
                attackName: attackName,
                _id: attackId,
                session_id: userStore.session_id
        })});
        
        if (result.status === 200) {
            console.log("Attack started successfully");
            return true;
        } else {
            console.log("An error ocurred in startAttack");
            return false;
        }
    } catch (error) {
        console.error('An error occurred when starting the attack:', error);
        return false;
    }
};

/**
 * Deletes the attack with the specified name and id
 * 
 * @param {string} attackName 
 * @param {int} attackId 
 * 
 * @returns {bool} true if the attack was deleted successfully, false otherwise.
 */
export const deleteAttack = async (attackName, attackId) => {
    try {
        const response = await fetch('/deleteAttack', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ nameAttack: attackName, _id: attackId })
        });
        
        console.log(response);
        if (!response.ok || response.success === false) {
            return false;
        } else {
            return true;
        }
    } catch (error) {
        console.log('Error with fetch' + error.message);
        return false;
    }
};