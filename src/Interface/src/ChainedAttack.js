import React from "react";
import UserStore from "./stores/userStore";
import SideMenu from "./components/SideMenu";
import { observer } from "mobx-react";
import { Palette } from "./components/canvas/Palette";
import { Canvas } from "./components/canvas/Canvas";
import { PropertiesPanel } from "./components/canvas/PropertiesPanel";
import { ToastContainer } from "react-toastify";
import { Navigate } from "react-router-dom";
import { runInAction, makeAutoObservable } from "mobx"

class ChainedAttack extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      attackList: [],
      constructList: [],
      attackLoaded: false,
      constructLoaded: false,
    };
  }

  async componentDidMount() {
    try {
      let res = await fetch("/searchAttacks", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      let result = await res.json();
      if (result && !result.success && "error" in result) {
        runInAction(() => {
          UserStore.isLoggedIn = false;
          UserStore.loading = false;
          UserStore.username = "";
          UserStore.session_id = "";
          UserStore.admin = "false";
        });
        window.location.href = "/";
        return;
      }
      this.setState({
        attackList: result,
        attackLoaded: true,
      });
    } catch (e) {
      this.setState({
        attackList: [
          {
            _id: "0",
            id: "0",
            name: "Error",
            step_num: 0,
          },
        ],
        attackLoaded: true,
      });
    }
    try {
      let res = await fetch("/getAllConstructs", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      let result = await res.json();
      if (result && !result.success && "error" in result) {
        return null;
      }
      this.setState({
        constructList: result,
        constructLoaded: true,
      });
    } catch (e) {
      this.setState({
        constructList: [
          {
            construct_id: 0,
            construct_name: "error",
          },
        ],
        constructLoaded: true,
      });
    }
    try {
      let res = await fetch("/isLoggedIn", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      let result = await res.json();

      if (result && result.success) {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = true;
          UserStore.username = result.username;
          UserStore.admin = result.admin;
        });
      } else {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = false;
        });
      }
    } catch (e) {
      runInAction(() => {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      });
    }
  }

  updateConstructList = async () => {
    console.log("dentro");
    try {
      let res = await fetch("/getAllConstructs", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      let result = await res.json();
      console.log(result);

      if (result && !result.success && "error" in result) {
        return null;
      }
      this.setState({
        constructList: result,
        constructLoaded: true,
      });
    } catch (e) {
      console.log("malo");

      this.setState({
        constructList: [
          {
            construct_id: 0,
            construct_name: "error",
          },
        ],
        constructLoaded: true,
      });
    }
  };

  render() {
    if (UserStore.isLoggedIn === false && UserStore.loading === false) {
      return <Navigate to="/" />;
    }

    if (!this.state.attackLoaded && !this.state.constructLoaded) {
      <div>
        <div className="menu-container">
          <div class="grid">
            <SideMenu></SideMenu>
            <h1>Loading in progress.... </h1>;
          </div>
        </div>
      </div>;
    }

    return (
      <div>
        <div className="menu-container">
          <div className="grid">
            <SideMenu />
            <div className="canvasInterior" style={{ width: "100%" }}>
              <Palette attackList={this.state.attackList} constructList={this.state.constructList} updateConstructList={this.updateConstructList}/>
              <Canvas updateConstructList={this.updateConstructList}/>
              <PropertiesPanel/>
              <ToastContainer/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default observer(ChainedAttack);
