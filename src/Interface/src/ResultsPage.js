import React, { useEffect, useState } from 'react';
import { Navigate } from "react-router-dom";
import SideMenu from './components/SideMenu';
import UserStore from './stores/userStore';
import ResultsView from './components/ResultsView';

/**
 * Results Page View
 * -----------------
 * 
 * This is the ResultsPage view. It is responsible for displaying the results of an attack.
 * 
 * It uses the ResultsView component to render the data.
 * 
 * The component expects the attack information to be stored in the sessionStorage under the key 'attackResults'.
 * If the attack information is not found, it displays 'No data available'.
 * The component also removes the attack information from the sessionStorage when it is unmounted.
 */
export default function ResultsPage() {
    const [attackInfo, setAttackInfo] = useState(null);

    useEffect(() => {
        const data = sessionStorage.getItem('attackResults');
        if (data) {
            setAttackInfo(JSON.parse(data));
        } else {
            console.error("No data received");
        }

        return () => {
            sessionStorage.removeItem('attackResults');
        };
    }, []);

    if (UserStore.isLoggedIn === false && UserStore.loading === false) {
        // alert("Your session has expired or you have logged out, please log in again");
        return <Navigate to='/' />;
    }

    //If no data found, render no data message
    const renderNoData = () => (
        <div className="noDataDashboard">
            <div className="dataInfo">No data available</div>
        </div>
    );

    //If data found, render the results
    const renderResults = () => (
        <div id="attackdiv">
            <ResultsView posts={attackInfo} />
        </div>
    );

    return (
        <div className="result">
            <div className="menu-container">
                <div className="grid">
                    <SideMenu />
                    {!attackInfo || !attackInfo.commandsresult ? renderNoData() : renderResults()}
                </div>
            </div>
        </div>
    );
}
