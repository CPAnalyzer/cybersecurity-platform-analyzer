import { makeObservable, observable, action, computed, toJS } from "mobx";

/**
 * This class is a MobX store that manages the state of the attack form when redirecting.
 * 
 * It can save the state of a New Attack form. It will be used to redirect
 * from the Control Panel View to the Attack View with a form filled with the
 * attack data so that the view can be initialized with the attack data.
 * 
 * @class AttackStore
 * 
 * @property {string} redirectFrom - True if the store is initialize because of a redirection
 * @property {JSON} attackOptions - The attack configuration [JSON]
 */
class AttackStore {
    redirectFrom = false;
    attackOptions = null;

    constructor() {
        makeObservable(this, {
            attackOptions: observable,

            isRedirectNull: computed,

            setRedirect: action,
            resetAttack: action,
        });
    }

    // returns true if there is no redirection set
    get isRedirectNull() {
        return !this.redirectFrom;
    }

    getOptions() {
        return toJS(this.attackOptions);
    }

    setRedirect(options) {
        this.redirectFrom = true;
        this.attackOptions = options;
    }

    // Resets all values to null
    resetAttack() {
        this.redirectFrom = false;
        this.attackOptions = null;
    }
}

const attackStoreInstance = new AttackStore();
export default attackStoreInstance;