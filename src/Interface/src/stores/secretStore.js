import { makeObservable, observable, action } from "mobx";

class SecretStore {
    secrets = new Map();

    constructor() {
        makeObservable(this, {
            addSecret: action,
            deleteSecret: action,
            resetSecrets: action,
        });
    }

    deleteSecret(name) {
        this.secrets.delete(name);
    }

    getSecret(name) {
        return this.secrets.get(name);
    }

    addSecret(name, secret) {
        this.secrets.set(name, secret);
    }

    // Resets all values to null
    resetSecrets() {
        delete(this.secrets);
        this.secrets = new Map();
    }
}

const secretStoreInstance = new SecretStore();
export default secretStoreInstance;
