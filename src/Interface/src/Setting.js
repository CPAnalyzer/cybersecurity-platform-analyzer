import React from 'react';
import UserStore from './stores/userStore';
import SideMenu from './components/SideMenu';
import Settings from './components/Settings'
import { Navigate } from "react-router-dom";
import { observer } from 'mobx-react';
import Users from './users';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

class Setting extends React.Component {

 constructor(props) {
    super(props);

    this.state = {
      items: [],
      DataisLoaded: false,
      Options: false
    };
  }
  
  async componentDidMount() {
    /*
    try {
      let res = await fetch('/users', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });
      let result =  await res.json();
      if (result && !result.success && "error" in result) {
        UserStore.isLoggedIn = false;
        UserStore.loading = false;
        UserStore.username = "";
        UserStore.session_id = "";
        UserStore.admin = "false";
        window.location.href = "/";
        return;
      }
      this.setState({
        items: result,
        DataisLoaded: true
      });
    }
    catch (e) {
      this.setState({
        items: [{ id: '1', name: 'error' }],
        DataisLoaded: true
      });
    }

    try {
      let res = await fetch('/isLoggedIn', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'

        }
      });
      let result = await res.json();

      if (result && result.success) {

        UserStore.loading = false;
        UserStore.isLoggedIn = true;
        UserStore.username = result.username;
        UserStore.admin = result.admin;
      }
      else {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      }
    }
    catch (e) {
      UserStore.loading = false;
      UserStore.isLoggedIn = false;
    }*/
  }

  
  render() {

    if (UserStore.isLoggedIn === false && 
        UserStore.loading === false) { //TODO FIX

     // alert("Your session has expired or you have logged out, please log in again");
      return <Navigate to='/' />;
    }
    return (
        <div className='Attack'>
            <div className='menu-container'>             
                <div class="grid">
                    <SideMenu></SideMenu>
                    <Settings 
                      posts={this.state.items}
                    ></Settings>
                </div>
                
            </div>
        </div>
    );
  }
}
export default observer(Setting);

