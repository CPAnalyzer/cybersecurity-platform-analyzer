/*
Carousel Component
----------------

This component is in charge of rendering a carousel of cards. It receives the following props:

- data: An array of objects with the data to be displayed in the cards
- CardComponent: The component to be used to render each card.
- removeAttack: A function to remove an attack from the data list in the ControlPanel.

This component is just functional, it just serves to render the cards in a carousel.
The cards that it renders are passed as a prop, so it can be used to render any kind of card.
Be sure to pass the data in a format that the CardComponent can process correctly.
*/

import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const Carousel = ({ data, CardComponent, removeAttack }) => {

    const carouselRef = useRef(null);
    const [canScrollLeft, setCanScrollLeft] = useState(false);
    const [canScrollRight, setCanScrollRight] = useState(false);

    const updateScrollButtons = () => {
        const { scrollLeft, scrollWidth, clientWidth } = carouselRef.current;
        setCanScrollLeft(scrollLeft > 0);

        //tolerance to avoid rounding errors
        const tolerance = 1;
        setCanScrollRight(scrollLeft < scrollWidth - clientWidth -tolerance);
    };

    const scroll = (direction) => {
        const card = carouselRef.current.children[0];
        const cardWidth = card.offsetWidth;
        const style = getComputedStyle(carouselRef.current);
        const gap = parseInt(style.columnGap || style.gap, 10);
    
        //total scroll distance of a card
        const totalScroll = cardWidth + gap;
        //get the total displacement from the left
        const currentScrollLeft = carouselRef.current.scrollLeft;
    
        let finalScroll;
        if (direction === 'right') {
            //always scroll a full card to the left... could change this so that it aligns
            finalScroll = totalScroll;
        } else {
            //scrolls to the left the necessary so that the card is always aligned to the left
            const offset = currentScrollLeft % totalScroll;
            finalScroll = offset !== 0 ? -offset : -totalScroll;
        }
    
        carouselRef.current.scrollBy({
            left: finalScroll,
            behavior: 'smooth',
        });
    };

    useEffect(() => {
        const carousel = carouselRef.current;
        updateScrollButtons();
        carousel.addEventListener('scroll', updateScrollButtons);
        return () => carousel.removeEventListener('scroll', updateScrollButtons);
    }, []);

    useEffect(() => {
        updateScrollButtons();
    }, [data]);

    let message = null;
    if (data[0] === "loading") {
        message = "Loading data...";
    } else if (data.length === 0) {
        message = "No matches found";
    }

    return (
        <div className="carousel-wrapper">
            {canScrollLeft && (
            <button
                className="carousel-button b-left"
                onClick={() => scroll('left')}
                aria-label="Scroll Left"
            />
            )}
            <div className="carousel-container">
                <div className="side-margin"></div>
                <div className="cards-control-panel" ref={carouselRef}>
                {message ? (
                        <div className="carousel-no-data">
                            <p>{message}</p>
                        </div>
                    ) : (
                        data.map((item, index) => (
                            <CardComponent
                                key={item.id ? item.id : index}
                                {...item}
                                carouselRef={carouselRef}
                                removeAttack={removeAttack}
                            />
                        ))
                    )
                }
                </div>
                <div className="side-margin"></div>
            </div>
            {canScrollRight && (
            <button
                className="carousel-button b-right"
                onClick={() => scroll('right')}
                aria-label="Scroll Right"
            />
            )}
        </div>
    );
};

Carousel.propTypes = {
    data: PropTypes.array.isRequired,
    CardComponent: PropTypes.elementType.isRequired,
    removeAttack: PropTypes.func.isRequired,
};

export default Carousel;