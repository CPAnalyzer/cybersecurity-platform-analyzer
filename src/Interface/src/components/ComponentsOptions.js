import Accordion from 'react-bootstrap/Accordion'
import Badge from 'react-bootstrap/Badge'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Button from 'react-bootstrap/Button'
import Popover from 'react-bootstrap/Popover'
import Form from 'react-bootstrap/Form';
import Switch from 'react-bootstrap/Switch';
import React, { useState } from 'react';






function FormOption(option){
    const [isEnabled, setIsEnabled] = useState(false);
    const [selectedOption, setSelectedOption] = useState(""); 

    
    

    const titleOption = (
        <h5 class ="optionAttackTitle" >
            {option.title}
            {' '}
            {option.subgroup === "" || (<Badge pill bg="dark">{option.subgroup}</Badge>)}
            {'   '}
        </h5>


    )
    
    
    switch(option.formattype){
        case "text":
            return(
                <div class={"divAttackOption" + option.maingroup} key={option.title}>
                    {titleOption}
                    <div class={"formatInput " + option.formattype} >
                        <Form.Control
                            className="text-input"
                            
                        />
                    </div>
                    <OverlayTrigger trigger="hover" placement="right" overlay={popover}>
                        <Button className='informationButton'  size="sm" >i</Button>
                    </OverlayTrigger>
                </div>
            )

        break;

    case "switch":
        return(
            <div class={"divAttackOption" + option.maingroup} key={option.title}>
                    {titleOption}
                <div class="divInputOptions">
                
                    <Form.Check
                    type="switch"
                    className='switch form-switch'
                    
                    />
                
                </div>
                <OverlayTrigger trigger="hover" placement="right" overlay={popover}>
                    <Button className='informationButton'  size="sm" >i</Button>
                </OverlayTrigger>
            </div>
        )
        
        break;

    case "select":
        return(
            <div class={"divAttackOption" + option.maingroup} key={option.title}>
                {titleOption}
                <div class="divInputOptions">
                    <Form.Select aria-label="Default select example">
                        {option.selectOptions && option.selectOptions.map((modeOption, index) => (
                            <option key={index} value={modeOption}>
                                {modeOption}
                            </option>
                        ))}
                    </Form.Select>
                    
                </div>
                <OverlayTrigger trigger="hover" placement="right" overlay={popover}>
                    <Button className='informationButton'  size="sm" >i</Button>
                </OverlayTrigger>
            </div>
            
        )
        break;

    case "switch-textbox":
        return(
            <div class={"divAttackOption" + option.maingroup} key={option.title}>
                    {titleOption}
                <div class="divInputOptions">
                    <InputGroup className="form-switch-text">
                        <Form.Check 
                            aria-label="Checkbox for following text input" 
                            type="switch"
                            className="switch-st form-control"
                            onChange={handleSwitchChange}
                        />
                        <Form.Control 
                            aria-label="Text input with checkbox" 
                            className="text-st form-control"
                            disabled={!isEnabled}
                        />
                    </InputGroup>
                </div>
                <OverlayTrigger trigger="hover" placement="right" overlay={popover}>
                    <Button className='informationButton'  size="sm" >i</Button>
                </OverlayTrigger>
            </div>
            
        )
        break;

    case "multiselect":
        return(
            <div class={"divAttackOption" + option.maingroup} key={option.title}>
                    {titleOption}
                <div class="divInputOptions">
                    <Form.Select aria-label="Default select example">
                        <option>Open this select menu</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </Form.Select>
                </div>
                <OverlayTrigger trigger="hover" placement="right" overlay={popover}>
                    <Button className='informationButton'  size="sm" >i</Button>
                </OverlayTrigger> 
            </div>

        )
        break;
    case "Range":
        
        break;

    default:
        

    }

    

    



}