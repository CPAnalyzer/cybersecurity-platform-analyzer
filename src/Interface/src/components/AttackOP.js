import React, { useEffect } from "react";
//import './App.css';
import OptionGroupView from "./OptionGroupView";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import Alert from "react-bootstrap/Alert";
import { toast } from "react-toastify";
import base64 from "react-native-base64";
import { useNavigate } from 'react-router-dom';
import { submitAttack } from "../utils/commonUtils";
import AttackStore from '../stores/attackStore';

function AttackOP(props) {
  const navigate = useNavigate();  
  const [position, setPosition] = React.useState(0);
  var opt;
  if (AttackStore.redirectFrom) {
    var tmp = AttackStore.getOptions();
    opt = tmp[position];
  } else {
    opt = props.attack[position];
  }

  var groups = [];
  var Modes = null;

  const [regexArray, setRegexArr] = React.useState([]);
  const [selectedMode, setSelectedMode] = React.useState("");

  const [newstate, setState] = React.useState([false]);
  const [validated, setValidated] = React.useState(false);
  const [show, setShow] = React.useState(false);
  const [alertType, setAlertType] = React.useState("info");
  const [name, setName] = React.useState("");
  const [dismissedOpt, setDismissedOpt] = React.useState();
  const [regexout, setRegex] = React.useState();
  const [alerts, setAlerts] = React.useState();
  const [msgcon, setMsgcon] = React.useState("");
  const [valuesRequire, setValuesRequire] = React.useState({});
  const [requireOption, setRequireOption] = React.useState(new Map());
  const [conflictOption, setConflictOption] = React.useState(new Map());

  const validModes = ["dir", "dns", "vhost", "fuzz", "s3", "gcs", "tftp"];

  const handleSelectedMode = (selectedMode) => {
    if (Modes.includes(selectedMode)) { // Verificar si el modo es válido
      setSelectedMode(selectedMode);
    } else {
      console.warn(`Invalid mode: ${selectedMode}. Action ignored.`);
    }
  };

  function remove(arr, item) {
    var index = arr.indexOf(item);
    return [
      // part of the array before the given item
      ...arr.slice(0, index),
      // part of the array after the given item
      ...arr.slice(index + 1)
    ];
  }
  
  function fillGroups(opt) {
    let current = null;
    let index = 0;
    let aux = [];
    opt.options.forEach((option, iter) => {
      if (current == null) {
        current = option.maingroup;
        aux.push(option);
      } else if (current !== option.maingroup) {
        current = option.maingroup;
        groups[index] = aux;
        aux = [];
        aux.push(option);
        ++index;
      } else {
        aux.push(option);
      }
      if (iter === opt.options.length - 1) {
        groups[index] = aux;
      }
    });
  }

  const handleConflict = (title, event) => {
    let option = opt.options.find((elem) => elem.title === title);
    let value = event.target.value;
    const checked = event.target.checked;
    const requireMap = new Map(requireOption); // Copia del estado actual
    const conflictMap = new Map(conflictOption); // Copia del estado actual
    let require = [];
    let conflict = [];
    
    if(event.target.type === "checkbox") {
      value = "";
    }

    //PARAMETER REQUIRE
    if (option && option.require && option.require.length > 0) {
      if (value !== "" || event.target.type === "checkbox" && event.target.checked) {
        option.require.forEach((req) => {
          
          if (!requireMap.has(title)) {
            requireMap.set(title, []);
          }
          if (!requireMap.get(title).includes(req)) {
            requireMap.get(title).push(req);
          }
          
        });
      } else if(value === "" || !checked){
        requireMap.delete(title);
      }

      requireMap.forEach((values, title) => {
        values.forEach((req) => {
          if(!requireMap.has(req)) {
            require.push(title + " requires " + req + ".");
          }
        });
      });
    }

    //PARAMETER CONFLICT
    if (option && option.conflict && option.conflict.length > 0) {
      if (value !== "" || event.target.type === "checkbox" && event.target.checked) {
        option.conflict.forEach((con) => {
          if (!conflictMap.has(title)) {
            conflictMap.set(title, []);
          }
          if (!conflictMap.get(title).includes(con)) {
            conflictMap.get(title).push(con);
          }
        });
      } else if(value === "" || !checked){
        conflictMap.delete(title);
      }

      conflictMap.forEach((values, title) => {
        values.forEach((con) => {
          if(conflictMap.has(con)) {
            conflict.push(title + " conflicts with " + con + ".");
          }
        });
      });
    }

    const combinedMessages = [...require, ...conflict].join("\n");
    setMsgcon(combinedMessages);
    
    setRequireOption(requireMap); // Actualiza el estado
    setConflictOption(conflictMap); // Actualiza el estado
  };

  useEffect(() => {
    if (opt && opt.attackName !== "") {
      setName(opt.attackName);
    }
  }, []);

  function nextOption() {
    setPosition(position + 1);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    setValidated(false)
    
    const form = event.currentTarget;
    
    if (msgcon !== ""){
      event.preventDefault();
      event.stopPropagation();
      
      setAlertType("conflicts");
      setShow(true);
      setValidated(false);
    } else if (regexArray.length !== 0) {
      event.stopPropagation();
      setAlertType("regexOngoing");
      setShow(true);
      setValidated(false);
    } else if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
      setAlertType("danger");
      setShow(true);
      setValidated(true);
    } else if (position < props.attack.length - 1) {
      event.preventDefault();
      setRegexArr([])
      setShow(false)
      nextOption();
      setValidated(false);
    } else if (!name || name === "") {
      event.preventDefault();
      setAlertType("wrongname");
      setShow(true);
      setValidated(false);
    } else {
      setValidated(true);
      const res = await submitAttack(opt.name, name, opt);
      if (res !== false) {
        toast.success("Attack submitted successfully. You can locate it on Last Attacks section");
        navigate(`/controlPanel`);
      }
      else {
        event.stopPropagation();
        toast.error("There was an error while submitting the attack");}
    }
  };

  function handleCheckboxChange(title) {
    let option = opt.options.find((elem) => elem.title === title);
    if (option.subgroup !== "") {
      opt.options.map((element, index) => {
        if (
          element.subgroup === option.subgroup &&
          element.title !== option.title &&
          element.checkbox === true
        ) {
          opt.options[index].checkbox = false;
          setDismissedOpt(opt.options[index].title);
          setAlertType("info");
          setShow(true);
        }
        return true;
      });
    }
    if (option.regex !== "" && option.checkbox === true) {
      var support_array = regexArray;
      support_array = remove(support_array, option.title);
      if(support_array.length === 0){
        setShow(false)
      }
      setRegexArr(support_array);
    }
    option.checkbox = !option.checkbox;
    
    setState(!newstate);
  }

  const handleTextChange = (title, event) => {
    let option = opt.options.find((elem) => elem.title === title);
    if (event.target.value !== "" && option.regex !== "") {
      var reg_expression = new RegExp(base64.decode(option.regex));
      if (reg_expression.test(event.target.value)) {
        option.value = event.target.value;
        if (regexArray.find((elem) => elem === option.title)) {
          var support_array = regexArray;
          support_array = remove(support_array, option.title);
          if(support_array.length === 0){
            setShow(false)
          }
          setRegexArr(support_array);
        }
      } else {
        if (!regexArray.find((elem) => elem === option.title))
          setRegexArr([...regexArray, option.title]);
      }
    } else {
      option.value = event.target.value;
    }
  }

  const handleTextBlur = (title, event) => {
    let option = opt.options.find((elem) => elem.title === title);
    if (event.target.value !== "" && option.regex !== "") {
      var reg_expression = new RegExp(base64.decode(option.regex))
      if (!reg_expression.test(event.target.value)) {
        setAlertType("regex")
        setRegex(event.target.value)
        setShow(true)
      }
    }
  }

  const handleOptionsName = (event) => {
    opt.optionsname = event.target.value;
    setState(!newstate);
  }

  function isFirst(index) {
    if (index === 0) return "0";
    else return "1";
  }

  function getAlertType() {
    if (alertType === "regex" || alertType === "wrongname") return "danger"
    else if (alertType === "regexOngoing") return "warning"
    else if (alertType === "conflicts") return "warning"
    else return alertType
  }
  
  const capitalizeFirstLetter = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  
  fillGroups(opt);

  if (!AttackStore.redirectFrom) {
    Modes = opt.mode.options && opt.mode.options.map(option => option);
  }

  return (
    <div id="attackOptionsView">
      <div class="headerOptionsAttack">
        
          <h2 id="attackTitleText">
            {" "}
            <b>{capitalizeFirstLetter(opt.name)}</b>
          </h2>
        <FormControl
          onChange={(e) => setName(e.target.value)}
          required={true}
          placeholder="Enter the name for your attack"
          defaultValue={opt.attackName !== "" ? opt.attackName : ""}
        ></FormControl>
      </div>
      
      <Form
        id="formAttack"
        noValidate
        validated={validated}
        onSubmit={handleSubmit}
      >
        {/*
        <div id="optionsTitle">
          <h3 id="optionsTitleText">
            <b>Options {opt.name} configuration</b>
          </h3>
        </div>*/}
        
          {groups.map((options, index) => {
            return (
              <OptionGroupView
                isFirst={isFirst(index)}
                optionsname={isFirst(index) === "0" && opt.optionsname !== "" ? opt.optionsname : undefined}
                options={options}
                handleSelectedMode={(selectedMode) => handleSelectedMode(selectedMode)}
                selectedMode={selectedMode} 
                validModes={Modes}
                position={position}
                handleCheckboxChange={(title) => handleCheckboxChange(title)}
                handleTextChange={(index, event) =>
                  handleTextChange(index, event)
                }
                handleTextBlur={(index, event) =>
                  handleTextBlur(index, event)
                }
                handleConflict={(index, event) =>
                  handleConflict(index, event)
                }
                handleSubmit={(event) => handleSubmit(event)}
                handleOptionsName={(event) => handleOptionsName(event)}
                key={index}
              />
            );
          })}
          <div className="form-button">
            <Button type='submit' className="btnsideNoData">
              {position !== props.attack.length - 1 || "Submit form"}
              {position === props.attack.length - 1 || "Next"}
            </Button>
          </div>

          <div className="options_alert">
            <Alert
              show={show}
              variant={getAlertType()}
              onClose={() => setShow(false)}
              dismissible
            >
              {alertType !== "conflicts" || (
                <p>
                  {msgcon}
                </p>
              )}
              {alertType !== "info" || (
                <p>
                  Only one option within the same subgroup can be used. The
                  option <b>{dismissedOpt}</b> has been dismissed
                </p>
              )}
              {alertType !== "danger" || (
                <p>
                  You must provide all the fields from the <b>Required</b> section and the activated options that need input!
                </p>
              )}
              {alertType !== "wrongname" || (
                <p>
                  Enter a valid <b>Attack Name</b>
                </p>
              )}
              {alertType !== "regex" || (
                <p>
                  The input <b>{regexout}</b> you provided is not valid for that field. Please read the Information and use a correct value!
                </p>
              )}
              {alertType !== "regexOngoing" || (
                <p>
                  The fields{" "}
                  <b>
                    {regexArray.map((el, index) => {
                      if( index === 0){
                        return el
                      }
                      return " ," + el
                    })}
                  </b>{" "}
                  have a wrong input! Please solve the problems before submiting the form.
                </p>
              )}
            </Alert>
          </div>
      </Form>
    </div>
  );
}

export default AttackOP;
