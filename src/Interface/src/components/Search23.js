import { useState } from "react";
import React from 'react';

 const posts = [
        { id: '1', name: 'Attack1' },
        { id: '2', name: 'Attack2' },
        { id: '3', name: 'Attack3' },
        { id: '4', name: 'Attack4' },
        { id: '5', name: 'Attack5' },
        { id: '6', name: 'Attack6' },
        { id: '7', name: 'Attack7' },
        { id: '8', name: 'Attack8' },
        { id: '9', name: 'Attack9' },
        { id: '10', name: 'Attack10' },
    ];
    
class Search extends React.Component {

  
    constructor(props) {
        super(props);
   
        this.state = {
            items: [],
            DataisLoaded: false
        };
    }
   async componentDidMount() {
       
       // let [query, setQuery] = useState("");
      /*  let res = fetch('/search')
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    items: json,
                    DataisLoaded: true
                });
            })
            console.log(res.json())
        };*/
   
         handleChange = e => {
            setSearchField(e.target.value);
          };
        }
    /*const fetchData = () => {
        return fetch('/searchAttacks')
            .then((response) => response.json())
            .then((data) => console.log(data));
    }*/
   // const posts = setTimeout(function () { fetchData() }, 1000);
   
   render() {

      /*  const { DataisLoaded, items } = this.state;
        if (!DataisLoaded) return <div>
            <h1> Pleses wait some time.... </h1> </div> ;*/
    return (

        <div>
        <div className='search-container'>
            <form id="form" role="search">
                <input  onChange = {handleChange} type="search" name="q" placeholder="Search..."
                    aria-label="Search through site content" />
                <div className='svg-container'>
                    <button onclick="search()" className='searchbtn'> <svg viewBox="0 0 1024 1024"><path class="path1" d="M848.471 928l-263.059-263.059c-48.941 36.706-110.118 55.059-177.412 55.059-171.294 0-312-140.706-312-312s140.706-312 312-312c171.294 0 312 140.706 312 312 0 67.294-24.471 128.471-55.059 177.412l263.059 263.059-79.529 79.529zM189.623 408.078c0 121.364 97.091 218.455 218.455 218.455s218.455-97.091 218.455-218.455c0-121.364-103.159-218.455-218.455-218.455-121.364 0-218.455 97.091-218.455 218.455z"></path></svg></button>
                </div>

            </form>
        </div>
        <nav >
            <ul id="attacks">
                {
                    posts.filter(post => {
                        if (query === '') {
                            return post;
                        } else if (post.name.toLowerCase().includes(query.toLowerCase())) {
                            return post;
                        }
                    }).map((post) => (
                        <a>{post.name}</a>
                    ))}
            </ul>
        </nav>
    </div >

);
}
}

export default Search;
 
