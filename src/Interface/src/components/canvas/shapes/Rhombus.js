import React, { useRef, useCallback, useState } from "react";
import { RegularPolygon, Line, Arrow, Text } from "react-konva";

import {
  selectShape,
  moveShape,
  selectConnection,
  clearSelection,
  getIdShapeOnPosition,
  getselectedShapeId,
  getPositionShapeFromId,
  createConnection,
  conectionAllowed,
  geShapeConnectionsById,
  moveConection,
  getTypeAttackById,
} from "../state";
import { Anchor } from "./Anchor";
import { stageRef } from "../Canvas.js";

export function Rhombus({ id, text, isSelected, type, ...shapeProps }) {
  const shapeRef = useRef();
  const anchorRef = useRef();

  const [connectionPreview, setConnectionPreview] = useState(null);
  const [isDragging, setIsDragging] = useState(false);

  const handleSelect = useCallback(
    (event) => {
      event.cancelBubble = true;
      selectShape(id);
      setIsDragging(false);
    },
    [id]
  );

  const handleSelectConection = useCallback((event) => {
    event.cancelBubble = true;
    const key = event.currentTarget.getAttr("keyAttribute");
    clearSelection();
    selectConnection(key);
    setIsDragging(false);
  }, []);

  const handleDrag = useCallback(
    (event) => {
      moveShape(id, event);
      setIsDragging(false);
      moveConection(id);
    },
    [id]
  );

  const handleDragAct = useCallback(
    (event) => {
      moveShape(id, event);
      setIsDragging(true);
      moveConection(id);
    },
    [id]
  );

  function handleAnchorDragStart(e) {
    const position = e.target.position();
    setConnectionPreview(
      <Line
        x={position.x}
        y={position.y}
        points={createConnectionPoints(position, position)}
        stroke="black"
        strokeWidth={2}
      />
    );
  }

  function handleAnchorDragMove(e) {
    const { mousePos, position } = getShapeAndMousePositon(e);
    setConnectionPreview(
      <Line
        x={position.x}
        y={position.y}
        points={createConnectionPoints({ x: 0, y: 0 }, mousePos)}
        stroke="black"
        strokeWidth={2}
      />
    );
  }

  function handleAnchorDragEnd(e) {
    setConnectionPreview(null);
    const { mousePos, position } = getShapeAndMousePositon(e);
    const connectionTo = getIdShapeOnPosition(mousePos.x, mousePos.y, position);
    if (connectionTo === null) {
      return;
    }
    const connectionFrom = getselectedShapeId();
    const toPos = getPositionShapeFromId(connectionTo);
    const fromPos = getPositionShapeFromId(connectionFrom);
    const toType = getTypeAttackById(connectionTo);
    const fromType = getTypeAttackById(connectionFrom);

    if (
      connectionFrom !== connectionTo &&
      conectionAllowed(connectionFrom, connectionTo)
    ) {
      createConnection(
        connectionFrom,
        connectionTo,
        fromPos,
        toPos,
        fromType,
        toType
      );
    }
  }

  function getShapeAndMousePositon(e) {
    const stage = e.target.getStage(); // stage of the anchor
    const position = e.target.position(); // position of the anchor
    const pointerPosition = stage.getPointerPosition(); // position of the mouse relative to the stage
    const newScale = stage.scaleX(); // actual scale of the stage
    const mousePos = {
      x: (pointerPosition.x - stageRef.current.x()) / newScale - position.x,
      y: (pointerPosition.y - stageRef.current.y()) / newScale - position.y,
    }; // position of the mouse relative to the anchor taking into account the scale of the stage
    return { mousePos, position };
  }

  function createConnectionPoints(source, destination) {
    return [source.x, source.y, destination.x, destination.y];
  }

  function getAnchorPoints(x, y) {
    return [
      {
        x: x + shapeRef.current?.width() / 2 - 50,
        y: y - 60,
      },
      {
        x: x + shapeRef.current?.width() / 2 - 10,
        y: y + shapeRef.current?.height() - 60,
      },
      {
        x: x + shapeRef.current?.width() / 2 - 90,
        y: y + shapeRef.current?.height() - 60,
      },
    ];
  }

  function generateArrow(connection, offsetX) {
    const lineEnd = {
      x: connection.toPos.x - connection.fromPos.x + offsetX,
      y: connection.toPos.y - connection.fromPos.y - 30,
    };
    const points = createConnectionPoints({ x: 0, y: 0 }, lineEnd);

    return (
      <Arrow
        x={connection.fromPos.x + (offsetX === 50 ? 25 : -25)}
        y={connection.fromPos.y + 25}
        key={connection.key}
        points={points}
        stroke="#545fb6"
        strokeWidth={5}
        globalCompositeOperation="destination-over"
        onClick={handleSelectConection}
        keyAttribute={connection.key}
      />
    );
  }

  const attacksConnection = geShapeConnectionsById(id).map(
    (connection, index) => {
      const offsetX = index === 0 ? 100 : 50; //This is the offset of the arrow taking into account the number of arrows that the shape has
      return generateArrow(connection, offsetX);
    }
  );

  const anchorsBubbles = () => {
    return getAnchorPoints(shapeRef.current?.x(), shapeRef.current?.y()).map(
      (position, index) => (
        <Anchor
          key={`anchor-${index}`}
          innerRef={anchorRef}
          x={position.x}
          y={position.y}
          onDragStart={handleAnchorDragStart}
          onDragMove={handleAnchorDragMove}
          onDragEnd={handleAnchorDragEnd}
        />
      )
    );
  };

  const attackText = () => {
    return (
      <Text
        text={text}
        x={shapeProps.x - 66}
        y={shapeProps.y - 40}
        fontSize={20}
        fontFamily="Calibri"
        fill="black"
        align="center"
        verticalAlign="middle"
        width={shapeProps.width - 20}
        height={shapeProps.height - 20}
        listening={false}
      />
    );
  };

  return (
    <>
      <RegularPolygon
        sides={4}
        radius={50}
        onClick={handleSelect}
        onTap={handleSelect}
        onDragStart={handleSelect}
        ref={shapeRef}
        {...shapeProps}
        draggable
        onDragEnd={handleDrag}
        onDragMove={handleDragAct}
      />
      {isSelected && !isDragging && anchorsBubbles()}
      {attackText()}
      {connectionPreview}
      {attacksConnection}
    </>
  );
}
