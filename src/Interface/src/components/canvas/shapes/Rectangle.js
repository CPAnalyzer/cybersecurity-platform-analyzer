import React, { useRef, useCallback, useState } from "react";
import { Rect, Line, Arrow, Text } from "react-konva";

import {
  rectangleIsMoving,
  rectangleIsNotMoving,
  selectShape,
  moveShape,
  selectConnection,
  clearSelection,
  getIdShapeOnPosition,
  getselectedShapeId,
  getPositionShapeFromId,
  createConnection,
  conectionAllowed,
  geShapeConnectionsById,
  moveConection,
  getTypeAttackById,
} from "../state";
import { Anchor } from "./Anchor";
import { stageRef } from "../Canvas.js";

export function Rectangle({
  id,
  attack,
  attackNumber,
  isSelected,
  type,
  ...shapeProps
}) {
  const shapeRef = useRef();
  const anchorRef = useRef();

  const [connectionPreview, setConnectionPreview] = useState(null);
  const [isDragging, setIsDragging] = useState(false);

  const handleSelect = useCallback(
    (event) => {
      event.cancelBubble = true;
      selectShape(id);
      setIsDragging(false);
      rectangleIsNotMoving(id);
    },
    [id]
  );

  const handleSelectConection = useCallback(
    (event) => {
      event.cancelBubble = true;
      const key = event.currentTarget.getAttr("keyAttribute");
      clearSelection();
      selectConnection(key);
      setIsDragging(false);
      rectangleIsNotMoving(id);
    },
    [id]
  );

  const handleDrag = useCallback(
    (event) => {
      moveShape(id, event);
      setIsDragging(false);
      rectangleIsNotMoving(id);
      moveConection(id);
    },
    [id]
  );

  const handleDragAct = useCallback(
    (event) => {
      moveShape(id, event);
      setIsDragging(true);
      rectangleIsMoving(id);
      moveConection(id);
    },
    [id]
  );

  function handleAnchorDragStart(e) {
    const position = e.target.position();
    setConnectionPreview(
      <Line
        x={position.x}
        y={position.y}
        points={createConnectionPoints(position, position)}
        stroke="black"
        strokeWidth={2}
      />
    );
  }

  function handleAnchorDragMove(e) {
    const { mousePos, position } = getShapeAndMousePositon(e);
    setConnectionPreview(
      <Line
        x={position.x}
        y={position.y}
        points={createConnectionPoints({ x: 0, y: 0 }, mousePos)}
        stroke="black"
        strokeWidth={2}
      />
    );
  }

  function handleAnchorDragEnd(e) {
    setConnectionPreview(null);
    const { mousePos, position } = getShapeAndMousePositon(e);
    const connectionTo = getIdShapeOnPosition(mousePos.x, mousePos.y, position);
    if (connectionTo === null) {
      return; // The user clicked on the canvas, not on a shape
    }
    const connectionFrom = getselectedShapeId();
    const toPos = getPositionShapeFromId(connectionTo);
    const fromPos = getPositionShapeFromId(connectionFrom);
    const fromType = getTypeAttackById(connectionFrom);
    const toType = getTypeAttackById(connectionTo);

    if (
      connectionFrom !== connectionTo &&
      conectionAllowed(connectionFrom, connectionTo)
    ) {
      createConnection(
        connectionFrom,
        connectionTo,
        fromPos,
        toPos,
        fromType,
        toType
      );
    }
  }

  function getShapeAndMousePositon(e) {
    const stage = e.target.getStage(); // stage of the anchor
    const position = e.target.position(); // position of the anchor
    const pointerPosition = stage.getPointerPosition(); // position of the mouse relative to the stage
    const newScale = stage.scaleX(); // actual scale of the stage
    const mousePos = {
      x: (pointerPosition.x - stageRef.current.x()) / newScale - position.x,
      y: (pointerPosition.y - stageRef.current.y()) / newScale - position.y,
    }; // position of the mouse relative to the anchor taking into account the scale of the stage
    return { mousePos, position };
  }

  function createConnectionPoints(source, destination) {
    return [source.x, source.y, destination.x, destination.y];
  }

  function getAnchorPoints(x, y) {
    const separation = 12;
    return [
      {
        x: x + shapeRef.current?.width() / 2,
        y: y - separation,
      },
      {
        x: x + shapeRef.current?.width() / 2,
        y: y + shapeRef.current?.height() + separation,
      },
    ];
  }

  const attacksConnection = geShapeConnectionsById(id).map((connection) => {
    let lineEnd = {
      x: connection.toPos.x - connection.fromPos.x,
      y: connection.toPos.y - connection.fromPos.y - 105,
    };

    if (connection.toType === "conditionRhombus") {
      lineEnd.x -= 150 / 2; //This is needed to center the line as the 0,0 of the rhombus is in the center and not the corner
      lineEnd.y -= 150 / 2 - 25;
    }

    const points = createConnectionPoints({ x: 0, y: 0 }, lineEnd);

    return (
      <Arrow
        x={connection.fromPos.x + 75}
        y={connection.fromPos.y + 100}
        key={connection.key}
        points={points}
        stroke="#545fb6"
        strokeWidth={5}
        globalCompositeOperation="destination-over"
        onClick={handleSelectConection}
        keyAttribute={connection.key}
      />
    );
  });

  const anchorsBubbles = () => {
    if (shapeRef.current === undefined) {
      return null;
    }
    return getAnchorPoints(shapeRef.current.x(), shapeRef.current.y()).map(
      (position, index) => (
        <Anchor
          key={`anchor-${index}`}
          innerRef={anchorRef}
          x={position.x}
          y={position.y}
          onDragStart={handleAnchorDragStart}
          onDragMove={handleAnchorDragMove}
          onDragEnd={handleAnchorDragEnd}
        />
      )
    );
  };

  const attackText = () => {
    return (
      <Text
      text={attack + " " + attackNumber}
      x={shapeProps.x + 10}
      y={shapeProps.y + 10}
      fontSize={20}
      fontFamily="Calibri"
      fill="black"
      align="center"
      verticalAlign="middle"
      width={shapeProps.width - 20}
      height={shapeProps.height - 20}
      listening={false}
    />

    );
  };

  return (
    <>
      <Rect
        onClick={handleSelect}
        onTap={handleSelect}
        onDragStart={handleSelect}
        ref={shapeRef}
        {...shapeProps}
        draggable
        onDragEnd={handleDrag}
        onDragMove={handleDragAct}
      />
      {isSelected && !isDragging && anchorsBubbles()}
      {attackText()}
      {connectionPreview}
      {attacksConnection}
    </>
  );
}
