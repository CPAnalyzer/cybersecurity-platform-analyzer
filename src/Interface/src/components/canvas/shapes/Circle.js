import React, { useRef, useCallback, useState } from "react";
import { Circle as CircleShape, Line, Arrow, Text } from "react-konva";

import {
  getTypeAttackById,
  selectShape,
  moveShape,
  selectConnection,
  clearSelection,
  getIdShapeOnPosition,
  getselectedShapeId,
  getPositionShapeFromId,
  createConnection,
  conectionAllowed,
  geShapeConnectionsById,
  moveConection,
} from "../state";
import { Anchor } from "./Anchor";
import { stageRef } from "../Canvas.js";

export function Circle({ id, text, isSelected, type, ...shapeProps }) {
  const shapeRef = useRef();
  const anchorRef = useRef();

  const [connectionPreview, setConnectionPreview] = useState(null);
  const [isDragging, setIsDragging] = useState(false);

  const handleSelect = useCallback(
    (event) => {
      event.cancelBubble = true;
      selectShape(id);
      setIsDragging(false);
    },
    [id]
  );

  const handleSelectConection = useCallback((event) => {
    event.cancelBubble = true;
    const key = event.currentTarget.getAttr("keyAttribute");
    clearSelection();
    selectConnection(key);
    setIsDragging(false);
  }, []);

  const handleDrag = useCallback(
    (event) => {
      moveShape(id, event);
      setIsDragging(false);
      moveConection(id);
    },
    [id]
  );

  const handleDragAct = useCallback(
    (event) => {
      moveShape(id, event);
      setIsDragging(true);
      moveConection(id);
    },
    [id]
  );

  function handleAnchorDragStart(e) {
    const position = e.target.position();
    setConnectionPreview(
      <Line
        x={position.x}
        y={position.y}
        points={createConnectionPoints(position, position)}
        stroke="black"
        strokeWidth={2}
      />
    );
  }

  function handleAnchorDragMove(e) {
    const { mousePos, position } = getShapeAndMousePositon(e);
    setConnectionPreview(
      <Line
        x={position.x}
        y={position.y}
        points={createConnectionPoints({ x: 0, y: 0 }, mousePos)}
        stroke="black"
        strokeWidth={2}
      />
    );
  }

  function handleAnchorDragEnd(e) {
    setConnectionPreview(null);
    const { mousePos, position } = getShapeAndMousePositon(e);
    const connectionTo = getIdShapeOnPosition(mousePos.x, mousePos.y, position);
    if (connectionTo === null) {
      return;
    }
    const connectionFrom = getselectedShapeId();
    const toPos = getPositionShapeFromId(connectionTo);
    const fromPos = getPositionShapeFromId(connectionFrom);
    const fromType = getTypeAttackById(connectionFrom);
    const toType = getTypeAttackById(connectionTo);

    if (
      connectionFrom !== connectionTo &&
      conectionAllowed(connectionFrom, connectionTo)
    ) {
      createConnection(
        connectionFrom,
        connectionTo,
        fromPos,
        toPos,
        fromType,
        toType
      );
    }
  }

  function getShapeAndMousePositon(e) {
    const stage = e.target.getStage(); // stage of the anchor
    const position = e.target.position(); // position of the anchor
    const pointerPosition = stage.getPointerPosition(); // position of the mouse relative to the stage
    const newScale = stage.scaleX(); // actual scale of the stage
    const mousePos = {
      x: (pointerPosition.x - stageRef.current.x()) / newScale - position.x,
      y: (pointerPosition.y - stageRef.current.y()) / newScale - position.y,
    }; // position of the mouse relative to the anchor taking into account the scale of the stage
    return { mousePos, position };
  }

  function createConnectionPoints(source, destination) {
    return [source.x, source.y, destination.x, destination.y];
  }

  function getAnchorPoints(x, y) {
    const separation = 10;
    return [
      {
        x: x - shapeProps.radius + shapeRef.current?.width() / 2,
        y: y - shapeProps.radius + shapeRef.current?.height() + separation,
      },
    ];
  }

  const attacksConnection = geShapeConnectionsById(id).map((connection) => {
    const lineEnd = {
      x: connection.toPos.x - connection.fromPos.x + 75,
      y: connection.toPos.y - connection.fromPos.y - 55,
    };
    const points = createConnectionPoints({ x: 0, y: 0 }, lineEnd);
    return (
      <Arrow
        x={connection.fromPos.x}
        y={connection.fromPos.y + 50}
        key={connection.key}
        points={points}
        stroke="#545fb6"
        strokeWidth={5}
        globalCompositeOperation="destination-over"
        onClick={handleSelectConection}
        keyAttribute={connection.key}
      />
    );
  });

  const anchorsBubbles = () => {
    return getAnchorPoints(shapeRef.current?.x(), shapeRef.current?.y()).map(
      (position, index) => (
        <Anchor
          key={`anchor-${index}`}
          innerRef={anchorRef}
          x={position.x}
          y={position.y}
          onDragStart={handleAnchorDragStart}
          onDragMove={handleAnchorDragMove}
          onDragEnd={handleAnchorDragEnd}
        />
      )
    );
  };

  const attackText = () => {
    return (
      <Text
        text={text}
        x={shapeProps.x - shapeProps.radius + 12}
        y={shapeProps.y - shapeProps.radius + 12}
        fontSize={20}
        fill="black"
        align="center"
        fontFamily="Calibri"
        verticalAlign="middle"
        width={shapeProps.radius + 24}
        height={shapeProps.radius + 24}
        listening={false}
      />
    );
  };

  return (
    <>
      <CircleShape
        onClick={handleSelect}
        onTap={handleSelect}
        onDragStart={handleSelect}
        ref={shapeRef}
        {...shapeProps}
        draggable
        onDragEnd={handleDrag}
        onDragMove={handleDragAct}
      />
      {isSelected && !isDragging && anchorsBubbles()}
      {attackText()}
      {connectionPreview}
      {attacksConnection}
    </>
  );
}
