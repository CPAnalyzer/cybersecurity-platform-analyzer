import { useState, useEffect } from "react";
import AttackOP from './AttackOP';

export default function Search({ posts }) {
    const [query, setQuery] = useState("")
    const [clicked, setClick] = useState(false)
    const [attackName, setName] = useState("")
    const [data, setData] = useState({ name: "", optionsid: "", options: [] });
    
    const clickAttack = (name) => {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ nameAttack: name })
        };
        return fetch('/getAttackOptions', requestOptions)
            .then(response => response.json())
            .then(data => {
                return data;
            });
    }
    function getOptions(name) {
        clickAttack(name).then((options) => {
            setData(options)
            setClick(true);
        })

        clickAttack(name)

    }
    if (!clicked) {
        return (
            <div className='search-view'>
                <div className='search-container'>
                    <form id="form" role="search">
                        <input id="in" onChange={event => setQuery(event.target.value)} type="search" name="q" placeholder="Search..."
                            aria-label="Search through site content" />
                        <div className='svg-container'>
                            <button className='searchbtn' aria-label="Search"> <svg id="svgSearch" viewBox="0 0 1024 1024"><path className="path1" d="M848.471 928l-263.059-263.059c-48.941 36.706-110.118 55.059-177.412 55.059-171.294 0-312-140.706-312-312s140.706-312 312-312c171.294 0 312 140.706 312 312 0 67.294-24.471 128.471-55.059 177.412l263.059 263.059-79.529 79.529zM189.623 408.078c0 121.364 97.091 218.455 218.455 218.455s218.455-97.091 218.455-218.455c0-121.364-103.159-218.455-218.455-218.455-121.364 0-218.455 97.091-218.455 218.455z"></path></svg></button>
                        </div>
                    </form>
                </div>
                <nav className='search-nav'>
                    <ul id="attacks">
                        {
                            posts.filter(post => {
                                if (query === '') {
                                    return post;
                                } else if (post.name.toLowerCase().includes(query.toLowerCase())) {
                                    return post;
                                }
                            }).map((post) => (
                                <a href="javascript:void(0);" onClick={() => getOptions(post.name)}>{post.name}</a>
                            ))}
                    </ul>
                </nav>
            </div >
        )
    }
    else return (<div id="attackdiv">
        <AttackOP attack={data}></AttackOP>
    </div>)
}


