import React, { useState, useEffect } from "react";
import "react-sweet-progress/lib/style.css";
import { Button } from 'react-bootstrap';
import sword from "../images/sword.png";

export default function DashboardCards(props) {
    return (
        <div>
            <div className="cardDashboard">
                <div className="titolDashboard">{props.name}</div>
                <div className="bodyDashboard">
                    <div className="valuesDashboard">
                        <div><b>Executing:</b> {props.executing}</div>
                        <div><b>Executed:</b> {props.executed}</div>
                        <div><b>Errors:</b> {props.error}</div>
                        <div><b>Average Time:</b> {props.avgtime} seconds</div>
                        <div><b>Attacks this month:</b> {props.attackmonth}</div>
                    </div>
                    {/*
                                    <div class="startDashboard">
                                        <Button className="btnsideDashboard"  href="/#/attack">
                                            <img className="sword_logo" src={sword}></img>
        
                                        </Button>
                                    </div>
                                    */}
                </div>
            </div>
        </div >
    );
}
