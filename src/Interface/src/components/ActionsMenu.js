import React, { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

/**
 * Actions Menu Component
 * 
 * This function is in charge of rendering a menu with actions to be performed on a card in the Control Panel View.
 * 
 * @param {boolean} showActions - A boolean that indicates if the actions menu should be shown.
 * @param {function} setShowActions - A function to set the showActions state.
 * @param {React.RefObject} carouselRef - A reference to the carousel element.
 * @param {object} menuPosition - An object that contains the x and y position of the top-left corner of the menu and the desired width.
 * @param {array} menuOptions - An array of objects with the label to be displayed and the action to be performed when the option is clicked.
 *                              [ { label: example, action: function }, ... ]
 * 
 * Displays the actions menu in a fixed position and gets closed when clicking outside, or scrolling
 * on the carousel or the control panel view.
 * 
 */

export default function ActionsMenu({
    showActions,
    setShowActions,
    carouselRef,
    menuPosition,
    menuOptions,
}) {
    const actionsMenuRef = useRef(null);

    useEffect(() => {
        const handleOutsideClick = (event) => {
            if (
                showActions &&
                !actionsMenuRef.current?.contains(event.target)
            ) {
                setShowActions(false);
            }
        };

        const handleScroll = () => {
            if (showActions) {
                setShowActions(false);
            }
        };

        let controlPanelView = null;
        let cardsControlPanel = null;
        if (showActions) {
            document.addEventListener('mousedown', handleOutsideClick);
            
            controlPanelView = document.querySelector('.control-panel-view');
            cardsControlPanel = carouselRef.current;

            if (controlPanelView) controlPanelView.addEventListener('scroll', handleScroll, { passive: true });
            if (cardsControlPanel) cardsControlPanel.addEventListener('scroll', handleScroll, { passive: true });
        }

        return () => {
            document.removeEventListener('mousedown', handleOutsideClick);

            if (controlPanelView) controlPanelView.removeEventListener('scroll', handleScroll);
            if (cardsControlPanel) cardsControlPanel.removeEventListener('scroll', handleScroll);
        };
    }, [showActions]);

    if (!showActions) return null;

    //helper function to avoid the menu from going out of the screen on the right side
    const adjustContainerWidth = () => {
        const containerRightEdge = menuPosition.left + menuPosition.width;
        const windowRightEdge = window.innerWidth;

        if (containerRightEdge > windowRightEdge) {
          return windowRightEdge - menuPosition.left;
        }
      
        return menuPosition.width;
      };
      
      return ReactDOM.createPortal(
        <div
          className="wrapper-expanded-actions-menu"
          ref={actionsMenuRef}
          style={{
            top: menuPosition.top,
            left: menuPosition.left,
            width: adjustContainerWidth()
          }}
        >
          <div
            className="expanded-actions-menu"
            style={{
              width: menuPosition.width,
            }}
          >
            {menuOptions.map((option, index) => (
              <button key={index} onClick={() => { option.action(); setShowActions(false); }}>
                {option.label}
              </button>
            ))}
            <button onClick={() => setShowActions(false)}>Close</button>
          </div>
        </div>,
        document.body
      );
      
}

ActionsMenu.propTypes = {
    showActions: PropTypes.bool.isRequired,
    setShowActions: PropTypes.func.isRequired,
    menuPosition: PropTypes.shape({
        top: PropTypes.number.isRequired,
        left: PropTypes.number.isRequired,
        width: PropTypes.string.isRequired,
    }).isRequired,
    menuOptions: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            action: PropTypes.func.isRequired,
        })
    ).isRequired,
    carouselRef: PropTypes.object.isRequired,
};