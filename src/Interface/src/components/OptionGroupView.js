import Accordion from 'react-bootstrap/Accordion'
import Badge from 'react-bootstrap/Badge'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Button from 'react-bootstrap/Button'
import Popover from 'react-bootstrap/Popover'
import Form from 'react-bootstrap/Form';
import Select from 'react-select';
import FormGroup from 'react-bootstrap/FormGroup';
import CreatableSelect from 'react-select/creatable';
import React, { useState, useEffect } from 'react';
import SecretStore from '../stores/secretStore';

import AccordionButton from 'react-bootstrap/esm/AccordionButton'

export default function OptionGroupView(props){
    let myMap = new Map();

    props.options.map((option,index)=> {
        if (!myMap.has(option.maingroup)) {
            myMap.set(option.maingroup, []); 
        }
        myMap.get(option.maingroup).push(
            FormOption(props, option)
        );
    })
    return (   
        <>
            {
            }
            {[...myMap.entries()].map(([clave, value],index) => {
                const isMode = props.validModes && props.validModes.includes(clave); // Verifica si la clave es un modo
                const isActive = isMode ? clave === props.selectedMode : true; // Para modos, valid
                return (
                    <Accordion 
                        key={index} 
                        className={`accordionOption ${clave}`}  
                        defaultActiveKey={clave === "Required" || clave === "Global options" ? index.toString() : null}
                    >
                        <Accordion.Item 
                            className={`accordionOptionItem" ${ isActive ? "active" : "inactive"}`}
                            eventKey={index.toString()} 
                            style={{border: `none`, background:`none`}}
                        >
                            <Accordion.Button className={`accordionHeaderOption ${clave}`} disabled={!isActive}>
                                {clave}
                            </Accordion.Button>
                            <Accordion.Body className={`accordionBody`}>
                                {value}
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                );
            })}
        </>
    );
}

function FormOption(props,option) {
    const [isEnabled, setIsEnabled] = useState(false);
    const [loading, setLoading] = useState(true); // Estado para la carga
    
    const handleSwitchChange = () => {
        setIsEnabled(!isEnabled);
    };
    const keys = SecretStore.secrets;
    const popover = (
        <Popover id="popover-basic">
            <Popover.Header as="h3">{option.title}{' Details'}</Popover.Header>
            <Popover.Body>
                {option.information}
            </Popover.Body>
        </Popover>
    );
    const titleOption = (
        <h5 class ="optionAttackTitle" style={{ alignItems: "center"}}>
            {option.title}
            {' '}
            {option.subgroup === "" || (<Badge pill bg="dark">{option.subgroup}</Badge>)}
            {'   '}
        </h5>
    )

    switch(option.formattype){
        case "text":
            return(
                <div class={"divAttackOption " + option.maingroup} key={option.title} >
                    <div class="titleDiv" style={{display: "flex", alignItems:"center"}}>
                    {titleOption}
                    <OverlayTrigger trigger="hover" placement="right" overlay={popover} >
                        <Button className='informationButton'  size="sm" >i</Button>
                    </OverlayTrigger>
                    </div>
                    <div class={"formatInput " + option.formattype} >
                        <Form.Control
                            className="text-input"
                            
                            onChange={(e) => {
                                props.handleTextChange(option.title,e);
                                props.handleConflict(option.title,e);
                            }}
                            onBlur={(e) => props.handleTextBlur(option.title,e)} 
                            defaultValue={option.value}                            
                        />
                    </div>
                </div>
            )
        break;

    case "switch":
        return(
            <div class={"divAttackOption " + option.maingroup} key={props.position + option.title} >
                    <div class="titleDiv" style={{display: "flex", alignItems:"center"}}>
                    {titleOption}
                    <OverlayTrigger trigger="hover" placement="right" overlay={popover}>
                        <Button className='informationButton'  size="sm" >i</Button>
                    </OverlayTrigger>
                </div> 
                <div class="divInputOptions">
                
                    <Form.Check
                    type="switch"
                    className='switch form-switch'
                    onChange={(e) => {
                        props.handleCheckboxChange(option.title);
                        props.handleConflict(option.title,e);
                    }}
                    checked={option.checkbox === true || false}
                    />
                </div>
            </div>
        )
        break;

    case "select":
        return(
            <div class={"divAttackOption " + option.maingroup} key={props.position + option.title} >
                <div class="titleDiv" style={{display: "flex", alignItems:"center"}}>
                    {titleOption}
                    <OverlayTrigger trigger="hover" placement="right" overlay={popover} >
                        <Button className='informationButton'  size="sm" >i</Button>
                    </OverlayTrigger>
                </div>
                <div class="divInputOptions">
                    <Form.Select 
                        onChange={(event) => {
                            const selMode = `Mode: ${event.target.value}`;
                            props.handleSelectedMode(selMode); 
                            props.handleTextChange(option.title, event); 
                            props.handleConflict(option.title, event); 
                        }}
                    >   
                        {option.title === "Mode" ? (
                            // Si maingroup es "Mode"
                            option.selectOptions && option.selectOptions.map((modeOption, index) => (
                                <option key={index} value={modeOption.replace("Mode: ", "")}>
                                    {modeOption.replace("Mode: ", "")}
                                </option>
                            ))
                        ) : (
                            // Si maingroup no es "Mode"
                            option.selectOptions && option.selectOptions.map((modeOption, index) => (
                                <option key={index} value={modeOption}>
                                    {modeOption}
                                </option>
                            ))
                        )}
                    </Form.Select>
                </div>
            </div>
        )
        break;

    case "switch-textbox":
        return(
            <div class={"divAttackOption " + option.maingroup} key={props.position + option.title} >
                <div class="titleDiv" style={{display: "flex", alignItems:"center"}}>
                    {titleOption}
                    <OverlayTrigger trigger="hover" placement="right" overlay={popover} >
                        <Button className='informationButton'  size="sm" >i</Button>
                    </OverlayTrigger>
                </div>
                <div class="divInputOptions">
                    <InputGroup className="form-switch-text">
                        <Form.Check 
                            checked={option.checkbox===true} 
                            type="switch"
                            className="switch-st form-control"
                            onChange={(event) => {
                               props.handleConflict(option.title,event);
                               props.handleCheckboxChange(option.title);
                            }}
                        />
                        <Form.Control 
                            onChange={(event) => {
                               props.handleTextChange(option.title,event); 
                               props.handleConflict(option.title,event);
                            }}
                            onBlur={(e) => props.handleTextBlur(option.title,e)}
                            aria-label="Text input with checkbox" 
                            className="text-st form-control"
                            disabled={(!option.checkbox && option.needcheckbox)}
                            defaultValue={option.value}
                        />
                    </InputGroup>
                </div>
            </div>
        )
        break;
    case "secret":
        return(
            <div class={"divAttackOption " + option.maingroup} key={props.position + option.title} >
                <div class="titleDiv" style={{display: "flex", alignItems:"center"}}>
                    {titleOption}
                    <OverlayTrigger trigger="hover" placement="right" overlay={popover} >
                        <Button className='informationButton'  size="sm" >i</Button>
                    </OverlayTrigger>
                </div>
                <div class="divInputOptions">
                    <Form.Select 
                        onChange={(event) => {
                            props.handleTextChange(option.title, event); // Asegura que handleTextChange existe
                            props.handleConflict(option.title, event); // Asegura que handleConflict existe
                        }}
                    >
                        <option value="">Select a secret</option>
                        {[...keys.values()].map((secret, index) => (
                            <option key={index} value={secret.secret}>
                                {secret.secret_key}
                            </option>
                        ))}
                    </Form.Select>
                </div>
            </div>
    )
    break;

    default:
    break;

    }
}