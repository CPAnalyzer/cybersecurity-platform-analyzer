import { Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';
import React, { useState, useEffect } from 'react';
import { getRemoteSecrets } from '../utils/commonUtils';
import SecretStore from '../stores/secretStore';

function Settings() {
    const [data, setData] = useState([]); 
    const [formValues, setFormValues] = useState({
        
        key: "",
        description:"",
        secret: ""
    });
    const [showPasswords, setShowPasswords] = useState({});

    const togglePassword = (id) => {
        setShowPasswords((prevState) => ({
            ...prevState,
            [id]: !prevState[id],
        }));
    };

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormValues({
            ...formValues,
            [name]: value,
        });
    };

    useEffect(() => {
        const fetchKeys = async () => {
            const keys = await getRemoteSecrets(); 
            setData([...keys]);
        };
    
        fetchKeys(); 
    }, []);


    const existKey = async(secret_key) => {
        const requestData = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ secret: secret_key }),
        };

        try {
            const response = await fetch('/getSecret', requestData);
            if (!response.ok) {
                console.error(`Error: ${response.status}`);
                throw new Error(`Error fetching secret: ${response.status}`);
            }

            const resp = await response.json(); // El array de objetos devuelto por el backend
            console.log("Response from existKey:", resp);
            

            // Verificar si algún objeto tiene la clave igual al `secret_key`
            const exists = data.some((item) => item.secret_key === secret_key);
            console.log(exists);
            return exists;
        } catch (error) {
            console.error("Error in existKey key: ", error.message);
            return [];
        }
    };
    

    const handleDeleteSecret = async (secret_key) => {
        var accepted = window.confirm("Do you want to delete this key?");
        
        if (!accepted) {
            return;
        }

        const requestData = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ secret: secret_key }),
        };

        try {
            const response = await fetch('/deleteSecret', requestData);
            if (!response.ok) {
                console.error(`Error: ${response.status}`);
                throw new Error(`Error fetching secrets: ${response.status}`);
            }else{
                SecretStore.deleteSecret(secret_key);
                console.log("Success: Secret deleted");
                window.location.reload();
            }
            
        } catch (error) {
            console.error("Error in delete key: ", error.message);
            return [];
        }
        
    };

    const handleSubmit = async (event) => {
        event.preventDefault(); // Detiene el comportamiento predeterminado
        event.stopPropagation();
        
        const form = event.target; 
        const key = form.key.value; 
        const description = form.description.value; 
        const secret = form.secret.value; 
        
        console.log("Submitting form data: " + key + " " + description + " " + secret);

        const keyExists = await existKey(key);
        console.log(keyExists); 
        if (keyExists) {
            window.alert("A secret with this name already exists!");
            return;
        }
        
        const requestData = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(
                {
                    key: key, 
                    description: description,
                    secret: secret,
                }
            ),
        };
        
        fetch('/addSecret', requestData)
            .then(response => {
            if (response.ok) {
                SecretStore.addSecret(key, {secret: secret, description: description, secret_key: key});
                window.location.reload();
            } else {
                console.log("Error " + response.status)
            }
        })
        .catch((error) => {
          console.log('Fetch Error:' + error.message);
        })
    };

    return (
    <div id="settingsLayout">
        <div id="headerTitleSetting">
            <h1 id="attackTitleText">Secret Keys</h1>
        </div>
        <div id="divSecretKeys">
            <Form 
                onSubmit={handleSubmit} 
                id='addSecretKeyForm'>
                <Form.Group className="mb-3">
                    <Form.Label>Secret Key Name</Form.Label>
                    <Form.Control
                        type="text" 
                        placeholder="Enter Secret Key Name" 
                        name="key"
                        onChange={handleInputChange}
                        required="true"
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formGroupPassword">
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Description"
                        name="description"
                        onChange={handleInputChange}
                        required="true"
                    
                    
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formGroupPassword">
                    <Form.Label>Key</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Secret"
                        name="secret"
                        onChange={handleInputChange}
                        required="true"
                    
                    
                    />
                </Form.Group>
                <Button type="submit" className='btnsideNoData'>
                    Add Key
                </Button>
            </Form>
                <div id="divTableSecretKey">
                <Table id="tableSecretKey" striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Key</th>
                            <th>Description</th>
                            <th>Secret</th>
                            <th>Action</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((item) => (
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.secret_key}</td>
                                <td>{item.description}</td>
                                <td>
                                    {showPasswords[item.id]
                                        ? item.secret
                                        : '•'.repeat(10)}
                                </td>
                                <td>
                                    <Button
                                        variant="primary"
                                        size="sm"
                                        onClick={() => togglePassword(item.id)}
                                    >
                                        {showPasswords[item.id] ? "Hide" : "Show"}
                                    </Button>
                                </td>
                                <td >
                                    <Button 
                                        variant="danger"
                                        size="sm"
                                        onClick={() => handleDeleteSecret(item.secret_key)}
                                    >
                                        Delete
                                    </Button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
                </div>
            

        </div>
    </div>
  );
}

export default Settings;