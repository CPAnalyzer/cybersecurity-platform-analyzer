// LastAttackCard.js
import React, { useState, useRef } from 'react';
import {MdDeleteForever} from 'react-icons/md';
import {FaPlay} from 'react-icons/fa'
import { useNavigate } from 'react-router-dom';
import { Progress } from 'react-sweet-progress';
import { toast } from "react-toastify";

import { getAttackInfo, getAttackOptionsById, submitAttack, startAttack, deleteAttack } from '../../utils/commonUtils';
import AttackStore from '../../stores/attackStore';
import ActionsMenu from '../ActionsMenu';


export default function LastAttackCard(props) {
    const [showActions, setShowActions] = useState(false);
    const [menuPosition, setMenuPosition] = useState({ top: 0, left: 0, width: '100%' });
    const actionsContainerRef = useRef(null);
    const navigate = useNavigate();  
        
    //Function to mark an attack as a copy
    const calculateRename = (attackName) => {
        if (attackName.includes("copy")) {
            return attackName;
        } else {
            return `${attackName} copy`;
        }
    };

   //Intermediary functions to handle menu actions
    const handleInformation = async () => {
        const attackInfo = await getAttackInfo(props.id);
        console.log(attackInfo);

        if (attackInfo != null & attackInfo.results != null) {
            sessionStorage.setItem('attackResults', JSON.stringify(attackInfo.results));
            navigate('/information');
        } else {
            toast.error("An error ocurred while getting the attack information");
        }
    };

    const handleStartAttack = async () => {
        if (props.state === 'notExecuted') {
            const result = await startAttack(props.id);

            if (result) {
                toast.success('Attack started successfully. You can follow the progress on the Attacks in Execution section');
                if (props.removeAttack) {
                    props.removeAttack(props.id);
                }
            } else {
                toast.error('An unexpected error occurred while trying to start the attack');
            }
        } else {
            toast.error(`The attack can't be started because its not in state notExecuted`);
        }
    };

    const handleLaunchAgain = async () => {
        try {
            const attackInfo = await getAttackInfo(props.id);
            if (!attackInfo || !attackInfo.clicommands) {
                toast.error('An error occurred while trying to get the attack information');
                return;
            }

            //finds the clicommand with a valid optionsId
            const optionsId = attackInfo.clicommands.find(
                (command) => typeof command.optionsid === 'string' && command.optionsid.trim() !== ''
            )?.optionsid;
            
            if (!optionsId) {
                toast.error('No valid configurated options were found for the selected attack');
                return;
            }

            let attackOptions = await getAttackOptionsById(optionsId);
            if (!attackOptions) {
                toast.error('An error occurred while trying to get the attack options');
                return;
            }

            if (attackOptions._id) { //We eliminate the _id camp since we don't need it when sending the json
                delete attackOptions._id;
            }

            const renamed = calculateRename(attackInfo.attackname);

            const submit_res = await submitAttack(attackInfo.typeattack, renamed, attackOptions);
            
            if (submit_res.attack_id)  {
                const start_res = await startAttack(submit_res.attack_id);

                if (start_res) {
                    toast.success('Attack launched successfully. You can follow the progress on the Attacks in Execution section');
                } else {
                    toast.error('An unexpected error occurred while trying to launch the attack');
                }
            } else {
                toast.error('An error occurred while trying to submit the attack');
                return;
            }
        } catch (error) {
            toast.error('An unexpected error occurred while trying to launch the attack');
            console.error(error);
        }
    };

    const handleConfigure = async () => {
        try {
            const renamedAttack = calculateRename(props.name);
            console.log(`Configuring attack ${renamedAttack}`);
            
            const attackInfo = await getAttackInfo(props.id);
            if (!attackInfo || !attackInfo.clicommands) {
                toast.error('An error occurred while trying to get the attack information');
                console.log(attackInfo);
                return;
            }

            //Finds the clicommand with a valid optionsId
            const optionsId = attackInfo.clicommands.find(
                (command) => typeof command.optionsid === 'string' && command.optionsid.trim() !== ''
            )?.optionsid;
            
            if (!optionsId) {
                toast.error('No valid configurated options were found');
                console.log(attackInfo.clicommands);
                return;
            }

            let attackOptions = await getAttackOptionsById(optionsId);
            if (!attackOptions) {
                toast.error('An error occurred while trying to get the attack options');
                console.log(attackOptions);
                return;
            }
            
            if (attackOptions && attackOptions._id) { //We eliminate the _id camp since we don't need it in the json
                delete attackOptions._id;
            }

            //Initialize the attack store with the attack data
            attackOptions.attackName = renamedAttack;
            attackOptions.optionsname = calculateRename(attackOptions.optionsname);
            AttackStore.setRedirect([attackOptions]);
            navigate(`/attack`);
        } catch (error) {
            console.error("Error while configuring the attack:", error);
            toast.error("An unexpected error occurred. Please try again.");
        }
    };

    const handleDelete = async () => {
        if (window.confirm('Are you sure you want to delete this attack?')) {
            console.log('Deleting attack');
            const result = await deleteAttack(props.name, props.id);
            if (result) {
                toast.success('Attack deleted successfully');
                if (props.removeAttack) {
                    props.removeAttack(props.id);
                }
            } else {
                toast.error('An error ocurred while deleting the attack');
            }
        }
    };

    //Default Options Menu Actions
    const defaultOptions = [
        { label: 'Launch Again', action: handleLaunchAgain },
        { label: 'Reconfigure', action: handleConfigure },
    ];

    //Options Menu Actions for attacks in state executed
    const executedOptions = [
        { label: 'View Results', action: handleInformation },
        { label: 'Launch Again', action: handleLaunchAgain },
        { label: 'Reconfigure', action: handleConfigure },
    ];

    //Options Menu Actions for attacks in state notExecuted
    const notExecutedOptions = [
        //{ label: 'Information', action: handleInformation },
        { label: 'Start', action: handleStartAttack },
        { label: 'Reconfigure', action: handleConfigure },
    ];

    //Returns the options menu to be displayed based on the state of the attack
    const getMenuOptions = (state) => {
        switch (state) {
            case 'notExecuted':
                return notExecutedOptions;
            case 'executed':
                return executedOptions;
            default:
                return defaultOptions;
        }
    };
    
    //returns the values to pass to the Progress component
    const getProgressProps = (state, percent) => {
        switch(state) {
            case 'executed':
                return {
                    status: 'success', 
                    percent: 100
                };
            case 'executing':
                return {
                    status: 'active',
                    percent: percent
                };
            case 'error':
                return {
                    status: 'error',
                    percent: percent
                };
            case 'deleted':
                return {
                    status: 'deleted',
                    percent: 1,
                    theme: {
                        deleted: {
                            symbol: '❌',
                            color: 'grey',
                            trailColor: 'grey'
                        }
                    }
                };
            case 'notExecuted':
                return {
                    status: 'notExecuted',
                    percent: percent+1,
                    theme: {
                        notExecuted: {
                            symbol: '⋯',
                            color: '#fbc630',
                            trailColor: '#fbc630'
                        }
                    }
                };
            default:
                return {
                    status: state,
                    percent: percent
                };
        }
    };

    //sets the coordinates of the actions menu and changes its visibility
    const handleToggleActions = () => {
        if (actionsContainerRef.current) {
            const rect = actionsContainerRef.current.getBoundingClientRect();
            setMenuPosition({
                top: rect.bottom, //actions-container.y
                left: rect.left, //actions-container.x
                width: `${rect.width}px`, //actions-container width
            });
        }
        setShowActions(!showActions);
    };

    const formatState = (state) => {
        switch (state) {
            case 'notExecuted':
                return 'Not executed';
            case 'executed':
                return 'Executed';
            case 'executing':
                return 'In execution';
            case 'error':
                return 'Error';
            case 'deleted':
                return 'Deleted';
            default:
                return state;
        }
    }

    return (
        <div
            className="card-control-panel"
            style={{ position: 'relative' }}  //Ensures that the context menu is positioned relative to this div
        >
            <div className="titol-card-control-panel">{props.name}</div>
            <div className="body-card-control-panel">
                <div className="values-card-control-panel">
                    <div><b>Attack type:</b> {props.type}</div>
                    <div><b>State:</b> {formatState(props.state)}</div>
                    <div><b>Percentage:</b> {props.percent}%</div>
                </div>
            </div>
            <div className="actions-container" ref={actionsContainerRef}>
                <button
                    className="more-actions-button"
                    title="Show more actions"
                    onClick={() => handleToggleActions(!showActions)}
                >
                    More Actions
                </button>
                {props.state === "notExecuted" && (
                    <button className="start-button" title="Start attack">
                        <FaPlay
                            onClick={handleStartAttack}
                            className='play-icon'
                        />
                    </button>
                )}
                <button className="delete-button" title="Delete attack">
                    <MdDeleteForever
                        onClick={handleDelete}
                        className='delete-icon'
                    />
                </button>
            </div>
            <div className='progress-control-panel'>
                <Progress
                    {...getProgressProps(props.state, props.percent)}
                />
            </div>
            
            <ActionsMenu
                showActions={showActions}
                setShowActions={setShowActions}
                menuPosition={menuPosition}
                menuOptions={getMenuOptions(props.state)}
                carouselRef={props.carouselRef}
            />
        </div>
    );
}
