// PopularAttackCard.js
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from "react-toastify";
import { getAttackOptionsForm } from '../../utils/commonUtils';
import AttackStore from '../../stores/attackStore';

export default function PopularAttackCard(props) {
    const navigate = useNavigate();

    const handleConfigure = async () => {
        try {
            const optionsForm = await getAttackOptionsForm(props.name);
    
            if (optionsForm) {
                AttackStore.setRedirect(optionsForm);
                navigate(`/attack`);
            } else {
                toast.error("An error occurred while trying to configure the new attack instance");
            }
        } catch (error) {
            console.error("Error while configuring the attack:", error);
            toast.error("An unexpected error occurred. Please try again.");
        }
    };

    return (
        <div className="card-control-panel">
            <div className="titol-card-control-panel">{props.name}</div>
            <div className="body-card-control-panel">
                <div className="values-card-control-panel"> {/* If there is no number we place a 0 instead*/}
                    <div><b>In execution:</b> {props.executing !== '' ? props.executing : 0}</div>
                    <div><b>Executed successfully:</b> {props.executed !== '' ? props.executed : 0}</div>
                    <div><b>Executed with error:</b> {props.error !== '' ? props.error : 0}</div>
                    
                    <div>
                        <b>Average time:</b> {
                            (() => {
                                if (props.avgtime) {
                                    const times = props.avgtime.split(',').map(Number);
                                    const validTimes = times.filter(time => !isNaN(time));
                                    const total = validTimes.reduce((sum, time) => sum + time, 0);
                                    const average = validTimes.length > 0 ? (total / validTimes.length).toFixed(2) : 0;
                                    return `${average}s`;
                                } else {
                                    return "0s";
                                }
                            })()
                        }
                    </div>
                </div>
            </div>
            <div className="actions-container">
                <button
                    className="configure-button"
                    title="Configure new attack instance"
                    onClick={() => handleConfigure()}
                >
                    Configure new instance
                </button>
            </div>
        </div>
    );
}