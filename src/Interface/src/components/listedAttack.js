import {MdDeleteForever} from 'react-icons/md'
import { useState } from 'react';
import {FaPlay} from 'react-icons/fa'

const listedAttack = ({nom, id, handleDeleteAttack}) =>{
  const [style, setStyle] = useState("playBtn");
  const [style2, setStyle2] = useState("attack-border");

  const changeStyle = () => {
    console.log("Start attack clicked");
    setStyle("lds-dual-ring");
    setStyle2("attack-borderClicked")
  };
  return(
    <div id={style2}>
      <div className={style}>
        <FaPlay className="play-icon"
          onClick={changeStyle}
        />
      </div>
      <div className='textAttack'>
        <span>Attack {nom}</span>
      </div>
      <div className='deleteBtn'>
        <MdDeleteForever 
          onClick={()=>handleDeleteAttack(nom, id)}
          className='delete-icon'
        />
      </div>
    </div>
  );
};

export default listedAttack;