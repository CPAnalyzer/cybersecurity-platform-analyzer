import Attack from "./Attack";

const AttackList = ({attackList, handleDeleteAttack, handleStartAttack}) =>{
    return(
        <div className="attack-list">
            <nav >
                <ul id="attackss">
                    {attackList.length !== 0 ? attackList.map((attack, index) => {
                        console.log("Rendering attack:", attack); // Verifica cada elemento en attackList
                        return (
                            <Attack 
                                key={attack._id}
                                nom={attack.name}
                                id={attack._id}
                                handleDeleteAttack={handleDeleteAttack}
                                handleStartAttack={handleStartAttack}
                            />
                        );
                    }) : <div id="attack-border"> <div className='textAttack'><span>No attacks found ready for execution</span></div></div>}
                </ul>
            </nav>
        </div>
    );
};

export default AttackList;