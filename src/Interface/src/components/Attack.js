import {MdDeleteForever} from 'react-icons/md'
import { useState } from 'react';
import {FaPlay} from 'react-icons/fa'
const Attack = ({nom, id, handleDeleteAttack, handleStartAttack}) =>{
  const [style, setStyle] = useState("playBtn");
  const [style2, setStyle2] = useState("attack-border");

  const changeStyle = () => {
    console.log("Start attack clicked");
    setStyle("lds-dual-ring");
    setStyle2("attack-borderClicked")
  };
  return(
    <div id={style2}>
      <div className={style}>
        <FaPlay aria-label="Start attack" className="play-icon"
          onClick={()=>handleStartAttack(nom, id)}
        />
      </div>
      <div className='textAttack'>
        <span>Attack {nom}</span>
      </div>
      <div className='deleteBtn'>
        <MdDeleteForever aria-label="Delete attack"
          onClick={()=>handleDeleteAttack(nom, id)}
          className='delete-icon'
        />
      </div>
    </div>
  );
};

export default Attack;
