import React from 'react';
import { observer } from 'mobx-react';
import { toast } from "react-toastify";
import SideMenu from './components/SideMenu';
import UserStore from './stores/userStore';
import AttackList from "./components/AttackList"
import { startAttack } from './utils/commonUtils';
import { runInAction, makeAutoObservable } from "mobx";

class ConfiguredAttacks extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
        items: [],
        DataisLoaded: false,
        Options: false
     
    };
  }
  deleteAttack=(nom, id)=>{
    const newAttacks=this.state.items.filter((attack)=>attack.name !== nom);
    this.setState({items:newAttacks});
    const requestOptions = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'

        },
        body: JSON.stringify({ nameAttack: nom, _id: id })
    };
    
    fetch('/deleteAttack', requestOptions)
        .then(response =>{
          if (response.ok){
            console.log("Successfully deleted")
          } else {
            console.log("Deletion error")
          }
        })
        .catch((error)=>{
          console.log('Error with fetch' + error.message);
        })
  }
  startAttack=(name, id)=>{
    const res = startAttack(id);

    if (!res) {
      toast.error("Error starting attack");
    } else {
      const newAttacks=this.state.items.filter((attack)=>attack.name !== name);
      this.setState({items:newAttacks});
      toast.success("Attack started successfully. Check the results in the Result History");
    }
  }
  
  async componentDidMount() {
    try {
      let res = await fetch('/getNotExecutedAttacks', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
 
        }
      });
      let result = await res.json();
      if (result && !result.success && "error" in result) {
        runInAction(() => {
          UserStore.isLoggedIn = false;
          UserStore.loading = false;
          UserStore.username = "";
          UserStore.session_id = "";
          UserStore.admin = "false";
        });
        window.location.href = "/";
        return;
      } else if (result && result.success && "error" in result) {
        this.setState({
          items: [],
          DataisLoaded: true
        });
        return;
      }
      console.log(result)
      this.setState({
        items: result,
        DataisLoaded: true
      });
    }
    catch (e) {
      console.log("Error parsing result from Get Attacks: ", e)
      this.setState({
        items: [],
        DataisLoaded: true
      });
    }
 
    try {
      let res = await fetch('/isLoggedIn', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
 
        }
      });
      let result = await res.json();
 
      if (result && result.success) {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = true;
          UserStore.username = result.username;
          UserStore.admin = result.admin;
        });
      }
      else {
        runInAction(() => {
          UserStore.loading = false;
          UserStore.isLoggedIn = false;
        });
      }
    }
    catch (e) {
      runInAction(() => {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      });
    }
  }
  render() {
  //   if (UserStore.isLoggedIn == false && UserStore.loading == false) { //TODO FIX
     // alert("Your session has expired or you have logged out, please log in again");
    //  return <Navigate to='/' />;
    //}
    console.log("State items: " + this.state.items)
    return (
      <div className='InstMaquina'>
        <div className='menu-container'>
          <div class="grid">
            <SideMenu></SideMenu>
              <AttackList 
                attackList={this.state.items}
                handleDeleteAttack={this.deleteAttack}
                handleStartAttack={this.startAttack}
              />
          </div>
      </div>
    </div>
    );
  }
}

export default observer(ConfiguredAttacks);
