package helpers

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"log"

	lgk "hd-database/libGenKey"
	as "helpers/AttackSchema"
	of "helpers/OptionsFile"
	def "information-gathering/definitions"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

type LoginResponse = def.LoginResponse

type AttackSchema = as.AttackSchema

type AttackList = def.AttackList

type OptionsFile = of.OptionsFile

type NmapResponse = def.NmapResponse

type FierceResponse = def.FierceResponse

func CreateLoginJson(loginData map[string]string) map[string]interface{} {

	h_pass := lgk.GenKey(loginData["password"])

	login_json := make(map[string]interface{})
	login_json["username"] = loginData["username"]

	login_json["hash"] = hex.EncodeToString(h_pass)

	return login_json
}

func LoginSendRequestSync(client PulsarLib.PL_Client, login_json map[string]interface{}) []byte {

	req := PulsarLib.BuildMessage(login_json)

	jsonMachine := PulsarLib.SendRequestSync(client, "ui-db.login", req)

	return jsonMachine
}

func CreateGetAttacksJson(sessionID string) map[string]interface{} {

	getAttacks_json := make(map[string]interface{})
	getAttacks_json["session_id"] = sessionID

	return getAttacks_json
}

func GetAttacksSendRequestSync(client PulsarLib.PL_Client, getAttacks_json map[string]interface{}) []byte {

	getAttReq := PulsarLib.BuildMessage(getAttacks_json)

	result := PulsarLib.SendRequestSync(client, "ui-db.getAttacks", getAttReq)

	return result
}

func CreateGetOptionsJson(optionsID string, sessionID string) map[string]interface{} {

	getOptions_json := make(map[string]interface{})
	getOptions_json["optionsId"] = optionsID
	getOptions_json["session_id"] = sessionID

	return getOptions_json
}

func GetOptionsSendRequestSync(client PulsarLib.PL_Client, getOptions_json map[string]interface{}) []byte {

	getOptReq := PulsarLib.BuildMessage(getOptions_json)

	log.Println("I send the message to get the attackOptions with SendRequestSync")
	result := PulsarLib.SendRequestSync(client, "attack-db.getSingleOption", getOptReq)

	return result
}

func CreateSubmitAttackJson(sessionID string, attackTitle string, options_file []OptionsFile, attackType string) map[string]interface{} {

	submitAttack_json := make(map[string]interface{})
	submitAttack_json["session_id"] = sessionID
	submitAttack_json["attack_name"] = attackType
	submitAttack_json["attack_title"] = attackTitle
	submitAttack_json["options"] = options_file

	return submitAttack_json
}

func SubmitAttackSendRequestSync(client PulsarLib.PL_Client, submitAttack_json map[string]interface{}) []byte {

	subAttackReq := PulsarLib.BuildMessage(submitAttack_json)

	result := PulsarLib.SendRequestSync(client, "ui-db.AttackSubmission", subAttackReq)

	return result
}

func CreateStartAttackJson(attackName string, attackId string, sessionID string) map[string]interface{} {

	startAttack_json := make(map[string]interface{})
	startAttack_json["attack_name"] = attackName
	startAttack_json["_id"] = attackId
	startAttack_json["session_id"] = sessionID

	return startAttack_json
}

func StartAttackSendAsyncRequest(client PulsarLib.PL_Client, startAttack_json map[string]interface{}) []byte {

	startAttReq := PulsarLib.BuildMessage(startAttack_json)

	var response []byte
	// ASYNC REQUEST
	PulsarLib.SendRequestAsync(client, "ui-db.startAttack", startAttReq, func(payload []byte) {
		response = payload
	})

	log.Println("The attack is executing")

	// getAttacks
	getAttacks_json := make(map[string]interface{})
	getAttacks_json["session_id"] = startAttack_json["session_id"]

	// temporary because "step" is not updating while the attack is executing
	results := GetResultsSendRequestSync(client, getAttacks_json)

	var objects []map[string]interface{}
	err := json.Unmarshal(results, &objects)
	if err != nil {
		log.Println("error unmashrshal after getAttacksSendRequestSync")
		log.Println(err)
	}

	// Search for the object with id = attackId
	object, err := findObjectByID(objects, startAttack_json["_id"].(string))
	if err != nil {
		log.Println(err)
	}

	if object != nil {
		log.Println("State: " + object["state"].(string))

		if object["state"] == "executed" {
			log.Println("The attack is state: executed")
		}
	}

	return response
}

func CreateGetResultJson1(attackName string, attackId string, sessionID string) map[string]interface{} {

	getResult_json := make(map[string]interface{})
	getResult_json["attack_name"] = attackName
	getResult_json["attack_id"] = attackId
	getResult_json["session_id"] = sessionID

	return getResult_json
}

func CreateGetResultJson2(attackName string, attackId string, sessionID string) map[string]interface{} {

	getResult_json := make(map[string]interface{})
	getResult_json["attack_name"] = attackName
	getResult_json["_id"] = attackId
	getResult_json["session_id"] = sessionID

	return getResult_json
}

func GetResultSendRequestSync(client PulsarLib.PL_Client, getResult_json map[string]interface{}) map[string]interface{} {

	getResReq := PulsarLib.BuildMessage(getResult_json)

	log.Println("I send the message to get the result with SendRequestSync")
	result := PulsarLib.SendRequestSync(client, "ui-db.getResult", getResReq)

	resultJson := make(map[string]interface{})
	json.Unmarshal(result, &resultJson)

	return resultJson
}

func GetResultsSendRequestSync(client PulsarLib.PL_Client, getResult_json map[string]interface{}) []byte {

	getResReq := PulsarLib.BuildMessage(getResult_json)

	log.Println("I send the message to get the results with SendRequestSync")
	result := PulsarLib.SendRequestSync(client, "ui-db.getAttacks", getResReq)

	return result
}

func findObjectByID(objects []map[string]interface{}, id string) (map[string]interface{}, error) {

	for _, object := range objects {
		if object["id"] == id {
			return object, nil
		}
	}

	return nil, errors.New("object with id " + id + " not found")
}

func FindAttackByAttackName(attacks AttackList, attackName string) (AttackSchema, error) {

	for _, attack := range attacks {

		if attack.AttackName == attackName {
			return attack, nil
		}
	}

	var emptyAttack AttackSchema

	return emptyAttack, errors.New("attack with attackname " + attackName + " not found")
}
