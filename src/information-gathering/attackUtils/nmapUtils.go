package attackUtils

import (
	"encoding/json"
	"log"

	"github.com/jung-kurt/gofpdf"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"

	def "information-gathering/definitions"
	helpers "information-gathering/helpers"
)

type NmapResponse = def.NmapResponse

func NmapManager(client PulsarLib.PL_Client, sessionID string, ipTarget string) (NmapResponse, interface{}) {

	// just to test
	attackNameNmap := "nmap"

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json := helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks := helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attackList AttackList
	json.Unmarshal(configuredAttacks, &attackList)

	attackNmap, err := helpers.FindAttackByAttackName(attackList, attackNameNmap)

	if err != nil {
		log.Println(err)
	}

	ip := ipTarget

	// get the options
	var nmapOptionsFile OptionsFile
	getOptions_json := helpers.CreateGetOptionsJson("nmap01", sessionID)
	nmapOptionsByte := helpers.GetOptionsSendRequestSync(client, getOptions_json)

	err = json.Unmarshal(nmapOptionsByte, &nmapOptionsFile)

	if err != nil {
		log.Println(err)
	}

	var nmapOptionsList []OptionsFile
	nmapOptionsList = append(nmapOptionsList, nmapOptionsFile)

	// edit attackSchema and OptionsFile
	name := "nmap " + ip
	attackNmap.AttackName = name
	options := ip + " -A -oX"
	attackNmap.CliCommands[0].Options = options
	nmapOptionsList[0].Options[0].Value = ip
	nmapOptionsList[0].Options[10].Flag = "-Pn"
	nmapOptionsList[0].Options[10].Checkbox = true
	nmapOptionsList[0].Options[4].Checkbox = false

	// create submitAttack_json and submit the attack to have it configured
	submitAttack_json := helpers.CreateSubmitAttackJson(sessionID, attackNmap.AttackName, nmapOptionsList, "nmap")
	helpers.SubmitAttackSendRequestSync(client, submitAttack_json)

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json = helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks = helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attList AttackList
	json.Unmarshal(configuredAttacks, &attList)

	nmapAttack, err := helpers.FindAttackByAttackName(attList, name)

	if err != nil {
		log.Println(err)
	}

	// create startAttack_json and execute it
	startAttack_json := helpers.CreateStartAttackJson(nmapAttack.AttackName, nmapAttack.Id, sessionID)
	helpers.StartAttackSendAsyncRequest(client, startAttack_json)

	// create getResult_json and get the results
	getResult_json := helpers.CreateGetResultJson1(nmapAttack.AttackName, nmapAttack.Id, sessionID)
	results := helpers.GetResultSendRequestSync(client, getResult_json)

	log.Println("RESULT")
	log.Println(results)

	var table interface{}

	// Process the result and return it
	return innerProcessorNmap(results, table)
}

func innerProcessorNmap(results map[string]interface{}, table interface{}) (NmapResponse, interface{}) {

	var response NmapResponse

	for _, indiv_attack := range results["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {

			table = indiv_attack.(map[string]interface{})["table"]
			log.Println("TABLE")
			log.Println(table)

			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})

			data, err := json.Marshal(resultsjson)
			if err != nil {
				log.Println(err)
			}

			err = json.Unmarshal(data, &response)
			if err != nil {
				log.Println(err)
			}
		}
	}

	return response, table
}

func NmapWriteOnReport(input interface{}, report *gofpdf.Fpdf) *gofpdf.Fpdf {

	data := input.([]interface{})

	for _, item := range data {
		log.Println("item")
		log.Println(item)
		itemData := item.(map[string]interface{})["item"].(map[string]interface{})
		rowData := itemData["row"].([]interface{})
		for i, row := range rowData {
			if i == 0 {
				continue
			}
			columns := row.(map[string]interface{})["columns"].([]interface{})
			for _, column := range columns {
				// Access the "value" field within each column
				value := column.(map[string]interface{})["value"].(string)
				report.SetFillColor(255, 255, 255)
				report.CellFormat(47.5, 7, value, "1", 0, "", true, 0, "")
			}
			report.Ln(-1)
		}
	}

	return report
}

/*
// Iterates between all the ips found and submits, starts and retrieve the results of the attack for every of them
func Iterator(client PulsarLib.PL_Client, sessionID string, list []map[string]string, attack_template AttackSchema, options_file []OptionsFile, report *gofpdf.Fpdf) (map[string][]string, *gofpdf.Fpdf) {

	listIpPorts := make(map[string][]string)

	for _, item := range list {

		ip := item["ip"]
		report = helpers.AddSubsectionToReport(report, item["ip"]+"  DNS: "+item["dns"])
		report = helpers.AddContentToReport(report, "The following are the traceroute results, showing the network path and the intermediate routers encountered during the journey to the destination host.", false, "")
		report = helpers.AddContentToReport(report, "----------------------------------------------------------------------------------------------", false, "")
		result := TracerouteManager(client, sessionID, item["ip"])

		var i = 1
		for _, item := range result {
			report = helpers.AddContentToReport(report, item, true, strconv.Itoa(i)+" ")
			i++
		}
		i = 0
		report = helpers.AddContentToReport(report, "----------------------------------------------------------------------------------------------", false, "")

		// edit attackSchema and OptionsFile
		name := "nmap " + ip
		attack_template.AttackName = name
		options := ip + " -A -oX"
		attack_template.CliCommands[0].Options = options
		options_file[0].Options[0].Value = ip
		options_file[0].Options[10].Flag = "-Pn"
		options_file[0].Options[10].Checkbox = true
		options_file[0].Options[4].Checkbox = false

		// create submitAttack_json
		submitAttack_json := helpers.CreateSubmitAttackJson(sessionID, attack_template.AttackName, options_file, "nmap")

		// submit the attack and its options to configure them and save in the db
		helpers.SubmitAttackSendRequestSync(client, submitAttack_json)

		// retrieve the configured attacks to get the _id of the attack i want to start
		getAttacks_json := helpers.CreateGetAttacksJson(sessionID)

		configuredAttacks := helpers.GetAttacksSendRequestSync(client, getAttacks_json)

		var attackList AttackList
		json.Unmarshal(configuredAttacks, &attackList)

		nmapAttack, err := helpers.FindAttackByAttackName(attackList, name)

		if err != nil {
			log.Println(err)
		}

		// create startAttack_json
		startAttack_json := helpers.CreateStartAttackJson(nmapAttack.AttackName, nmapAttack.Id, sessionID)

		// start the attack
		helpers.StartAttackSendAsyncRequest(client, startAttack_json)

		// create getResult_json
		getResult_json := helpers.CreateGetResultJson1(nmapAttack.AttackName, nmapAttack.Id, sessionID)

		// Get the result of the attack
		results := make(map[string]interface{})
		results = helpers.GetResultSendRequestSync(client, getResult_json)

		log.Println("RESULT")
		log.Println(result)

		var response NmapResponse
		var table interface{}

		for _, indiv_attack := range results["commandsresult"].([]interface{}) {

			if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {

				table = indiv_attack.(map[string]interface{})["table"]
				log.Println("TABLE")
				log.Println(table)

				resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})

				data, err := json.Marshal(resultsjson)
				if err != nil {
					log.Println(err)
				}

				err = json.Unmarshal(data, &response)
				if err != nil {
					log.Println(err)
				}
			}
		}

		log.Println("SCAN OF ")
		log.Println("ip: " + ip)
		log.Println("hostname: " + response.NmapRun.Host.HostNames.HostName.Name)

		var performHping3 = false
		if response.NmapRun.Host.Ports.Port != nil {
			report = helpers.AddContentToReport(report, "Presented below are the port scanning results, providing details about protocols, ports, and other pertinent information discovered on the target host or network.", false, "")
			performHping3 = true
		}

		var performNikto = false
		var performNiktoSSL = false
		for _, port := range response.NmapRun.Host.Ports.Port {

			if port.State.State == "open" {

				listIpPorts[ip] = append(listIpPorts[ip], port.PortID, port.Protocol)

				if port.PortID == "80" {
					performNikto = true
				}

				if port.PortID == "443" {
					performNiktoSSL = true
				}
			}
		}

		nmapWriteOnReport(table, report)

		if performNikto {
			report = NiktoManager(client, sessionID, ip, false, report)
		}

		if performNiktoSSL {
			report = NiktoManager(client, sessionID, ip, true, report)
		}

		if performHping3 {
			report = HpingManager(client, sessionID, ip, report)
		}

	}

	return listIpPorts, report
}
*/
