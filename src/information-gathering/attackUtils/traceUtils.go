package attackUtils

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"

	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"

	helpers "information-gathering/helpers"
)

func TracerouteManager(client PulsarLib.PL_Client, sessionID string, ipTarget string) map[int]string {

	// just to test
	attackNameTrace := "trace"

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json := helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks := helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attackList AttackList
	json.Unmarshal(configuredAttacks, &attackList)

	attackTrace, err := helpers.FindAttackByAttackName(attackList, attackNameTrace)

	if err != nil {
		log.Println(err)
	}

	ip := ipTarget

	// get the options related to the attack
	var traceOptionsFIle OptionsFile
	getOptions_json := helpers.CreateGetOptionsJson("traceroute01", sessionID)
	traceOptionsByte := helpers.GetOptionsSendRequestSync(client, getOptions_json)

	err = json.Unmarshal(traceOptionsByte, &traceOptionsFIle)

	if err != nil {
		log.Println(err)
	}

	var traceOptionsList []OptionsFile
	traceOptionsList = append(traceOptionsList, traceOptionsFIle)

	// edit attackSchema and OptionsFile
	name := "traceroute " + ip
	attackTrace.AttackName = name
	options := ip + " -4 "
	attackTrace.CliCommands[0].Options = options
	traceOptionsList[0].Options[0].Value = ip

	// create submitAttack_json and submit the attack to have it configured
	submitAttack_json := helpers.CreateSubmitAttackJson(sessionID, attackTrace.AttackName, traceOptionsList, "traceroute")
	helpers.SubmitAttackSendRequestSync(client, submitAttack_json)

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json = helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks = helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attList AttackList
	json.Unmarshal(configuredAttacks, &attList)

	traceAttack, err := helpers.FindAttackByAttackName(attList, name)

	if err != nil {
		log.Println(err)
	}

	// create startAttack_json and execute it
	startAttack_json := helpers.CreateStartAttackJson(traceAttack.AttackName, traceAttack.Id, sessionID)
	helpers.StartAttackSendAsyncRequest(client, startAttack_json)

	// create getResult_json get the result of the attack
	getResult_json := helpers.CreateGetResultJson1(traceAttack.AttackName, traceAttack.Id, sessionID)
	result := helpers.GetResultSendRequestSync(client, getResult_json)

	log.Println("RESULT")
	log.Println(result)

	// Process the result and return it
	return innerProcessorTrace(result)
}

func innerProcessorTrace(result map[string]interface{}) map[int]string {

	resultMap := make(map[int]string)

	if resultOutput, ok := result["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {

		resultJson, err := base64.StdEncoding.DecodeString(resultOutput)
		if err != nil {
			log.Println("Error decoding base64:", err)
		}
		output := string(resultJson)

		lines := strings.Split(output, "\n")

		for i, line := range lines {
			if i != 0 && !strings.Contains(line, "*") {
				parts := strings.Fields(line)
				if len(parts) >= 2 {
					resultMap[i] = strings.Join(parts[1:], " ")
				}
			}
		}

		log.Println("resultmap")
		log.Println(resultMap)
	}

	return resultMap
}
