package attackUtils

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"

	"github.com/jung-kurt/gofpdf"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"

	helpers "information-gathering/helpers"
)

func NiktoManager(client PulsarLib.PL_Client, sessionID string, ipTarget string, sslEnabled bool, report *gofpdf.Fpdf) *gofpdf.Fpdf {

	// just to test
	attackNameNikto := "nikto"

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json := helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks := helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attackList AttackList
	json.Unmarshal(configuredAttacks, &attackList)

	attackNikto, err := helpers.FindAttackByAttackName(attackList, attackNameNikto)

	if err != nil {
		log.Println(err)
	}

	ip := ipTarget

	var niktoOptionsFile OptionsFile
	getOptions_json := helpers.CreateGetOptionsJson("nikto01", sessionID)
	niktoOptionsByte := helpers.GetOptionsSendRequestSync(client, getOptions_json)

	err = json.Unmarshal(niktoOptionsByte, &niktoOptionsFile)

	if err != nil {
		log.Println(err)
	}

	var niktoOptionsList []OptionsFile
	niktoOptionsList = append(niktoOptionsList, niktoOptionsFile)

	// edit attackSchema and OptionsFile
	name := "nikto " + ip
	attackNikto.AttackName = name
	options := "-h " + ip + "-timeout 300 "
	attackNikto.CliCommands[1].Options = options
	niktoOptionsList[0].Options[0].Value = ip
	niktoOptionsList[0].Options[11].Value = "300"
	niktoOptionsList[0].Options[11].Checkbox = true
	if sslEnabled {
		name += " ssl"
		niktoOptionsList[0].Options[2].Checkbox = true
	}
	attackNikto.AttackName = name

	// create submitAttack_json and submit the attack to have it configured
	submitAttack_json := helpers.CreateSubmitAttackJson(sessionID, attackNikto.AttackName, niktoOptionsList, "nikto")
	helpers.SubmitAttackSendRequestSync(client, submitAttack_json)

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json = helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks = helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	log.Println("configuredAttacks")
	log.Println(string(configuredAttacks))

	var attList AttackList
	json.Unmarshal(configuredAttacks, &attList)

	niktoAttack, err := helpers.FindAttackByAttackName(attList, name)

	if err != nil {
		log.Println(err)
	}

	// create startAttack_json and execute it
	startAttack_json := helpers.CreateStartAttackJson(niktoAttack.AttackName, niktoAttack.Id, sessionID)
	helpers.StartAttackSendAsyncRequest(client, startAttack_json)

	// create getResult_json and get the result of the attack
	getResult_json := helpers.CreateGetResultJson1(niktoAttack.AttackName, niktoAttack.Id, sessionID)
	result := helpers.GetResultSendRequestSync(client, getResult_json)

	log.Println("RESULT")
	log.Println(result)

	// Analyze the results
	return niktoWriteOnReport(result, report)
}

func niktoWriteOnReport(result map[string]interface{}, report *gofpdf.Fpdf) *gofpdf.Fpdf {

	log.Println("RESULT NIKTO")
	log.Println(result)
	if base64Output, ok := result["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
		}
		output := string(outputBytes)

		lines := strings.Split(output, "\n")
		if len(lines) > 7 {
			lines = lines[3:]
			lines = lines[:len(lines)-4]
		}

		report = helpers.AddContentToReport(report, "During the port scanning phase, a web server was identified and it was analyzed. Valuable information regarding its configuration, security, and potential areas of concern were found.", false, "")
		for _, line := range lines {
			report = helpers.AddContentToReport(report, line, true, " - ")
		}
	}

	return report
}
