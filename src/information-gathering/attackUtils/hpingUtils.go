package attackUtils

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"

	"github.com/jung-kurt/gofpdf"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"

	helpers "information-gathering/helpers"
)

func HpingManager(client PulsarLib.PL_Client, sessionID string, ipTarget string, report *gofpdf.Fpdf) *gofpdf.Fpdf {

	// just to test
	attackNameHping := "hping3"

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json := helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks := helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	var attackList AttackList
	json.Unmarshal(configuredAttacks, &attackList)

	attackHping, err := helpers.FindAttackByAttackName(attackList, attackNameHping)

	if err != nil {
		log.Println(err)
	}

	ip := ipTarget

	getOptions_json := helpers.CreateGetOptionsJson("hping01", sessionID)
	hpingOptionsByte := helpers.GetOptionsSendRequestSync(client, getOptions_json)

	var hpingOptionsFile OptionsFile
	err = json.Unmarshal(hpingOptionsByte, &hpingOptionsFile)

	if err != nil {
		log.Println(err)
	}

	var hpingOptionsList []OptionsFile
	hpingOptionsList = append(hpingOptionsList, hpingOptionsFile)

	// edit attackSchema and OptionsFile
	name := "hping3 " + ip
	attackHping.AttackName = name
	options := ip + " --count 10 --icmp "
	attackHping.CliCommands[0].Options = options
	hpingOptionsList[0].Options[0].Value = ip
	hpingOptionsList[0].Options[1].Value = "10"
	hpingOptionsList[0].Options[1].Checkbox = true
	hpingOptionsList[0].Options[10].Checkbox = true

	// create submitAttack_json and submit the attack to have it configured
	submitAttack_json := helpers.CreateSubmitAttackJson(sessionID, attackHping.AttackName, hpingOptionsList, "hping3")
	helpers.SubmitAttackSendRequestSync(client, submitAttack_json)

	// retrieve the configured attacks to get the _id of the attack i want to start
	getAttacks_json = helpers.CreateGetAttacksJson(sessionID)
	configuredAttacks = helpers.GetAttacksSendRequestSync(client, getAttacks_json)

	log.Println("configuredAttacks")
	log.Println(string(configuredAttacks))

	var attList AttackList
	json.Unmarshal(configuredAttacks, &attList)

	hpingAttack, err := helpers.FindAttackByAttackName(attList, name)

	if err != nil {
		log.Println(err)
	}

	// create startAttack_json and execute it
	startAttack_json := helpers.CreateStartAttackJson(hpingAttack.AttackName, hpingAttack.Id, sessionID)
	helpers.StartAttackSendAsyncRequest(client, startAttack_json)

	// create getResult_json and get the result of the attack
	getResult_json := helpers.CreateGetResultJson1(hpingAttack.AttackName, hpingAttack.Id, sessionID)
	result := helpers.GetResultSendRequestSync(client, getResult_json)

	log.Println("RESULT")
	log.Println(result)

	// Analyze the results
	return hpingWriteOnReport(result, report)
}

func hpingWriteOnReport(result map[string]interface{}, report *gofpdf.Fpdf) *gofpdf.Fpdf {

	log.Println("RESULT HPING3")
	log.Println(result)
	if base64Output, ok := result["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
		}
		output := string(outputBytes)

		lines := strings.Split(output, "\n")

		report = helpers.AddContentToReport(report, "Hping results are the following", false, "")
		for _, line := range lines {
			report = helpers.AddContentToReport(report, line, true, " - ")
		}
	}
	return report
}
