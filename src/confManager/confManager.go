package confManager

import (
	"encoding/json"
	"log"
	"os"
)

var conf_data = map[string]string{}

var default_file = "/etc/huntdown/environment.json"

func LoadConfig(file string) ConfigContext {
	var confEnvironment ConfigContext

	err := LoadConfigFromFile(file)
	if err != nil {
		log.Fatalf("Could not load the configuration file: %v", err)
	}

	mongo_host := GetValue("mongo_host")
	pulsar_host := GetValue("pulsar_host")
	pulsar_port := GetValue("pulsar_port")
	mongo_port := GetValue("mongo_port")
	mongo_user := GetValue("mongo_user")
	mongo_password := GetValue("mongo_password")

	confEnvironment.DatabaseUsername = mongo_user
	confEnvironment.DatabasePassword = mongo_password
	confEnvironment.DatabaseHost = mongo_host
	confEnvironment.DatabasePort = mongo_port
	confEnvironment.PulsarHost = pulsar_host
	confEnvironment.PulsarPort = pulsar_port

	return confEnvironment
}

func LoadDefaultConfig() error {
	for _, file := range []string{default_file, "../config/environment.json", "../../config/environment.json",
		"../../../config/environment.json"} {
		if _, err := os.Stat(file); err == nil {
			default_file = file
			break
		}
	}
	return LoadConfigFromFile(default_file)
}

func LoadConfigFromFile(file string) error {
	data, err := os.ReadFile(file)
	if err != nil {
		log.Println("Error loading file: " + err.Error())
		return err
	}
	json.Unmarshal([]byte(data), &conf_data)
	return nil
}

func GetValue(key string) string {
	return conf_data[key]
}

func SetDefaultFile(file string) bool {
	if _, err := os.Stat(file); err != nil {
		return false
	}
	default_file = file
	return true
}
