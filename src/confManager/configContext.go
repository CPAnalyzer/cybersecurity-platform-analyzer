package confManager

type ConfigContext struct {
	DatabaseHost     string
	DatabasePort     string
	DatabaseUsername string
	DatabasePassword string
	PulsarHost       string
	PulsarPort       string
}
