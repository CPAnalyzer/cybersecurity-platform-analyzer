package GeneralHelper

import (
	"encoding/hex"
	"encoding/json"
	"os"

	lgk "hd-database/libGenKey"

	log "github.com/sirupsen/logrus"
	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func LoginWithCredentialFile(client PulsarLib.PL_Client, filename string) (int, error) {
	cred_raw, err := os.ReadFile(filename)
	if err != nil {
		log.Println("Error reading credentials file")
		return -1, err
	}
	var cred Credentials
	json.Unmarshal(cred_raw, &cred)
	return Login(client, cred.Username, cred.Password)
}

func Login(client PulsarLib.PL_Client, username string, password string) (int, error) {
	var err error
	login_json := make(map[string]interface{})
	login_json["username"] = username
	h_pass := lgk.GenKey(password)
	login_json["hash"] = hex.EncodeToString(h_pass)

	res := PulsarLib.BuildMessage(login_json)
	jsonMachine := PulsarLib.SendRequestSync(client, "ui-db.login", res)
	login_data := make(map[string]interface{})

	err = json.Unmarshal(jsonMachine, &login_data)
	if err != nil {
		log.Println("Error unmarshalling login data")
		return -1, err
	}

	var session_id int
	session_id, err = GetSession_id(login_data)

	if err != nil {
		log.Println("Error getting session_id")
		return -2, err
	}
	return session_id, nil
}

func Logout(client PulsarLib.PL_Client, session_id int) {
	query := make(map[string]interface{})
	query["session_id"] = session_id
	res := PulsarLib.BuildMessage(query)
	PulsarLib.SendRequestSync(client, "ui-db.logout", res)
}
