package Machine_definitions

type Machine_definitions struct {
	MachineName       string `json:"machinename"`
	Username          string `json:"username"`
	Password          string `json:"password"`
	Persistent        bool   `json:"persistent"`
	BoxSpecifications struct {
		BoxName  string `json:"boxname"`
		CPU      string `json:"cpu"`
		Size     int    `json:"size"`
		Provider string `json:"provider"`
		TTL      string `json:"ttl"`
		BoxIP    string `json:"boxip"`
		Gui      bool   `json:"gui"`
	} `json:"boxSpecifications"`
}
