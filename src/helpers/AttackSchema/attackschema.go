package AttackSchema

import (
	"encoding/json"
	FlagsSchema "helpers/FlagsSchema"
	"time"
)

type AttackState string

const (
	NotExecuted AttackState = "notExecuted"
	Deleted     AttackState = "deleted"
	Executed    AttackState = "executed"
	Executing   AttackState = "executing"
	Error       AttackState = "error"
)

type AttackSchema struct {
	Id           string `json:"_id"`
	AttackName   string `json:"attackname"`
	AttackID     string `json:"attackid"`
	ErrorMessage string `json:"error"`
	ResultsId    string `json:"resultsid"`
	State        string `json:"state"`
	Step         string `json:"step"`
	StepNum      string `json:"step_num"`
	TypeAttack   string `json:"typeattack"`
	ErrorMsg     string `json:"errormsg"`
	DockerImage  string `json:"dockerimage"`
	OsType       string `json:"os_type"`
	RunnerType   string `json:"runnertype"`
	Webrunner    []struct {
		Url         string            `json:"url"`
		Headers     map[string]string `json:"headers"`
		QueryParams map[string]string `json:"queryParams"`
		Type        string            `json:"type"`
	} `json:"webrunner"`
	Dependencies []struct {
		Key  string `json:"key"`
		Size string `json:"size"`
	} `json:"dependencies"`
	CliCommands []CliCommands `json:"clicommands"`
	Results     AttackResult  `json:"results"`
	Logs        []string      `json:"logs"`
}

type CliCommands struct {
	Action               string            `json:"action"`
	OptionsId            string            `json:"optionsid"`
	Options              string            `json:"options"`
	Flags                FlagsSchema.Flags `json:"Flags"`
	RedirectStdin        string            `json:"redirectstdin"`
	RedirectStdout       string            `json:"redirectstdout"`
	RedirectStderr       string            `json:"redirectstderr"`
	Pipe                 bool              `json:"pipe"`
	BackgroundJob        bool              `json:"backgroundjob"`
	ConncatenateCommands bool              `json:"conncatenatecommands"`
	Timeout              string            `json:"timeout"`
	OutputType           string            `json:"outputtype"`
	Fileoutput           bool              `json:"fileoutput"`
	NeedResults          bool              `json:"needresults"`
}

// Resultsfile structure
type AttackResult struct {
	ID              string                   `json:"_id"`
	CommandsResult  []ResultSchema           `json:"commandsresult"`
	ProcessedOutput []map[string]interface{} `json:"processedOutput"`
	Start           time.Time                `json:"start"`
	End             time.Time                `json:"end"`
}

type Results struct {
	Id      string `json:"id"`
	Name    string `json:"name"`
	Percent int    `json:"percent"`
	State   string `json:"state"`
	Status  string `json:"status"`
	Step    string `json:"step"`
}

type Table []struct {
	Item struct {
		Row []struct {
			Columns []struct {
				Color string `json:"color"`
				Value string `json:"value"`
			} `json:"columns"`
		} `json:"row"`
	} `json:"item"`
}
type ResultSchema struct {
	CommandInfo   CliCommands            `json:"commandinfo"`
	Resultsoutput string                 `json:"resultsoutput"`
	Resultsjson   map[string]interface{} `json:"resultsjson"`
	Table         Table                  `json:"table"`
}

func (c *ResultSchema) LoadFromMap(m map[string]interface{}) error {
	data, err := json.Marshal(m)
	if err == nil {
		err = json.Unmarshal(data, c)
	}
	return err
}
