package AttackAbstraction

type ResultTable []struct {
	Item Item `json:"item"`
}

type Item struct {
	Row []Row `json:"row"`
}
type Row struct {
	Columns []Element `json:"columns"`
}

type Element struct {
	Color string `json:"color"`
	Value string `json:"value"`
}

func NewTable() ResultTable {
	return ResultTable{}
}

func (i *Item) AddRow(row Row) {
	i.Row = append(i.Row, row)
}

func (r *Row) AddColumn(column Element) {
	r.Columns = append(r.Columns, column)
}

func (r *Row) AddColumnFields(color string, value string) {
	var col Element = Element{color, value}
	r.Columns = append(r.Columns, col)
}

func (t *ResultTable) AddItem(item Item) {
	*t = append(*t, struct {
		Item Item `json:"item"`
	}{Item: item})
}

func (t *ResultTable) AddRow(row Row) {
	if len(*t) == 0 {
		var item Item
		t.AddItem(item)
	}
	(*t)[len(*t)-1].Item.AddRow(row)
}
