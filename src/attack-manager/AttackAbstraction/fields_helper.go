package AttackAbstraction

import "reflect"

func GetInterfaceField(i_data interface{}, fields ...string) any {
	// check if field exists in data
	data := i_data.(map[string]interface{})
	return GetMapInterfaceField(data, fields...)
}

func GetMapInterfaceField(data map[string]interface{}, fields ...string) any {
	// check if field exists in data
	tmp_data := data
	for _, field := range fields {
		_, ok := tmp_data[field]
		if ok {
			to_eval := reflect.TypeOf(tmp_data[field])
			if to_eval.Kind() == reflect.Map {
				tmp_data = tmp_data[field].(map[string]interface{})
			} else {
				return tmp_data[field]
			}
		} else {
			return nil
		}
	}
	return tmp_data
}
