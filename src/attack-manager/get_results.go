package main

import (
	"encoding/json"
	"errors"
	"helpers/DatabaseHelper"
	GeneralHelper "helpers/GeneralHelper"
	"math"
	"reflect"
	"strconv"

	as "helpers/AttackSchema"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

var attacksCache = make(map[string]int)

func getAttack(value []byte) {
	var message PulsarLib.MessageResponse
	var session_id int
	var err error

	log.Info("Getting Attack")

	json.Unmarshal(value, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error with session ID " + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	if len(attacksCache) == 0 {
		fillAttacksCache(session_id, comm_client)
	}

	var attack_id string
	err = GeneralHelper.GetMapElement(message.Msg, "attack_id", &attack_id)
	if err != nil {
		log.Error("Query from UI invalid, attack_id not found " + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	var attack as.AttackSchema
	err = getAttackByID(session_id, attack_id, &attack)
	if err != nil {
		log.Println("Error getting attack " + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	byte_result, _ := json.Marshal(attack)
	PulsarLib.SendMessage(*comm_client, message.Id, byte_result)
}

func getResult(value []byte) {
	var message PulsarLib.MessageResponse
	var session_id int
	var err error

	log.Info("Getting result of attack")

	json.Unmarshal(value, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error with session ID " + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	if len(attacksCache) == 0 {
		fillAttacksCache(session_id, comm_client)
	}

	var attack_id string
	err = GeneralHelper.GetMapElement(message.Msg, "attack_id", &attack_id)
	if err != nil {
		log.Error("Query from UI invalid, attack_id not found " + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	//get the attackSchema frist
	var attack as.AttackSchema
	err = getAttackByID(session_id, attack_id, &attack)
	if err != nil {
		log.Error("Error getting attack " + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	result := GeneralHelper.StructToMap(attack.Results)
	byte_result, _ := json.Marshal(result)

	PulsarLib.SendMessage(*comm_client, message.Id, byte_result)
}

func fillAttacksCache(session_id int, comm_client *PulsarLib.PL_Client) {
	attacks, err := DatabaseHelper.GetAllGeneralCollectionEntries(session_id, comm_client, "attacks")
	log.Println(attacks)
	if err != nil {
		return
	}
	for _, attack := range attacks {
		steps := int(math.Round(attack["step_num"].(float64)))
		attacksCache[attack["name"].(string)] = steps
	}
}

func getAttacks(msg []byte) {
	log.Info("Getting resultsDB")
	var message PulsarLib.MessageResponse
	var session_id int
	var err error

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error malformed get attack query ", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var attack_status_filter []as.AttackState
	if message.Msg["state"] == nil {
		// Default filter for main attack list view
		attack_status_filter = append(attack_status_filter, as.Executed)
		attack_status_filter = append(attack_status_filter, as.Executing)
		attack_status_filter = append(attack_status_filter, as.Error)
	}

	if val, ok := message.Msg["state"]; ok {
		attack_status_filter = nil
		if _, ok := val.([]interface{}); ok {
			for _, state := range val.([]interface{}) {
				attack_status_filter = append(attack_status_filter, as.AttackState(state.(string)))
			}
		} else if reflect.TypeOf(val).Kind() == reflect.String {
			attack_status_filter = append(attack_status_filter, as.AttackState(val.(string)))
		} else {
			PulsarLib.SendMessage(*comm_client, message.Id,
				GeneralHelper.BuildLegacyErrorMessage(errors.New("Invalid state filter"), GeneralHelper.InvalidFormat))
			return
		}
	}

	var attack_filter []DatabaseHelper.Filter
	if val, ok := message.Msg["filters"]; ok {
		var filters []map[string]interface{}
		for _, v := range val.([]interface{}) {
			filters = append(filters, v.(map[string]interface{}))
		}
		err = GeneralHelper.ArrayMapToStruct(filters, &attack_filter)
		if err != nil {
			PulsarLib.SendMessage(*comm_client, message.Id,
				GeneralHelper.BuildLegacyErrorMessage(errors.New("Invalid General filter"), GeneralHelper.InvalidFormat))
			return
		}
	}

	var attacks []as.AttackSchema

	for _, state := range attack_status_filter {
		var attacksTmp []as.AttackSchema
		err = getAttacksByState(session_id, state, &attacksTmp, attack_filter)
		if err == nil {
			attacks = append(attacks, attacksTmp...)
		}
	}
	all_attacks := buildAttacksResponse(attacks, session_id)

	byte_attacks, _ := json.Marshal(all_attacks)
	PulsarLib.SendMessage(*comm_client, message.Id, byte_attacks)
}

func buildAttacksResponse(attacks []as.AttackSchema, session_id int) []map[string]interface{} {
	var all_attacks []map[string]interface{}
	for i := range attacks {
		my_attack := make(map[string]interface{})
		my_attack["name"] = attacks[i].AttackName
		my_attack["type"] = attacks[i].TypeAttack
		my_attack["id"] = attacks[i].Id
		my_attack["state"] = attacks[i].State
		my_attack["results"] = attacks[i].Results
		my_attack["logs"] = attacks[i].Logs

		my_attack["step"] = attacks[i].Step
		step, _ := strconv.Atoi(attacks[i].StepNum)
		my_attack["percent"] = 100 * step / getAttackNumberSteps(session_id, attacks[i].TypeAttack)
		if attacks[i].State == string(as.Executing) {
			my_attack["status"] = "default"
		} else if attacks[i].State == string(as.Executed) {
			my_attack["status"] = "success"
			my_attack["percent"] = 100
		} else {
			my_attack["status"] = "error"
		}
		all_attacks = append(all_attacks, my_attack)
	}
	return all_attacks
}

func getFilteredAttacks(msg []byte) {
	log.Info("Getting Attack by Filter")
	var message PulsarLib.MessageResponse
	var session_id int
	var err error

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error malformed delete attack query ", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var attack_filter string
	err = GeneralHelper.GetMapElement(message.Msg, "state", &attack_filter)
	if err != nil {
		err_str := "You must specify the state"
		log.Error(err_str + err.Error())
		err = errors.New(err_str + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	var filters []DatabaseHelper.Filter
	if val, ok := message.Msg["filters"]; ok {
		var filters []map[string]interface{}
		for _, v := range val.([]interface{}) {
			filters = append(filters, v.(map[string]interface{}))
		}
		err = GeneralHelper.ArrayMapToStruct(filters, &attack_filter)
		if err != nil {
			PulsarLib.SendMessage(*comm_client, message.Id,
				GeneralHelper.BuildLegacyErrorMessage(errors.New("Invalid General filter"), GeneralHelper.InvalidFormat))
			return
		}
	}

	var attacks []as.AttackSchema
	filterState := as.AttackState(attack_filter)
	getAttacksByState(session_id, filterState, &attacks, filters)

	all_attacks := buildAttacksResponse(attacks, session_id)
	byte_attacks, _ := json.Marshal(all_attacks)
	PulsarLib.SendMessage(*comm_client, message.Id, byte_attacks)
}

func updateResultLogs(msg []byte) {
	log.Info("Updating Attack Logs")
	var message PulsarLib.MessageResponse
	var session_id int
	var err error

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		err_msg := "Error malformed Log update " + err.Error()
		log.Error(err_msg)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(errors.New(err_msg), GeneralHelper.NoSession))
		return
	}

	var result_id string
	err = GeneralHelper.GetMapElement(message.Msg, "result_id", &result_id)
	if err != nil {
		err_str := "Missing attack_id in query"
		log.Error(err_str + err.Error())
		err = errors.New(err_str + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	var logs []string
	err = GeneralHelper.GetMapElement(message.Msg, "logs", &logs)
	if err != nil {
		err_str := "Logs missing"
		log.Error(err_str + err.Error())
		err = errors.New(err_str + err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	new_logs := make(map[string]interface{})
	new_logs["logs"] = logs

	response, err := DatabaseHelper.UpdateOneElementByID(session_id, comm_client, result_id, "", new_logs)
	if err != nil {
		err_msg := "Error updating attack logs " + err.Error() + " " + string(response)
		log.Error(err_msg)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(errors.New(err_msg), GeneralHelper.InvalidFormat))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}
