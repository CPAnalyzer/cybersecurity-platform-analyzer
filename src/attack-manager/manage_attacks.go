package main

import (
	"attack-manager/AttackAbstraction"
	"encoding/json"
	"errors"
	"fmt"
	ac "helpers/AttackChecks"
	as "helpers/AttackSchema"
	"helpers/DatabaseHelper"
	GeneralHelper "helpers/GeneralHelper"
	"helpers/OptionParser"
	of "helpers/OptionsFile"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func getAttackByID(session_id int, attack_id string, attack *as.AttackSchema) error {
	var element map[string]interface{}
	element, err := DatabaseHelper.GetEntryByID(session_id, comm_client, attack_id)
	if err != nil {
		return err
	}
	tmp, err := json.Marshal(element)
	if err != nil {
		log.Error("Error parsing response", err)
		return err
	}
	err = json.Unmarshal(tmp, attack)
	if err != nil {
		log.Error("Error parsing response", err)
		return err
	}
	return nil
}

func getAttackByName(session_id int, attackname string, attack *as.AttackSchema) bool {
	raw_attack, err := DatabaseHelper.GetEntryByName(session_id, comm_client, "attackname", attackname)
	if err != nil {
		log.Error("Error getting attacks by name", err)
		return false
	}
	err = GeneralHelper.MapToStruct(raw_attack, &attack)
	if err != nil {
		log.Error("Error with response", err)
		return false
	}
	return true
}

func getGeneralAttackByName(session_id int, attackname string) (as.AttackSchema, bool) {
	var err error
	var attack as.AttackSchema

	formatted, err := DatabaseHelper.GetGeneralEntryByName(session_id, comm_client, "AttackSchema", "attackname", attackname)
	if err != nil {
		log.Error("Error getting general attack by name ", err)
		return attack, false
	}
	err = GeneralHelper.MapToStruct(formatted, &attack)
	if err != nil {
		log.Error("Error with response ", err)
		return attack, false
	}
	attack.Id = ""

	return attack, true
}

func getAttacksByState(session_id int, state as.AttackState, attacks *[]as.AttackSchema, filters []DatabaseHelper.Filter) error {
	filter, _ := DatabaseHelper.BuildFilter("state", string(state), "eq")
	filters = append(filters, filter)

	all_attacks, err := DatabaseHelper.GetEntryWithFilter(session_id, comm_client, filters)
	if err != nil {
		return err
	}

	err = GeneralHelper.ArrayMapToStruct(all_attacks, &attacks)
	if err != nil {
		log.Error("Error getting attacks by state: ", err)
		return err
	}
	if attacks == nil || len(*attacks) == 0 {
		log.Error("No attacks on status: ", state)
		return err
	}
	return nil
}

func getAttackNumberSteps(session_id int, attack_name string) int {
	if len(attacksCache) == 0 {
		log.Debug("Filling attacks cache for faster access")
		fillAttacksCache(session_id, comm_client)
	}
	return attacksCache[attack_name]
}

func getNotExecutedAttacks(msg []byte) {
	log.Info("Getting not executed attacks")
	var message PulsarLib.MessageResponse
	var session_id int
	var err error

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error malformed attack query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	filters := []DatabaseHelper.Filter{}

	var attacks []as.AttackSchema
	getAttacksByState(session_id, as.NotExecuted, &attacks, filters)
	response, err := json.Marshal(attacks)
	if err != nil || len(attacks) == 0 {
		response, _ = json.Marshal(make([]string, 0))
	}
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}

func setAttackStatus(session_id int, attack_id string, status as.AttackState) ([]byte, error) {
	return DatabaseHelper.UpdateOneElementByID(session_id, comm_client, attack_id, "state", string(status))
}

func startAttack(msg []byte) {
	log.Info("Starting attack")
	dtStart := time.Now()
	var message PulsarLib.MessageResponse
	var err error
	var attack_id string
	var session_id int

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error malformed start attack query ", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	err = GeneralHelper.GetMapElement(message.Msg, "_id", &attack_id)
	if err != nil {
		log.Error("Error parsing entry ", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	var attack as.AttackSchema
	err = getAttackByID(session_id, attack_id, &attack)
	if err != nil {
		log.Error("Error getting attack ", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	response, err := setAttackStatus(session_id, attack_id, as.Executing)
	_ = response
	if err != nil {
		log.Error("Attack: " + attack_id + " error setting to executing")
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}
	log.Info("Attack: " + attack_id + " is now executing")

	changeDashboardStatistics(session_id, attack.TypeAttack, as.Executing, 1)

	attack_with_database := make(map[string]any)
	attack_with_database["session_id"] = strconv.Itoa(int(session_id))
	attack_with_database["attack"] = attack

	attack_to_send := PulsarLib.BuildMessage(attack_with_database)

	// send attack schema
	attack_result := PulsarLib.SendRequestSync(*comm_client, "instantiation.runAttack", attack_to_send)

	results := make(map[string]any)
	json.Unmarshal(attack_result, &results)

	if results == nil {
		err_msg := "Error executing attack"
		log.Error(err_msg)
		setAttackStatus(session_id, attack_id, as.Error)
		changeDashboardStatistics(session_id, attack.TypeAttack, as.Executing, -1)
		changeDashboardStatistics(session_id, attack.TypeAttack, as.Error, 1)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(errors.New(err_msg), GeneralHelper.UnknownError))
		return
	}

	if _, ok := results["success"]; !ok {
		err_msg := "Error: 'success' field is missing in the results"
		log.Error(err_msg)
		setAttackStatus(session_id, attack_id, as.Error)
		changeDashboardStatistics(session_id, attack.TypeAttack, as.Executing, -1)
		changeDashboardStatistics(session_id, attack.TypeAttack, as.Error, 1)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(errors.New(err_msg), GeneralHelper.UnknownError))
		return
	} else if results["success"].(bool) {
		results["result"].(map[string]any)["processor_name"] = attack.TypeAttack
		msgToProcess, _ := json.Marshal(results["result"])
		map_to_process := make(map[string]any)
		json.Unmarshal(msgToProcess, &map_to_process)
		msg_to_instantiation := PulsarLib.BuildMessage(map_to_process)
		res_processed := PulsarLib.SendRequestSync(*comm_client, "attack-db.postProcessResult", msg_to_instantiation)

		var final_attack as.AttackResult
		err = json.Unmarshal(res_processed, &final_attack)
		if err != nil {
			log.Error("Error parsing attack ", err.Error())
			return
		}
		attack.Results = final_attack
		_, err = DatabaseHelper.UpdateOneElementByID(session_id, comm_client, attack_id, "results", final_attack)
		if err != nil {
			log.Error("Error inserting attack ", err.Error())
			return
		}

		// Check for errors
		_, err = setAttackStatus(session_id, attack_id, as.Executed)
		if err != nil {
			log.Error("Error setting attack status ", err.Error())
			return
		}

		changeDashboardStatistics(session_id, attack.TypeAttack, as.Executing, -1)
		changeDashboardStatistics(session_id, attack.TypeAttack, as.Executed, 1)

		dtEnd := time.Since(dtStart)
		addAttackExecutionTime(session_id, attack.TypeAttack, dtEnd)
	} else {
		setAttackStatus(session_id, attack_id, as.Error)
		_, err = DatabaseHelper.UpdateOneElementByID(session_id, comm_client, attack_id, "errormsg", results["errormsg"].(string))
		if err != nil {
			log.Error("Error updating attack ", err.Error())
			return
		}

		changeDashboardStatistics(session_id, attack.TypeAttack, as.Executing, -1)
		changeDashboardStatistics(session_id, attack.TypeAttack, as.Error, 1)
	}
	// On either case both success and error will have some information
	b_results, err := json.Marshal(results)
	if err != nil {
		log.Error("Error marshaling results")
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
	} else {
		PulsarLib.SendMessage(*comm_client, message.Id, b_results)
	}
}

func deleteAttack(msg []byte) {
	log.Info("Deleting the attack")
	var message PulsarLib.MessageResponse
	var session_id int
	var response []byte
	var err error

	json.Unmarshal(msg, &message)

	log.Println(message)
	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Println("Error malformed delete attack query ", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var attack_id string
	err = GeneralHelper.GetMapElement(message.Msg, "_id", &attack_id)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	response, err = setAttackStatus(session_id, attack_id, as.Deleted)
	if err != nil {
		log.Println("Error setting attack status ", err.Error())
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}

func attackSubmission(msg []byte) {
	log.Info("Attack received")

	var message PulsarLib.MessageResponse
	var err error
	var session_id int

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error malformed attack query ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var attack_title string
	err = GeneralHelper.GetMapElement(message.Msg, "attack_title", &attack_title)
	if err != nil {
		log.Error("Invalid format on input ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}

	var attack_name string
	err = GeneralHelper.GetMapElement(message.Msg, "attack_name", &attack_name)
	if err != nil {
		log.Error("Invalid format on input ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}
	var attack_options []of.OptionsFile
	tmp, _ := json.Marshal(message.Msg["options"])
	json.Unmarshal(tmp, &attack_options)

	var attack as.AttackSchema
	attack, _ = getGeneralAttackByName(session_id, attack_name)

	var indexes []int
	for i := range attack.CliCommands {
		if attack.CliCommands[i].OptionsId != "" {
			indexes = append(indexes, i)
		}
	}

	/*
	   Activate option fileoutput when there is a option with Maingroup OUTPUT and subgroup file.
	   It also activates XML output if the title is XML. The name of the file will be the options id
	*/
	for attackindex, optiongroup := range attack_options {
		for _, option := range optiongroup.Options {
			if len(indexes) > attackindex {
				if option.MainGroup == "Output" && option.SubGroup == "file" && option.Checkbox {
					attack.CliCommands[indexes[attackindex]].Fileoutput = true
					if option.Title == "XML" {
						attack.CliCommands[indexes[attackindex]].OutputType = "XML"
					} else if option.Title == "JSON" {
						attack.CliCommands[indexes[attackindex]].OutputType = "JSON"
					}
				}
			}
		}
	}

	for i := 0; i < len(attack_options); i++ {
		if len(indexes) > i {
			log.Info("Adding attack option to database: " + attack.CliCommands[indexes[i]].OptionsId)
			id, err := DatabaseHelper.InsertOneElement(session_id, comm_client, GeneralHelper.StructToMap(attack_options[i]))
			if err != nil {
				log.Error("Error inserting attack option ", err)
				PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
				return
			}
			attack.CliCommands[indexes[i]].OptionsId = id
			log.Debug(id)
		}
	}
	attack.AttackName = attack_title

	err = createAttackLocal(session_id, &attack)
	if err != nil {
		log.Error("Error creating attack ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.UnknownError))
		return
	}

	// Parse the tool option file to get the tool configuration
	toolConfig, err := OptionParser.ParseToolOptionFile(session_id, comm_client, attack_name)
	if err != nil {
		log.Println("error parsing tool option file:", err)
		return
	}

	// search for Commands
	var selectedIndex int = -1

	// Iterate through CliCommands to find the index where OptionsId is not empty
	for i, cmd := range attack.CliCommands {
		if cmd.OptionsId != "" {
			selectedIndex = i
			break
		}
	}

	// Check if a valid index was found
	if selectedIndex != -1 {
		if toolConfig.Delimiter != "" {
			// Perform checks using the correct index
			if err := ac.PerformChecks(attack.CliCommands[selectedIndex].Flags, toolConfig); err != nil {
				fmt.Println("error in security checks:", err)
				// Write error in attack schema
				attack.ErrorMessage = err.Error()
			}
		}
	} else {
		fmt.Println("No valid OptionsId found in CliCommands")
		// Handle the case where no valid OptionsId was found, if necessary
	}

	var i int = 0
	var name_base string = attack.AttackName
	for attackExists(session_id, attack.AttackName) {
		name := name_base + " (" + strconv.Itoa(i) + ")"
		attack.AttackName = name
		i++
	}

	log.Info("Inserting attack:", attack)
	attack_oid, insert_err := DatabaseHelper.InsertOneElement(session_id, comm_client, GeneralHelper.StructToMap(attack))

	if insert_err != nil {
		log.Error("Error inserting attack ", insert_err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(insert_err, GeneralHelper.UnknownError))
		return
	}

	//DASHBOARD
	var dashboard DashboardSchema
	raw_dashboard, err := DatabaseHelper.GetEntryByName(session_id, comm_client, "attacknamedashboard", attack_name)
	if err != nil {
		log.Error("Error getting dashboard info ", err)
	}
	err = GeneralHelper.MapToStruct(raw_dashboard, &dashboard)
	if err != nil {
		log.Error("Error getting dashboard info ", err)
	}

	if dashboard.AttacknameDashboard == "" {
		dashboardSchema, err := DatabaseHelper.GetGeneralEntryByName(session_id, comm_client, "DashboardSchema",
			"attacknamedashboard", attack_name)
		if err != nil {
			log.Error("Error getting dashboard info ", err)
		} else {
			_, _ = DatabaseHelper.InsertOneElement(session_id, comm_client, dashboardSchema)
		}
	}
	result := make(map[string]string)
	result["attack_id"] = attack_oid
	response, _ := json.Marshal(result)
	PulsarLib.SendMessage(*comm_client, message.Id, response)
}

func createAttackLocal(session_id int, attack *as.AttackSchema) error {
	for iter, element := range attack.CliCommands {
		// Parse the tool option file to get the tool configuration
		toolConfig, err := OptionParser.ParseToolOptionFile(session_id, comm_client, attack.TypeAttack)
		if err != nil {
			log.Println("Error parsing tool option file:", err)
			return err
		}

		if toolConfig.Delimiter == "" {
			res, err := createOptionsLocal(session_id, element.OptionsId)
			if err != nil {
				log.Println("Error getting attack options:", err)
				return err
			}
			attack.CliCommands[iter].Options = res
		} else {
			res, err := createJsonOptionsLocal(session_id, element.OptionsId)
			if err != nil {
				log.Println("Error getting attack options:", err)
				return err
			}
			attack.CliCommands[iter].Flags = res
		}
	}
	return nil
}

func attackExists(session_id int, attack_name string) bool {
	var the_attack as.AttackSchema
	return getAttackByName(session_id, attack_name, &the_attack)
}

func postProcessAttack(msg []byte) {
	log.Println("Postprocessing attack")

	var message PulsarLib.MessageResponse
	json.Unmarshal(msg, &message)
	result := AttackAbstraction.ProcessResult(message.Msg)
	if result == nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(errors.New("Error processing results"), GeneralHelper.UnknownError))
		return
	}
	with_table := AttackAbstraction.ResultsToTable(result)
	var resultMap map[string]interface{}
	err := json.Unmarshal(with_table, &resultMap)
	if err != nil {
		log.Println("Error converting with_table to map[string]interface{}", err)
	}
	with_workflowAttackResults := AttackAbstraction.ProcessAttackWorkflow(resultMap)
	if with_workflowAttackResults == nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(errors.New("Invalid workflow"), GeneralHelper.UnknownError))
		return
	}

	PulsarLib.SendMessage(*comm_client, message.Id, with_workflowAttackResults)
}

func searchGeneralAttacks(msg []byte) {
	var err error
	/*{ "session_id": "1234567890", "database": "general",
		"collection": "attacks",
		"field": "attacknamedashboard",
		"filters":
			[{"filter": "name", "value": "test", "op": "$eq"},
		   {"filter": "name", "value": "test2", "op": "$eq"}]
	}*/
	var message PulsarLib.MessageResponse
	var query DatabaseHelper.DatabaseQuery

	log.Info("We are searching attacks....")
	json.Unmarshal(msg, &message)
	search_query := message.Msg

	session_id, err := GeneralHelper.GetSession_id(search_query)
	if err != nil {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	filter := DatabaseHelper.Filter{Field: "name", Value: "true", Op: "$exists"}
	query = DatabaseHelper.DatabaseQuery{Session_id: session_id,
		Database:   "general",
		Collection: "attacks", Filters: []DatabaseHelper.Filter{filter}}

	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	result := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	PulsarLib.SendMessage(*comm_client, message.Id, result)
}
