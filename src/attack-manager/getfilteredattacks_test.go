package main

import (
	"confManager"
	"encoding/json"
	"flag"
	"fmt"
	"helpers/GeneralHelper"
	"strconv"
	"testing"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func TestWrongGetFilteredAttacks(t *testing.T) {
	var pulsar_host *string
	var pulsar_port *int
	var creds *string

	creds = flag.String("credentials", "../../config/credentials.json", "JSON file with the credentials")
	pulsar_host = flag.String("pulsar", "", "Pulsar host: e.g. 192.168.122.168")
	pulsar_port = flag.Int("pulsar-port", 0, "Pulsar port: e.g. 6650")

	flag.Parse()

	confManager.LoadDefaultConfig()
	if *pulsar_host == "" {
		*pulsar_host = confManager.GetValue("pulsar_host")
	}
	if *pulsar_host == "" {
		*pulsar_host = "localhost"
	}

	port, err := strconv.Atoi(confManager.GetValue("pulsar_port"))
	if port != 0 && *pulsar_port == 0 {
		if err != nil {
			t.Fatalf("failed to convert pulsar port: %v", err)
		}
		*pulsar_port = port
	}
	if *pulsar_port == 0 {
		*pulsar_port = 6650
	}
	pulsar_port_str := strconv.Itoa(*pulsar_port)
	client := PulsarLib.InitClient("pulsar://" + *pulsar_host + ":" + pulsar_port_str)
	comm_client = &client
	defer (*comm_client.Client).Close()

	log.Print("Logging in")
	session_id, err := GeneralHelper.LoginWithCredentialFile(client, *creds)
	if err != nil {
		log.Println("Error logging in: ", err)
		return
	}

	parsed := make(map[string]interface{})
	parsed["session_id"] = session_id
	parsed["state"] = []string{"executed", "error", "executing", "notExecuted", "deleted"}
	msg_to_instantiation := PulsarLib.BuildMessage(parsed)
	result := PulsarLib.SendRequestSync(*comm_client, "ui-db.getAttacks", msg_to_instantiation)

	var parsed_result []interface{}
	err = json.Unmarshal(result, &parsed_result)
	if err != nil {
		log.Println(parsed_result)
		GeneralHelper.Logout(*comm_client, session_id)
		return
	}
	GeneralHelper.Logout(*comm_client, session_id)
	fmt.Println("No error reported")
}

func TestGetFilteredAttacks(t *testing.T) {
	var pulsar_host *string
	var pulsar_port *int
	var creds *string

	creds = flag.String("credentials", "../../config/credentials.json", "JSON file with the credentials")
	pulsar_host = flag.String("pulsar", "", "Pulsar host: e.g. 192.168.122.168")
	pulsar_port = flag.Int("pulsar-port", 0, "Pulsar port: e.g. 6650")

	flag.Parse()

	confManager.LoadDefaultConfig()
	if *pulsar_host == "" {
		*pulsar_host = confManager.GetValue("pulsar_host")
	}
	if *pulsar_host == "" {
		*pulsar_host = "localhost"
	}

	port, err := strconv.Atoi(confManager.GetValue("pulsar_port"))
	if port != 0 && *pulsar_port == 0 {
		if err != nil {
			t.Fatalf("failed to convert pulsar port: %v", err)
		}
		*pulsar_port = port
	}
	if *pulsar_port == 0 {
		*pulsar_port = 6650
	}
	pulsar_port_str := strconv.Itoa(*pulsar_port)
	client := PulsarLib.InitClient("pulsar://" + *pulsar_host + ":" + pulsar_port_str)
	comm_client = &client
	defer (*comm_client.Client).Close()

	log.Print("Logging in")
	session_id, err := GeneralHelper.LoginWithCredentialFile(client, *creds)
	if err != nil {
		log.Println("Error logging in: ", err)
		return
	}

	parsed := make(map[string]interface{})
	parsed["session_id"] = session_id
	parsed["state"] = "executed"
	msg_to_instantiation := PulsarLib.BuildMessage(parsed)
	result := PulsarLib.SendRequestSync(*comm_client, "ui-db.getAttacks", msg_to_instantiation)

	var parsed_result []interface{}
	err = json.Unmarshal(result, &parsed_result)
	if err != nil {
		log.Println(parsed_result)
		GeneralHelper.Logout(*comm_client, session_id)
		return
	}
	GeneralHelper.Logout(*comm_client, session_id)
	fmt.Println("No error reported")
}
