package nmapworkflow

import (
	"encoding/json"
	"log"
	"reflect"
)

func ExtractPortsInfo(results map[string]interface{}) []byte {
	var portInfo []map[string]string

	for _, indiv_attack := range results["commandsresult"].([]interface{}) {
		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			if resultsjson != nil {
				log.Println(resultsjson)
				resultsjsonrun := resultsjson["nmaprun"].(map[string]interface{})
				if host, ok := resultsjsonrun["host"].(map[string]interface{}); ok {
					/*
						host = resultsjsonrun["host"].(map[string]interface{})
						log.Panicln("HOST")
						log.Println(host)
					*/
					ports := host["ports"].(map[string]interface{})
					log.Println("PORTS")
					log.Println(ports)

					if _, ok := ports["port"]; !ok {
						continue
					}

					portType := reflect.TypeOf(ports["port"])
					switch portType.Kind() {
					case reflect.Map:
						port := ports["port"].(map[string]interface{})
						portID := port["@portid"].(string)
						portStatus := port["state"].(map[string]interface{})["@state"].(string)

						portInfo = append(portInfo, map[string]string{
							portID: portStatus,
						})
					case reflect.Slice:
						for _, open_port := range ports["port"].([]interface{}) {
							port := open_port.(map[string]interface{})
							portID := port["@portid"].(string)
							portStatus := port["state"].(map[string]interface{})["@state"].(string)

							portInfo = append(portInfo, map[string]string{
								portID: portStatus,
							})
						}
					}
				}
			}

		}
	}

	if len(portInfo) > 0 {
		// Store the processed port information in the object "results".
		results["processedOutput"] = portInfo
	}

	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
