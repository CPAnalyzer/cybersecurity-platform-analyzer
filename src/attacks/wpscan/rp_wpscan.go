package main

import (
	ww "attacks/wpscan/wpscanWorkflow"
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"
)

var Name = "wpscan"

type WPScanData struct {
	Version             string            `json:"version"`
	WordPressVersion    string            `json:"wordpress_version"`
	URL                 string            `json:"url"`
	PHPVersion          string            `json:"php_version,omitempty"`
	Theme               ThemeInfo         `json:"theme"`
	InterestingFindings []Finding         `json:"interesting_findings"`
	Plugins             []PluginInfo      `json:"plugins"`
	Statistics          map[string]string `json:"statistics"`
}

type ThemeInfo struct {
	Name          string `json:"name"`
	Location      string `json:"location,omitempty"`
	Style         string `json:"style,omitempty"`
	LatestVersion string `json:"latest_version,omitempty"`
	LastUpdated   string `json:"last_updated,omitempty"`
}

type PluginInfo struct {
	Name          string `json:"name"`
	Location      string `json:"location,omitempty"`
	Found         string `json:"found_by"`
	LastUpdated   string `json:"last_updated,omitempty"`
	LatestVersion string `json:"latest_version,omitempty"`
	Version       string `json:"version,omitempty"`
}

type Finding struct {
	Info       string   `json:"info"`
	Found      string   `json:"found_by"`
	References []string `json:"references,omitempty"`
}

func TableGenerator(results map[string]interface{}) []byte {
	return wpscanTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	data := WPScanData{
		Theme:               ThemeInfo{},
		Plugins:             []PluginInfo{},
		Statistics:          make(map[string]string),
		InterestingFindings: []Finding{},
	}

	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")
		var currentPlugin *PluginInfo
		var currentFinding *Finding
		parsingPlugin := false
		parsingWordpresstheme := false
		parsingInteresting := false

		for _, line := range lines {
			if strings.Contains(line, "Finished:") {
				parsingPlugin = false
				parsingInteresting = false
				data.Statistics["finished"] = strings.TrimSpace(strings.Split(line, ":")[1])
			}
			if strings.Contains(line, "Plugin(s) Identified:") || strings.Contains(line, "Enumerating All") {
				parsingPlugin = true
				parsingInteresting = false
				continue
			}
			if strings.Contains(line, "Interesting") {
				parsingInteresting = true
				continue
			}

			if strings.Contains(line, "Version:") {
				data.Version = strings.TrimSpace(strings.Split(line, ":")[1])
			}
			if strings.Contains(line, "WordPress version") {
				data.WordPressVersion = strings.TrimSpace(strings.Split(strings.Split(line, "WordPress version")[1], "identified")[0])
			}
			if strings.Contains(line, "[+] URL:") {
				data.URL = strings.TrimSpace(strings.Split(line, "URL:")[1])
			}
			if strings.Contains(line, "PHP version:") {
				data.PHPVersion = strings.TrimSpace(strings.Split(line, "PHP version:")[1])
			}

			if strings.Contains(line, "[+]") && parsingInteresting {
				if currentFinding != nil {
					data.InterestingFindings = append(data.InterestingFindings, *currentFinding)
				}

				currentFinding = &Finding{}
				currentFinding.Info = strings.TrimSpace(strings.TrimPrefix(line, "[+]"))
			} else if strings.Contains(line, "|") && parsingInteresting {
				if strings.Contains(line, " - ") {
					currentFinding.References = append(currentFinding.References, strings.TrimSpace(strings.Split(line, "-")[1]))
				} else {
					if strings.Contains(line, "Found By:") {
						currentFinding.Found = strings.TrimSpace(strings.Split(line, "Found By:")[1])
					}
				}

			}
			if strings.Contains(line, "WordPress theme in use:") {
				parsingWordpresstheme = true
				data.Theme.Name = strings.TrimSpace(strings.Split(line, "WordPress theme in use:")[1])
			}
			if strings.Contains(line, "Location:") && data.Theme.Name != "" && parsingWordpresstheme {
				data.Theme.Location = strings.TrimSpace(strings.Split(line, "Location:")[1])
			}
			if strings.Contains(line, "Style URL:") && data.Theme.Name != "" && parsingWordpresstheme {
				data.Theme.Style = strings.TrimSpace(strings.Split(line, "Style URL:")[1])
			}
			if strings.Contains(line, "Latest version:") && data.Theme.Name != "" && parsingWordpresstheme {
				data.Theme.LatestVersion = strings.TrimSpace(strings.Split(line, "Latest version:")[1])
			}
			if strings.Contains(line, "Last updated:") && data.Theme.Name != "" && parsingWordpresstheme {
				data.Theme.LastUpdated = strings.TrimSpace(strings.Split(line, "Last updated:")[1])
			}

			if strings.Contains(line, "[+]") && parsingPlugin {
				if strings.Contains(line, "Enumerating") || strings.Contains(line, "Checking Plugin Versions") {
					continue
				}
				if currentPlugin != nil && parsingPlugin {
					data.Plugins = append(data.Plugins, *currentPlugin)
				}
				currentPlugin = &PluginInfo{
					Name: strings.TrimSpace(strings.Split(line, "[+]")[1]),
				}
			}

			if currentPlugin != nil && parsingPlugin {
				if strings.Contains(line, "Location:") {
					currentPlugin.Location = strings.TrimSpace(strings.Split(line, "Location:")[1])
				}
				if strings.Contains(line, "Last Updated:") {
					currentPlugin.LastUpdated = strings.TrimSpace(strings.Split(line, "Last Updated:")[1])
				}
				if strings.Contains(line, "The version is out of date, the latest version is") {
					currentPlugin.LatestVersion = strings.TrimSpace(strings.Split(line, "the latest version is")[1])
				}
				if strings.HasPrefix(line, " | Version:") {
					currentPlugin.Version = strings.TrimSpace(strings.Split(line, "Version:")[1])
				}
			}

			if strings.Contains(line, "Requests Done:") {
				data.Statistics["requests_done"] = strings.TrimSpace(strings.Split(line, ":")[1])
			}
			if strings.Contains(line, "Cached Requests:") {
				data.Statistics["cached_requests"] = strings.TrimSpace(strings.Split(line, ":")[1])
			}
			if strings.Contains(line, "Data Sent:") {
				data.Statistics["data_sent"] = strings.TrimSpace(strings.Split(line, ":")[1])
			}
			if strings.Contains(line, "Data Received:") {
				data.Statistics["data_received"] = strings.TrimSpace(strings.Split(line, ":")[1])
			}
			if strings.Contains(line, "Memory used:") {
				data.Statistics["memory_used"] = strings.TrimSpace(strings.Split(line, ":")[1])
			}
			if strings.Contains(line, "Elapsed time:") {
				data.Statistics["elapsed_time"] = strings.TrimSpace(strings.Split(line, ":")[1])
			}
		}
		if currentPlugin != nil {
			data.Plugins = append(data.Plugins, *currentPlugin)
		}
	}
	results["processedOutput"] = data
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return ww.ExtractInfo(results)
}

func wpscanTableGenerator(results map[string]interface{}) []byte {
	bytes_final, _ := json.Marshal(results)
	return bytes_final
}
