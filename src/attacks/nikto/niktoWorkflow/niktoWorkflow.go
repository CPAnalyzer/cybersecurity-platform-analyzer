package niktoWorkflow

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"
)

/*
"processedOutput": [

	  {
	    "OSVDB-29786": "/admin.php?en_log_id=0&action=config"
	  },
	  {
	    "OSVDB-3092": "/admin.php"
	  }
	],
*/
func ExtractVulnerabilities(results map[string]interface{}) []byte {
	var vulnerabilitiesInfo []map[string]string

	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")

		// Create a temporary map to store unique vulnerabilities
		uniqueVulnerabilities := make(map[string]string)

		for _, line := range lines {
			if strings.HasPrefix(line, "+ OSVDB-") {
				fields := strings.SplitN(line, ":", 3)
				vulnID := strings.TrimSpace(fields[0][2:])
				vulnDesc := strings.TrimSpace(fields[1])

				// Check if the vulnerability ID is not already in the uniqueVulnerabilities map
				if _, exists := uniqueVulnerabilities[vulnID]; !exists {
					uniqueVulnerabilities[vulnID] = vulnDesc
				}
			}
		}

		// Convert the uniqueVulnerabilities map to a slice of maps
		for vulnID, vulnDesc := range uniqueVulnerabilities {
			vulnerabilitiesInfo = append(vulnerabilitiesInfo, map[string]string{vulnID: vulnDesc})
		}
	}

	// Store the processed vulnerability information in the object "results".
	results["processedOutput"] = vulnerabilitiesInfo

	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
