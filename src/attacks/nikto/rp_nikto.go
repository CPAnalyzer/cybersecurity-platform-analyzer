package main

import (
	nw "attacks/nikto/niktoWorkflow"
	"encoding/json"
)

var Name = "nikto"

func TableGenerator(results map[string]interface{}) []byte {
	return niktoTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	// Hydra supports JSON output so we don't handle that
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return nw.ExtractVulnerabilities(results)
}

func niktoTableGenerator(results map[string]interface{}) []byte {
	bytes_final, _ := json.Marshal(results)
	return bytes_final
}
