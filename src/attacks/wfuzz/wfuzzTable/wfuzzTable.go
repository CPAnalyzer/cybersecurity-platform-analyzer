package wfuzzTable

import (
	aa "attack-manager/AttackAbstraction"
	"encoding/json"
	"log"
	"reflect"
)

func WfuzzTableGenerator(results map[string]interface{}) []byte {
	log.Println("We are generating wfuzz table")
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {
		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			resultsjsonrun := resultsjson["wfuzzrun"].(map[string]interface{})
			var output []byte
			output = internalTableGeneration(resultsjsonrun)
			var output_schema []interface{}
			json.Unmarshal(output, &output_schema)

			indiv_attack.(map[string]interface{})["table"] = output_schema
		}
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

// processes the results assuming that will have wfuzz syntax and returns the processed results marshaled
func internalTableGeneration(results map[string]interface{}) []byte {
	var finalResult aa.ResultTable
	var col aa.Element
	var row aa.Row

	//0 hosts where up
	if results["runstats"].(map[string]interface{})["hosts"].(map[string]interface{})["@up"].(string) == "0" {
		col.Value = "0 hosts up"
		row.AddColumn(col)
		finalResult.AddRow(row)
	} else {
		hosts := results["host"]

		//If the wfuzz was done with just one host, the type is map, if there are other hosts, it's a slice
		type1 := reflect.TypeOf(hosts)

		switch type1.Kind() {
		case reflect.Slice: //Several Hosts
			for _, host := range hosts.([]interface{}) {
				col.Value = "IP"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = host.(map[string]interface{})["address"].(map[string]interface{})["@addr"].(string) //.(map[string]interface{})["@addr"]
				row.AddColumn(col)

				col.Value = "Hostname"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = host.(map[string]interface{})["hostnames"].(map[string]interface{})["hostname"].([]interface{})[0].(map[string]interface{})["@name"].(string) //.(map[string]interface{})["@addr"]
				row.AddColumn(col)

				finalResult.AddRow(row)
				row.Columns = nil

				col.Value = "Open Ports"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Service"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Protocol"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Reason"
				col.Color = "true"
				row.AddColumn(col)
				col.Color = ""

				finalResult.AddRow(row)
				row.Columns = nil

				port_type := reflect.TypeOf(host.(map[string]interface{})["ports"].(map[string]interface{})["port"])
				switch port_type.Kind() {
				case reflect.Map: //Just one port TODO: what happens if 0??

					col.Value = host.(map[string]interface{})["ports"].(map[string]interface{})["port"].(map[string]interface{})["@portid"].(string)
					row.AddColumn(col)

					col.Value = host.(map[string]interface{})["ports"].(map[string]interface{})["port"].(map[string]interface{})["service"].(map[string]interface{})["@name"].(string)
					row.AddColumn(col)

					col.Value = host.(map[string]interface{})["ports"].(map[string]interface{})["port"].(map[string]interface{})["@protocol"].(string)
					row.AddColumn(col)

					col.Value = host.(map[string]interface{})["ports"].(map[string]interface{})["port"].(map[string]interface{})["state"].(map[string]interface{})["@reason"].(string)
					row.AddColumn(col)

					finalResult.AddRow(row)
					row.Columns = nil

				case reflect.Slice:
					for _, open_port := range host.(map[string]interface{})["ports"].(map[string]interface{})["port"].([]interface{}) {

						col.Value = open_port.(map[string]interface{})["@portid"].(string)
						row.AddColumn(col)

						col.Value = open_port.(map[string]interface{})["service"].(map[string]interface{})["@name"].(string)
						row.AddColumn(col)

						col.Value = open_port.(map[string]interface{})["@protocol"].(string)
						row.AddColumn(col)

						col.Value = open_port.(map[string]interface{})["state"].(map[string]interface{})["@reason"].(string)
						row.AddColumn(col)

						finalResult.AddRow(row)
						row.Columns = nil
					}
				}
			}
		case reflect.Map: //Just one host
			host := results["host"]
			//Fill Row 0
			col.Value = "IP"
			col.Color = "true"
			row.AddColumn(col)

			col.Value = host.(map[string]interface{})["address"].(map[string]interface{})["@addr"].(string)
			row.AddColumn(col)

			col.Value = "Hostname"
			col.Color = "true"
			row.AddColumn(col)

			if host.(map[string]interface{})["hostnames"] != "" {
				hostname := reflect.TypeOf(host.(map[string]interface{})["hostnames"].(map[string]interface{})["hostname"])
				switch hostname.Kind() {
				case reflect.Slice: //We just pick the first hostname
					col.Value = host.(map[string]interface{})["hostnames"].(map[string]interface{})["hostname"].([]interface{})[0].(map[string]interface{})["@name"].(string)
					row.AddColumn(col)
				case reflect.Map:
					col.Value = host.(map[string]interface{})["hostnames"].(map[string]interface{})["hostname"].(map[string]interface{})["@name"].(string)
					row.AddColumn(col)
				}
				finalResult.AddRow(row)
				row.Columns = nil

				col.Value = "Open Ports"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Service"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Protocol"
				col.Color = "true"
				row.AddColumn(col)

				col.Value = "Reason"
				col.Color = "true"
				row.AddColumn(col)
				col.Color = ""

				finalResult.AddRow(row)
				row.Columns = nil
			}

			if host.(map[string]interface{})["ports"] != "" {
				port_type := reflect.TypeOf(host.(map[string]interface{})["ports"].(map[string]interface{})["port"])
				switch port_type.Kind() {
				case reflect.Map:

					col.Value = host.(map[string]interface{})["ports"].(map[string]interface{})["port"].(map[string]interface{})["@portid"].(string)
					row.AddColumn(col)

					col.Value = host.(map[string]interface{})["ports"].(map[string]interface{})["port"].(map[string]interface{})["service"].(map[string]interface{})["@name"].(string)
					row.AddColumn(col)

					col.Value = host.(map[string]interface{})["ports"].(map[string]interface{})["port"].(map[string]interface{})["@protocol"].(string)
					row.AddColumn(col)

					col.Value = host.(map[string]interface{})["ports"].(map[string]interface{})["port"].(map[string]interface{})["state"].(map[string]interface{})["@reason"].(string)
					row.AddColumn(col)

					finalResult.AddRow(row)
					row.Columns = nil

				case reflect.Slice:
					for _, open_port := range host.(map[string]interface{})["ports"].(map[string]interface{})["port"].([]interface{}) {

						col.Value = open_port.(map[string]interface{})["@portid"].(string)

						col.Value = open_port.(map[string]interface{})["service"].(map[string]interface{})["@name"].(string)
						row.AddColumn(col)

						col.Value = open_port.(map[string]interface{})["@protocol"].(string)
						row.AddColumn(col)

						col.Value = open_port.(map[string]interface{})["state"].(map[string]interface{})["@reason"].(string)
						row.AddColumn(col)

						finalResult.AddRow(row)
						row.Columns = nil
					}
				}
			}

		}
	}
	bytes_final, _ := json.Marshal(finalResult)
	return bytes_final
}
