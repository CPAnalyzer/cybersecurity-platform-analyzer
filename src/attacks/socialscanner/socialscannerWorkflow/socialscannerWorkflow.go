package socialscannerworkflow

import (
	"encoding/json"
)

func ExtractSocialInfo(results map[string]interface{}) []byte {
	var socialInfo []map[string]string

	// Asumiendo que 'commandsresult' es un campo que existe en 'results'.
	for _, indivAttack := range results["commandsresult"].([]interface{}) {
		// Verificar si 'resultsjson' existe dentro de 'indivAttack'.
		indivAttackMap, ok := indivAttack.(map[string]interface{})
		if !ok {
			continue // Si indivAttack no es un mapa, pasa al siguiente.
		}
		resultsjson, ok := indivAttackMap["resultsjson"].(map[string]interface{})
		if !ok {
			continue // Si 'resultsjson' no es un mapa, pasa al siguiente.
		}
		// Verificar si 'detected' es un array dentro de 'resultsjson'.
		detected, ok := resultsjson["detected"].([]interface{})
		if !ok {
			continue // Si 'detected' no es un array, pasa al siguiente.
		}
		// Iterar sobre cada elemento en 'detected'.
		for _, d := range detected {
			detectedMap, ok := d.(map[string]interface{})
			if !ok {
				continue // Si el elemento no es un mapa, pasa al siguiente.
			}
			// Extraer 'link' y 'status' si están disponibles.
			link, linkOk := detectedMap["link"].(string)
			status, statusOk := detectedMap["status"].(string)
			if linkOk && statusOk {
				// Agregar la información al slice.
				socialInfo = append(socialInfo, map[string]string{link: status})
			}
		}
	}

	// Almacenar la información procesada en el objeto 'results'.
	results["processedOutput"] = socialInfo

	// Convertir 'results' a JSON.
	schemaBytes, _ := json.Marshal(results)
	return schemaBytes
}
