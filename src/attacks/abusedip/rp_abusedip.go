package main

import (
	nm "attacks/abusedip/abusedipTable"
	nmw "attacks/abusedip/abusedipWorkflow"
	"encoding/json"
)

var Name = "abusedip"

func TableGenerator(results map[string]interface{}) []byte {
	return nm.AbusedipTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	// Hydra supports JSON output so we don't handle that
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return nmw.ExtractIpReport(results)
}
