package main

import (
	tw "attacks/traceroute/tracerouteWorkflow"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
)

var Name = "traceroute"

func TableGenerator(results map[string]interface{}) []byte {
	return tracerouteTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	data := make(map[string]interface{})

	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")
		lineCount := 0
		var hops []map[string]interface{}

		for _, line := range lines {
			if lineCount == 0 {
				destinationRegex := regexp.MustCompile(`\btraceroute\s+to\s+([a-zA-Z0-9.-]+)\s*\((\d+\.\d+\.\d+\.\d+)\)`)
				matches := destinationRegex.FindStringSubmatch(line)

				// Imprimir la información encontrada
				if len(matches) >= 3 {
					data["domain"] = matches[1]
					data["ip"] = matches[2]
				} else {
					data["error"] = line
				}
			}
			hopeInfo := make(map[string]interface{})
			if strings.Contains(line, strconv.Itoa(lineCount)) {
				if strings.Contains(line, "* * *") {
					hopeInfo["rtt1"] = "*"
					hopeInfo["rtt2"] = "*"
					hopeInfo["rtt3"] = "*"
				} else {
					regex := regexp.MustCompile(`^\s*(\d+\s+){0,4}([\w.-]+)\s+\(([\d.]+)\)\s+(\d+\.\d+)\s+ms\s+(\d+\.\d+)\s+ms\s+(\d+\.\d+)\s+ms`)
					matches := regex.FindStringSubmatch(line)

					if len(matches) >= 7 {
						domain := matches[2]
						ip := matches[3]
						rtt1 := matches[4]
						rtt2 := matches[5]
						rtt3 := matches[6]

						hopeInfo["domain"] = domain
						hopeInfo["ip"] = ip

						if rtt1 != "" {
							hopeInfo["rtt1"] = rtt1
						} else {
							hopeInfo["rtt1"] = "*"
						}

						if rtt1 != "" {
							hopeInfo["rtt2"] = rtt2
						} else {
							hopeInfo["rtt2"] = "*"
						}

						if rtt1 != "" {
							hopeInfo["rtt3"] = rtt3
						} else {
							hopeInfo["rtt3"] = "*"
						}
					}
				}
			}

			if strings.Contains(line, "unknown host") {
				fmt.Println("Unknown host")
				data["error"] = "Unknown host"
				break
			}
			if len(hopeInfo) != 0 {
				hops = append(hops, hopeInfo)
			}
			lineCount++
		}
		data["hops"] = hops
		data["hops_number"] = lineCount
	}

	// Store the processed hop information in the object "results".
	results["processedOutput"] = data
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return tw.ExtractInfo(results)
}

func tracerouteTableGenerator(results map[string]interface{}) []byte {
	bytes_final, _ := json.Marshal(results)
	return bytes_final
}
