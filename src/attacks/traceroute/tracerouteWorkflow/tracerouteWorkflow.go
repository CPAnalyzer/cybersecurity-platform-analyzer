package tracerouteWorkflow

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"
)

/*
"processedOutput": [

	{
	  "192.168.121.1": "1"
	},
	{
	  "10.10.14.1": "2"
	}

]
*/
func ExtractInfo(results map[string]interface{}) []byte {
	var hopInfo []map[string]string

	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")

		for _, line := range lines {
			if strings.Contains(line, "ms") {
				fields := strings.Fields(line)
				hop := fields[0]
				ip := fields[1]

				hopInfo = append(hopInfo, map[string]string{ip: hop})
			}
		}
	}

	// Store the processed hop information in the object "results".
	results["processedOutput"] = hopInfo

	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
