package nucleiworkflow

import (
	"encoding/json"
)

func ExtractNucleiReport(results map[string]interface{}) []byte {
	var NucleInfo []map[string]string

	// Asumiendo que 'commandsresult' siempre está presente y es un slice.
	for _, indivAttack := range results["commandsresult"].([]interface{}) {
		resultsjson := indivAttack.(map[string]interface{})["resultsjson"].(map[string]interface{})

		data, dataExists := resultsjson["detected"].(map[string]interface{})
		if !dataExists {
			continue
		}

		for _, indiv_result := range data {
			severity := indiv_result.(map[string]interface{})["severity"].(string)
			template := indiv_result.(map[string]interface{})["template"].(string)

			NucleInfo = append(NucleInfo, map[string]string{
				template: severity,
			})
		}
	}

	// Agregamos la información procesada a 'results'.
	results["processedOutput"] = NucleInfo

	// Convertimos 'results' a JSON.
	schemaBytes, _ := json.Marshal(results) // En producción, manejar este error.
	return schemaBytes
}
