package gobusterworkflow

import (
	"encoding/json"
	"reflect"
)

func ExtractPortsInfo(results map[string]interface{}) []byte{
	var portInfo []map[string]string

	for _, indiv_attack := range results["commandsresult"].([]interface{}) {
		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			resultsjsonrun := resultsjson["gobusterrun"].(map[string]interface{})
			host := resultsjsonrun["host"].(map[string]interface{})
			ports := host["ports"].(map[string]interface{})

			portType := reflect.TypeOf(ports["port"])
			switch portType.Kind() {
			case reflect.Map:
				port := ports["port"].(map[string]interface{})
				portID := port["@portid"].(string)
				portStatus := port["state"].(map[string]interface{})["@state"].(string)

				portInfo = append(portInfo, map[string]string{
					portID: portStatus,
				})
			case reflect.Slice:
				for _, open_port := range ports["port"].([]interface{}) {
					port := open_port.(map[string]interface{})
					portID := port["@portid"].(string)
					portStatus := port["state"].(map[string]interface{})["@state"].(string)

					portInfo = append(portInfo, map[string]string{
						portID: portStatus,
					})
				}
			}
		}
	}

	// Store the processed port information in the object "results".
	results["processedOutput"] = portInfo

	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
