import json

# Ruta del archivo JSON
json_path = "c:/Users/bruno/huntdown/HuntDown/src/attacks/wpscan/wpscan-options.json"

# Cargar el archivo JSON
with open(json_path, "r", encoding="utf-8") as file:
    data = json.load(file)

# Iterar sobre cada elemento en la lista de datos
for item in data:
    # Verificar si el elemento tiene la clave "options" y es una lista
    if "options" in item and isinstance(item["options"], list):
        for option in item["options"]:
            # Verificar las condiciones para determinar el valor de "formattype"
            if "needcheckbox" in option and "textinput" in option:
                if not option["needcheckbox"] and option["textinput"]:
                    option["formattype"] = "text"
                elif option["needcheckbox"] and option["textinput"]:
                    option["formattype"] = "switch-textbox"
                elif option["needcheckbox"] and not option["textinput"]:
                    option["formattype"] = "switch"
            else:
                # Si no cumple ninguna condición, asignar un valor por defecto o dejarlo como está
                option.setdefault("formattype", "select")

# Guardar los cambios en el archivo JSON
with open(json_path, "w", encoding="utf-8") as file:
    json.dump(data, file, indent=4, ensure_ascii=False)

print("Parámetros 'formattype' añadidos correctamente según las condiciones especificadas.")
