package main

import (
	"encoding/json"
)

var Name = "gobuster"

func TableGenerator(results map[string]interface{}) []byte {
	out, _ := json.Marshal(results)
	return out
}

func ResultProcessor(results map[string]interface{}) []byte {
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	out, _ := json.Marshal(results)
	return out
}

func gobusterTableGenerator(results map[string]interface{}) []byte {
	bytes_final, _ := json.Marshal(results)
	return bytes_final
}
