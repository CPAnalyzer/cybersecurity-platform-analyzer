#!/bin/bash

DEPLOY_BINARY=../database/DeployAttack/hd-deploy-attack

if [ -z $MONGO_HOST ] ; then
    MONGO_HOST=localhost
fi

if [ -z $MONGO_USER ] ; then
    echo You must export MONGO_USER variable
    exit 3
fi

if [ -z $MONGO_PASSWORD ] ; then
    echo You must export MONGO_PASSWORD variable
    exit 3
fi

if [ -z $1 ] ; then
    echo Specify an attack directory
    exit 2
fi

if [ ! -d $1 ] ; then
    echo Specify an existing directory
    exit 1
fi
ATTACK=${1/\//}

CWD=$(pwd)

cd $ATTACK
${CWD}/${DEPLOY_BINARY} -attack-schema $ATTACK-attack.json \
    -options-schema $ATTACK-options.json \
    -dashboard-schema $ATTACK-dashboard.json \
    -mongo-user $MONGO_USER -mongo-host $MONGO_HOST \
    -mongo-pass $MONGO_PASSWORD
cd ..
