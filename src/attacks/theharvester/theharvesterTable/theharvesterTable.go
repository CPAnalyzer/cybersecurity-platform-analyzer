package theharvestertable

import (
	aa "attack-manager/AttackAbstraction"
	"encoding/json"
)

// processes the results assuming that will have hydra syntax and returns the processed results marshaled
func TheharvesterTableGenerator(results map[string]interface{}) []byte {
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			output := internalTableGeneration(resultsjson)
			var output_schema []interface{}
			json.Unmarshal(output, &output_schema)

			indiv_attack.(map[string]interface{})["table"] = output_schema
		}
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

func maxOfThree(a, b, c int) int {
	max := a
	if b > max {
		max = b
	}
	if c > max {
		max = c
	}
	return max
}

func internalTableGeneration(results map[string]interface{}) []byte {
	var finalResult aa.ResultTable
	var col aa.Element
	var row aa.Row

	col.Value = "IP"
	col.Color = "true"
	row.AddColumn(col)

	col.Value = "Email"
	col.Color = "true"
	row.AddColumn(col)

	col.Value = "Hosts" //La variable hosts siempre esta
	col.Color = "true"
	row.AddColumn(col)
	col.Color = ""
	ips, ok1 := results["ips"].([]interface{})
	emails, ok2 := results["emails"].([]interface{})
	hosts, _ := results["hosts"].([]interface{})
	max := maxOfThree(len(ips), len(emails), len(hosts))
	finalResult.AddRow(row)
	row.Columns = nil

	for i := 0; i < max; i++ {
		if ok1 && i < len(ips) {
			col.Value = ips[i].(string)
			row.AddColumn(col)
		} else {
			col.Value = "--"
			row.AddColumn(col)
		}

		if ok2 && i < len(emails) {
			col.Value = emails[i].(string)
			row.AddColumn(col)
		} else {
			col.Value = "--"
			row.AddColumn(col)
		}

		if i < len(hosts) {
			col.Value = hosts[i].(string)
			row.AddColumn(col)
		} else {
			col.Value = "--"
			row.AddColumn(col)
		}

		if i < max-1 {
			finalResult.AddRow(row)
			row.Columns = nil
		}
	}
	if max == 0 {
		for i := 0; i < 3; i++ {
			col.Value = "--"
			row.AddColumn(col)
		}
	}

	finalResult.AddRow(row)
	row.Columns = nil

	bytes_final, _ := json.Marshal(finalResult)
	return bytes_final
}
