package theharvesterworkflow

import (
	"encoding/json"
)

func ExtractHarvesterInfo(results map[string]interface{}) []byte {
	var harvesterInfo []string

	// Asumiendo que 'commandsresult' siempre está presente y es un slice.
	for _, indivAttack := range results["commandsresult"].([]interface{}) {
		resultsjson := indivAttack.(map[string]interface{})["resultsjson"].(map[string]interface{})

		// Obtenemos 'country_name' y 'region_name', que serán cadenas vacías si no existen.

		hosts := resultsjson["hosts"].([]interface{})

		if len(hosts) != 0 {
			for _, host := range hosts {
				harvesterInfo = append(harvesterInfo, host.(string))
			}
		}
	}

	// Agregamos la información procesada a 'results'.
	results["processedOutput"] = harvesterInfo

	// Convertimos 'results' a JSON.
	schemaBytes, _ := json.Marshal(results) // En producción, manejar este error.
	return schemaBytes
}
