package main

import (
	nm "attacks/theharvester/theharvesterTable"
	nmw "attacks/theharvester/theharvesterWorkflow"
	"encoding/json"
)

var Name = "theharvester"

func TableGenerator(results map[string]interface{}) []byte {
	return nm.TheharvesterTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	// Hydra supports JSON output so we don't handle that
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return nmw.ExtractHarvesterInfo(results)
}
