package main

import (
	sw "attacks/slowloris/slowlorisWorkflow"
	"encoding/base64"
	"encoding/json"
	"log"
	"regexp"
	"strconv"
	"strings"
)

var Name = "slowloris"

type SlowlorisData struct {
	Initialized int  `json:"initialized"`
	Pending     int  `json:"pending"`
	Connected   int  `json:"connected"`
	Error       int  `json:"error"`
	Closed      int  `json:"closed"`
	Available   bool `json:"available"`
}

func TableGenerator(results map[string]interface{}) []byte {
	return slowlorisTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	data := SlowlorisData{}
	available := false
	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)
		lines := strings.Split(output, "\n")

		for _, line := range lines {
			if initializedMatch := regexp.MustCompile(`Initialized:\s*(\d+)`).FindStringSubmatch(line); len(initializedMatch) == 2 {
				data.Initialized, _ = strconv.Atoi(initializedMatch[1])
			}

			if pendingMatch := regexp.MustCompile(`Pending:\s*(\d+)`).FindStringSubmatch(line); len(pendingMatch) == 2 {
				data.Pending, _ = strconv.Atoi(pendingMatch[1])
			}

			if connectedMatch := regexp.MustCompile(`Connected:\s*(\d+)`).FindStringSubmatch(line); len(connectedMatch) == 2 {
				data.Connected, _ = strconv.Atoi(connectedMatch[1])
			}

			if errorMatch := regexp.MustCompile(`Error:\s*(\d+)`).FindStringSubmatch(line); len(errorMatch) == 2 {
				data.Error, _ = strconv.Atoi(errorMatch[1])
			}

			if closedMatch := regexp.MustCompile(`Closed:\s*(\d+)`).FindStringSubmatch(line); len(closedMatch) == 2 {
				data.Closed, _ = strconv.Atoi(closedMatch[1])
			}

			if availableMatch := regexp.MustCompile(`Available:\s*(true|false)`).FindStringSubmatch(line); len(availableMatch) == 2 {
				available = availableMatch[1] == "true"
			}
			data.Available = available
		}
	}

	// Store the processed hop information in the object "results".
	results["processedOutput"] = data
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return sw.ExtractInfo(results)
}

func slowlorisTableGenerator(results map[string]interface{}) []byte {
	bytes_final, _ := json.Marshal(results)
	return bytes_final
}
