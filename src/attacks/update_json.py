import json

# Load the JSON file
with open('/home/blueh0rse/code/public/HuntDownUPC/HuntDown/src/attacks/gobuster/gobuster-options.json', 'r') as file:
    data = json.load(file)

# Update each option
for tool in data:
    for option in tool['options']:
        if 'require' not in option:
            option['require'] = []
        if 'conflict' not in option:
            option['conflict'] = []

# Save the updated JSON back to the file
with open('gobuster-options-updated.json', 'w') as file:
    json.dump(data, file, indent=4)
