package hping3Workflow

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"
)

/*
"processedOutput": [

	  {
	    "192.168.1.39": "alive"
	  }
	]
*/

func ExtractHostStatus(results map[string]interface{}) []byte {
	var hostStatusInfo []map[string]string

	if base64Output, ok := results["commandsresult"].([]interface{})[0].(map[string]interface{})["resultsoutput"].(string); ok {
		outputBytes, err := base64.StdEncoding.DecodeString(base64Output)
		if err != nil {
			log.Println("Error decoding base64:", err)
			return nil
		}
		output := string(outputBytes)

		tmp_res := results["commandsresult"].([]interface{})
		if len(tmp_res) == 0 {
			log.Println("No results provided for hping3 command")
			return nil
		}

		command_info := tmp_res[0].(map[string]interface{})["commandinfo"].(map[string]interface{})
		options := command_info["options"].(string)

		if options == "" {
			log.Println("No options provided for hping3 command")
			return nil
		}

		// Extract the host from the options
		host := strings.Fields(options)[0]

		/*
			for _, line := range lines {
				if strings.Contains(line, "packet loss") {
					if strings.Contains(line, "100%") {
						hostStatusInfo = append(hostStatusInfo, map[string]string{host: "dead"})
					} else if strings.Contains(line, "0%") {
						hostStatusInfo = append(hostStatusInfo, map[string]string{host: "alive"})
					}
				}
			}
		*/

		if strings.Contains(output, "seq=") {
			hostStatusInfo = append(hostStatusInfo, map[string]string{host: "alive"})
		} else {
			hostStatusInfo = append(hostStatusInfo, map[string]string{host: "dead"})
		}

	}

	// Store the processed host status information in the object "results".
	results["processedOutput"] = hostStatusInfo

	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}
