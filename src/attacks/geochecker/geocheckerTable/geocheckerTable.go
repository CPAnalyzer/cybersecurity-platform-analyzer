package geocheckertable

import (
	aa "attack-manager/AttackAbstraction"
	"encoding/json"
	"fmt"
)

// processes the results assuming that will have hydra syntax and returns the processed results marshaled
func GeocheckerTableGenerator(results map[string]interface{}) []byte {
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			output := internalTableGeneration(resultsjson)
			var output_schema []interface{}
			json.Unmarshal(output, &output_schema)

			indiv_attack.(map[string]interface{})["table"] = output_schema
		}
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

func internalTableGeneration(results map[string]interface{}) []byte {
	var finalResult aa.ResultTable
	var col aa.Element
	var row aa.Row

	//si la key introducida no es correcta
	if _, ok := results["error"].(map[string]interface{}); ok {
		col.Value = "Error"
		col.Color = "True"
		row.AddColumn(col)

		finalResult.AddRow(row)
		row.Columns = nil

		error := results["error"].(map[string]interface{})
		col.Value = error["type"].(string)
		row.AddColumn(col)
	} else {
		if results["city"] != nil { //Si algun campo es nulo es pq la IP no se encuentra
			col.Value = "Country"
			col.Color = "true"
			row.AddColumn(col)

			col.Value = "Continent"
			col.Color = "true"
			row.AddColumn(col)

			col.Value = "Region"
			col.Color = "true"
			row.AddColumn(col)

			col.Value = "Latitude"
			col.Color = "true"
			row.AddColumn(col)

			col.Value = "Longitude"
			col.Color = "true"
			row.AddColumn(col)

			col.Value = "IP"
			col.Color = "true"
			row.AddColumn(col)
			col.Color = ""

			finalResult.AddRow(row)
			row.Columns = nil

			col.Value = results["country_name"].(string)
			row.AddColumn(col)

			col.Value = results["continent_name"].(string)
			row.AddColumn(col)

			col.Value = results["region_name"].(string)
			row.AddColumn(col)

			col.Value = fmt.Sprint(results["latitude"].(float64))
			row.AddColumn(col)

			col.Value = fmt.Sprint(results["longitude"].(float64))
			row.AddColumn(col)

			col.Value = results["ip"].(string)
			row.AddColumn(col)
		} else {
			col.Value = "Error"
			col.Color = "true"
			row.AddColumn(col)

			finalResult.AddRow(row)
			row.Columns = nil
			col.Value = "Must be a public IP address"
			row.AddColumn(col)
		}
	}
	finalResult.AddRow(row)
	row.Columns = nil

	bytes_final, _ := json.Marshal(finalResult)
	return bytes_final
}
