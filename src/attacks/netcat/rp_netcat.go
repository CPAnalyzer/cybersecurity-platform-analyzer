package main

import (
	nw "attacks/netcat/netcatWorkflow"
	"encoding/json"
)

var Name = "netcat"

func TableGenerator(results map[string]interface{}) []byte {
	return netcatTableGenerator(results)
}

func ResultProcessor(results map[string]interface{}) []byte {
	// Hydra supports JSON output so we don't handle that
	out, _ := json.Marshal(results)
	return out
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	return nw.ExtractInfo(results)
}

func netcatTableGenerator(results map[string]interface{}) []byte {
	bytes_final, _ := json.Marshal(results)
	return bytes_final
}
