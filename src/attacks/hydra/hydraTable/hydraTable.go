package hydratable

import (
	aa "attack-manager/AttackAbstraction"
	"encoding/json"
	"fmt"
)

// processes the results assuming that will have hydra syntax and returns the processed results marshaled
func HydraTableGenerator(results map[string]interface{}) []byte {
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {

		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsjson"] != nil {
			resultsjson := indiv_attack.(map[string]interface{})["resultsjson"].(map[string]interface{})
			output := internalTableGeneration(resultsjson)
			var output_schema []interface{}
			json.Unmarshal(output, &output_schema)

			indiv_attack.(map[string]interface{})["table"] = output_schema
		}
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

func internalTableGeneration(results map[string]interface{}) []byte {
	var finalResult aa.ResultTable
	var col aa.Element
	var row aa.Row

	//0 hosts where up
	if len(results["results"].([]interface{})) == 0 && len(results["errormessages"].([]interface{})) != 0 {
		for _, err := range results["errormessages"].([]interface{}) {
			col.Value = err.(string)
			row.AddColumn(col)
			finalResult.AddRow(row)
			row.Columns = nil
		}
	} else {
		col.Value = "Host"
		col.Color = "true"
		row.AddColumn(col)

		col.Value = "Login"
		col.Color = "true"
		row.AddColumn(col)

		col.Value = "Password"
		col.Color = "true"
		row.AddColumn(col)

		col.Value = "Port"
		col.Color = "true"
		row.AddColumn(col)

		col.Value = "Service"
		col.Color = "true"
		row.AddColumn(col)
		col.Color = ""

		finalResult.AddRow(row)
		row.Columns = nil

		for _, indiv_result := range results["results"].([]interface{}) {
			col.Value = indiv_result.(map[string]interface{})["host"].(string)
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["login"].(string)
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["password"].(string)
			row.AddColumn(col)

			col.Value = fmt.Sprint(int(indiv_result.(map[string]interface{})["port"].(float64)))
			row.AddColumn(col)

			col.Value = indiv_result.(map[string]interface{})["service"].(string)
			row.AddColumn(col)

			finalResult.AddRow(row)
			row.Columns = nil

		}
	}
	bytes_final, _ := json.Marshal(finalResult)
	return bytes_final
}
