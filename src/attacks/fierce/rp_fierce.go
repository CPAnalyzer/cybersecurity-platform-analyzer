package main

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"

	nmw "attacks/fierce/fierceWorkflow"
)

var Name = "fierce"

type fierceResult struct {
	WorkflowResult string   `json:"workflowresult"`
	NS             []string `json:"ns"`
	SOA            []struct {
		Ip string `json:"ip"`
		Ns string `json:"ns"`
	} `json:"soa"`
	Zone     string `json:"zone"`
	Wildcard string `json:"wildcard"`
	Found    []struct {
		Ip      string `json:"ip"`
		DnsName string `json:"dnsname"`
		Nearby  []struct {
			Ip      string `json:"ip"`
			DnsName string `json:"dnsname"`
		} `json:"nearby"`
	} `json:"found"`
}

func TableGenerator(results map[string]interface{}) []byte {
	out, _ := json.Marshal(results)
	return out
}

func ResultProcessor(results map[string]interface{}) []byte {
	log.Println("fierce ResultProcessor")

	return fierceResultGenerator(results)
}

func WorkflowProcessor(results map[string]interface{}) []byte {
	log.Println("fierce WorkflowProcessor")

	return nmw.ExtractIpsInfo(results)

}

func fierceResultGenerator(results map[string]interface{}) []byte {
	log.Println("We are processing fierce results")
	for _, indiv_attack := range results["commandsresult"].([]interface{}) {
		if indiv_attack != nil && indiv_attack.(map[string]interface{})["resultsoutput"] != nil {

			resultsoutput, err := base64.StdEncoding.DecodeString(indiv_attack.(map[string]interface{})["resultsoutput"].(string))

			if err != nil {
				log.Println(err)
			}

			json_result := make(map[string]interface{})

			res := FierceParser(resultsoutput)

			data, err := json.Marshal(res)

			if err != nil {
				log.Println(err)
			}

			err = json.Unmarshal(data, &json_result)

			if err != nil {
				log.Println(err)
			}

			indiv_attack.(map[string]interface{})["resultsjson"] = json_result
		}
	}
	schema_bytes, _ := json.Marshal(results)
	return schema_bytes
}

func FierceParser(resultsoutput []byte) fierceResult {
	res := fierceResult{}

	lines := strings.Split(string(resultsoutput), "\n")

	var currentFound struct {
		Ip      string `json:"ip"`
		DnsName string `json:"dnsname"`
		Nearby  []struct {
			Ip      string `json:"ip"`
			DnsName string `json:"dnsname"`
		} `json:"nearby"`
	}

	var currentSOA struct {
		Ip string `json:"ip"`
		Ns string `json:"ns"`
	}

	for _, line := range lines {
		parts := strings.SplitN(line, ": ", 2)
		if len(parts) != 2 {
			continue
		}

		key := strings.ToLower(parts[0])
		value := strings.TrimSpace(parts[1])

		switch key {
		case "ns":
			res.NS = strings.Fields(value)
		case "soa":
			soaParts := strings.SplitN(value, " ", 2)
			if len(soaParts) != 2 {
				log.Println("Invalid SOA value")
				break
			}
			currentSOA = struct {
				Ip string `json:"ip"`
				Ns string `json:"ns"`
			}{
				Ip: strings.Trim(soaParts[1], "()"),
				Ns: soaParts[0],
			}
			res.SOA = append(res.SOA, currentSOA)
		case "zone":
			res.Zone = value
		case "wildcard":
			res.Wildcard = value
		case "found":
			if currentFound.Ip != "" {
				res.Found = append(res.Found, currentFound)
			}

			foundParts := strings.SplitN(value, " ", 2)
			if len(foundParts) != 2 {
				log.Println("Invalid found value")
			}
			currentFound = struct {
				Ip      string `json:"ip"`
				DnsName string `json:"dnsname"`
				Nearby  []struct {
					Ip      string `json:"ip"`
					DnsName string `json:"dnsname"`
				} `json:"nearby"`
			}{
				Ip:      strings.Trim(foundParts[1], "()'"),
				DnsName: strings.Trim(foundParts[0], "'"),
			}
		default: // it's used for the nearby part
			if strings.HasPrefix(key, " ") || strings.HasPrefix(key, "{") {
				if currentFound.Ip == "" {
					log.Println("Invalid nearby line")
				}

				nearbyParts := strings.SplitN(strings.TrimSpace(line), ": ", 2)
				if len(nearbyParts) != 2 {
					log.Println("Invalid nearby line")
				}

				dnsname := strings.TrimSuffix(strings.Trim(strings.Trim(nearbyParts[1], "'"), "'}"), "',")
				currentNearby := struct {
					Ip      string `json:"ip"`
					DnsName string `json:"dnsname"`
				}{
					Ip:      strings.Trim(nearbyParts[0], "'"),
					DnsName: dnsname,
				}

				currentFound.Nearby = append(currentFound.Nearby, currentNearby)
			}
		}
	}

	if currentFound.Ip != "" {
		res.Found = append(res.Found, currentFound)
	}

	return res
}
