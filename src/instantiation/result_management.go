package main

import (
	"encoding/json"

	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func getResult(attack_id string, session_id string) map[string]interface{} {
	query := make(map[string]interface{})
	query["session_id"] = session_id
	query["attack_id"] = attack_id
	res := PulsarLib.BuildMessage(query)

	result_raw := PulsarLib.SendRequestSync(global_client, "ui-db.getResult", res)
	var result map[string]interface{}
	json.Unmarshal(result_raw, &result)
	return result
}
