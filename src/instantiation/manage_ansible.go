package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strings"

	as "helpers/AttackSchema"

	me "helpers/MonitorEvents"

	"github.com/apenella/go-ansible/pkg/stdoutcallback/results"
	"github.com/noirbizarre/gonja"
	log "github.com/sirupsen/logrus"
)

type AnsibleStatus struct {
	Action  string `json:"action"`
	Block   string `json:"block"`
	Status  string `json:"status"`
	Message string `json:"message"`
}

func initTask(monitor *me.MonitorContext, current_action *AnsibleStatus, task string) {
	current_action.Action = "TASK"
	current_action.Block = task
	current_action.Status = ""
	current_action.Message = ""
	if task == "Execute Attack" {
		me.SendAndIncreaseMonitorEvent(monitor, "Executing attack this may take a while...")
	}
}

func updateTask(current_action *AnsibleStatus, message string) {
	words := []string{"ok", "changed", "unreachable", "fatal", "skipped", "rescued", "ignored"}
	re := regexp.MustCompile(fmt.Sprintf(`^(%s): \[(.*)\]`, strings.Join(words, "|")))
	result := re.FindStringSubmatch(message)
	if len(result) > 2 {
		current_action.Status = result[1]
		current_action.Message = message
		return
	}
	current_action.Message = current_action.Message + "\n" + message
}

func isOngoingTask(current_action *AnsibleStatus) bool {
	return current_action.Action == "TASK"
}

func isFatalTask(current_action *AnsibleStatus) bool {
	return isOngoingTask(current_action) && current_action.Status == "fatal"
}

func isFinishedTask(current_action *AnsibleStatus) bool {
	return isOngoingTask(current_action) && current_action.Status != ""
}

func emptyStatus(current_action *AnsibleStatus) {
	current_action.Action = ""
	current_action.Block = ""
	current_action.Status = ""
	current_action.Message = ""
}

func interpretOutput(monitor *me.MonitorContext, current_action *AnsibleStatus) results.TransformerFunc {
	return func(message string) string {
		if isOngoingTask(current_action) {
			updateTask(current_action, message)
			if isFatalTask(current_action) {
				me.SendMonitorError(monitor, fmt.Errorf(current_action.Message))
				emptyStatus(current_action)
			} else if isFinishedTask(current_action) {
				me.SendMonitorEvent(monitor, current_action.Block, current_action.Message)
				emptyStatus(current_action)
			}
			return message
		}
		re := regexp.MustCompile(`([A-Z]+) \[(.*?)\] \*+$`)
		result := re.FindStringSubmatch(message)
		if len(result) > 2 {
			switch result[1] {
			case "TASK":
				initTask(monitor, current_action, result[2])
			default:
				log.Info("Unknown ansible status")
			}
		}
		return message
	}
}

func generate_ansible_playbook(the_attack as.AttackSchema) (bool, error) {
	var playbook_file = etc_dir + "/playbook-" + the_attack.TypeAttack + ".template"

	_, err := os.Stat(playbook_file)
	if os.IsNotExist(err) {
		my_err := errors.New("Error, " + playbook_file + " does not exist")
		log.Error(my_err.Error())
		return false, my_err
	}
	/* Load the template into the tpl variable */
	var tpl = gonja.Must(gonja.FromFile(playbook_file))

	/* Reading the machine_definition json */
	x := gonja.Context{}
	structToMap(the_attack, &x)

	/* Write the template with the given variables */
	out, err := tpl.Execute(gonja.Context(x))
	if err != nil {
		return false, err
	}

	/* Create the attack.yml file in where we will write the completed template */
	f, err := os.Create(var_dir + "/attack.yml")
	if err != nil {
		return false, err
	}

	defer f.Close()

	/* Write the completed template in to the attack.yml file */
	_, err2 := f.WriteString(out)
	if err2 != nil {
		return false, err2
	}
	return true, nil
}
