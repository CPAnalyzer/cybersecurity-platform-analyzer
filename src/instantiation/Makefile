include ../Makefile.inc
BINARY_NAME=hd-instantiation
SRC=main.go manage_machines.go manage_vagrant.go manage_ansible.go manage_docker.go manage_webrunner.go handle_errors.go result_management.go

INSTANTIATION_SERVICE=/lib/systemd/system/huntdown-instantiation.service
INSTANTIATION_DOCKER_SERVICE=/lib/systemd/system/huntdown-instantiation-docker.service

build: dep ${SRC}
	$(GO_BUILD) -o ${BINARY_NAME} ${SRC}

debug: dep ${SRC}
	$(GO_BUILD) $(DEBUG_FLAGS) -o ${BINARY_NAME} ${SRC}

build_dev: dep_dev ${SRC}
	$(GO_BUILD) $(DEBUG_FLAGS) -o ${BINARY_NAME} ${SRC}

${INSTANTIATION_SERVICE}: systemd/huntdown-instantiation.service.in 
	cat $< | sed s#%HUNTDOWN_PREFIX%#${HUNTDOWN_PREFIX}#g > ${INSTANTIATION_SERVICE}

systemd_install_docker: systemd/huntdown-instantiation-docker.service.in 
	cat $< | sed s#%HUNTDOWN_PREFIX%#${HUNTDOWN_PREFIX}#g > ${INSTANTIATION_DOCKER_SERVICE}

${HUNTDOWN_PREFIX}/bin/${BINARY_NAME}: ${BINARY_NAME} 
	install -D $< --target-directory ${HUNTDOWN_PREFIX}/bin

${HUNTDOWN_CONFIG_DIR}/Vagrantfile.template: Vagrantfile.template
	install -D $< --target-directory ${HUNTDOWN_CONFIG_DIR}

install: ${HUNTDOWN_PREFIX}/bin/${BINARY_NAME} ${INSTANTIATION_SERVICE} ${HUNTDOWN_CONFIG_DIR}/Vagrantfile.template

run:
	./${BINARY_NAME}

clean:
	go clean
	rm ${BINARY_NAME}

test:
	go test ./...

test_coverage:
	go test ./... -coverprofile=coverage.out

dep_dev:
	go get github.com/apenella/go-common-utils
	go get github.com/apenella/go-ansible
	go get gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2@develop
	go mod tidy

dep:
	go get github.com/apenella/go-common-utils
	go get github.com/apenella/go-ansible
	go get gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2

	go get google.golang.org/genproto@latest
	go get github.com/gogo/protobuf
	go get gopkg.in/yaml.v2@v2.4.0
	go get gopkg.in/yaml.v3@v3.0.1
	go get gotest.tools/v3@v3.5.1
	go get github.com/docker/cli/cli/connhelper
	go get github.com/docker/cli@v23.0.3+incompatible
	go mod tidy

lint:
	golangci-lint run --enable-all
