package main

import "encoding/json"

func generateError(err error) []byte {
	result := make(map[string]any)
	result["success"] = false
	result["errormsg"] = err.Error()
	byte_result, _ := json.Marshal(result)
	return byte_result
}

func generateErrorFromString(err string) []byte {
	result := make(map[string]any)
	result["success"] = false
	result["errormsg"] = err
	byte_result, _ := json.Marshal(result)
	return byte_result
}
