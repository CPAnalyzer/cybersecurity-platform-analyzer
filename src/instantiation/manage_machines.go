package main

import (
	"encoding/json"
	"helpers/DatabaseHelper"
	"helpers/GeneralHelper"
	md "helpers/Machine_definitions"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

var global_client PulsarLib.PL_Client

const general_database string = "general"

func searchMachines(session_id int, comm_client *PulsarLib.PL_Client, os_type string) []byte {
	var filters []DatabaseHelper.Filter

	filter := DatabaseHelper.Filter{Field: "os_type", Value: os_type, Op: "$eq"}
	filters = append(filters, filter)
	query := DatabaseHelper.DatabaseQuery{Session_id: session_id,
		Database:   general_database,
		Collection: "Machine_definitions",
		Filters:    filters}
	to_send := PulsarLib.BuildMessage(GeneralHelper.StructToMap(query))
	response := PulsarLib.SendRequestSync(*comm_client, "db.newQuery", to_send)

	var message []md.Machine_definitions
	json.Unmarshal(response, &message)
	var machine md.Machine_definitions

	if len(message) >= 1 {
		machine = message[0]
	} else {
		return []byte("")
	}
	machinebyte, err := json.Marshal(machine)
	if err != nil {
		log.Fatal(err)
	}
	return machinebyte
}

func getMachine(msg []byte) {
	var message PulsarLib.MessageResponse
	log.Println("Getting a machine")
	json.Unmarshal(msg, &message)
	log.Println(string(msg))

	var session_id int
	var err error

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Println("Error malformed get Machine query ", err.Error())
		PulsarLib.SendMessage(global_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoSession))
		return
	}

	var os_type string
	err = GeneralHelper.GetMapElement(message.Msg, "os_type", &os_type)
	if err != nil {
		PulsarLib.SendMessage(global_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.FieldNotFound))
		return
	}
	machine := searchMachines(session_id, &global_client, os_type)
	PulsarLib.SendMessage(global_client, message.Id, machine)
}
