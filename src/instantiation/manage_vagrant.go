package main

import (
	"context"
	"encoding/json"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"

	"github.com/bramvdbogaerde/go-scp/auth"
	"github.com/noirbizarre/gonja"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/ssh"

	as "helpers/AttackSchema"
	md "helpers/Machine_definitions"

	me "helpers/MonitorEvents"

	"github.com/apenella/go-ansible/pkg/execute"
	"github.com/apenella/go-ansible/pkg/options"
	"github.com/apenella/go-ansible/pkg/playbook"
	"github.com/apenella/go-ansible/pkg/stdoutcallback/results"
	vagrant "github.com/bmatcuk/go-vagrant"
)

func generate_vagrantfile(byteValue []byte) (bool, error) {
	var tpl = gonja.Must(gonja.FromFile(etc_dir + "/Vagrantfile.template"))

	log.Println(string(byteValue))
	gonja_context := gonja.Context{}

	err := json.Unmarshal(byteValue, &gonja_context)
	if err != nil {
		return false, err
	}

	log.Println("Vaig a fer print de Context!!!!!!!!!!!!")
	log.Println(gonja_context)

	out, err := tpl.Execute(gonja.Context(gonja_context))
	if err != nil {
		return false, err
	}

	f, err := os.Create(vagrant_base_dir + "/Vagrantfile")
	if err != nil {
		return false, err
	}

	defer f.Close()

	_, err2 := f.WriteString(out)
	if err2 != nil {
		return false, err2
	}
	return true, nil
}

func startVirtualMachine(vagrantClient *vagrant.VagrantClient) error {
	upcmd := vagrantClient.Up()
	upcmd.Provisioners = []string{""}
	upcmd.Provisioning = vagrant.DisableProvisioning
	upcmd.Verbose = true

	if err := upcmd.Run(); err != nil {
		return err
	}
	return nil
}

func provisionVM(vagrantClient *vagrant.VagrantClient, monitor *me.MonitorContext, machine md.Machine_definitions) error {
	cmd_ssh_ex := vagrantClient.SSHConfig()
	err := cmd_ssh_ex.Run()
	if err != nil {
		return err
	}

	ansiblePlaybookConnectionOptions := &options.AnsibleConnectionOptions{
		Connection: "ssh",
		PrivateKey: cmd_ssh_ex.Configs[machine.MachineName].IdentityFile,
		User:       cmd_ssh_ex.Configs[machine.MachineName].User,
	}

	ansiblePlaybookOptions := &playbook.AnsiblePlaybookOptions{
		Inventory: cmd_ssh_ex.Configs[machine.MachineName].HostName + ",",
	}

	var action AnsibleStatus
	execute := execute.NewDefaultExecute(
		execute.WithTransformers(
			interpretOutput(monitor, &action),
			results.LogFormat(results.DefaultLogFormatLayout, results.Now),
		),
	)

	playbook := &playbook.AnsiblePlaybookCmd{
		Playbooks:         []string{var_dir + "/attack.yml"},
		Exec:              execute,
		ConnectionOptions: ansiblePlaybookConnectionOptions,
		Options:           ansiblePlaybookOptions,
		StdoutCallback:    "yaml",
	}

	signalChan := make(chan os.Signal, 1)
	ctx, cancel := context.WithCancel(context.Background())

	signal.Notify(signalChan, os.Interrupt)
	defer func() {
		signal.Stop(signalChan)
		cancel()
	}()

	go func() {
		select {
		case <-signalChan:
			cancel()
		case <-ctx.Done():
		}
	}()

	err = playbook.Run(ctx)
	if err != nil {
		return err
	}

	return nil
}

func runVagrant(monitor *me.MonitorContext, attack *as.AttackSchema, jsonMachine []byte) ([]byte, *vagrant.VagrantClient, bool) {
	var machine md.Machine_definitions
	err := json.Unmarshal(jsonMachine, &machine)
	if err != nil {
		log.Error("Error: ", err)
	}
	log.Info(machine)

	me.SendAndIncreaseMonitorEvent(monitor, "Creating Vagrant Client")
	vagrantClient, err := vagrant.NewVagrantClient(vagrant_base_dir)
	if err != nil {
		me.SendMonitorError(monitor, err)
		return generateError(err), nil, false
	}

	me.SendAndIncreaseMonitorEvent(monitor, "Obtaining Vagrant Context")
	stat := vagrantClient.GlobalStatus()
	stat.Prune = true
	err = stat.Run()
	if err != nil {
		log.Println("Error getting status, reason:", err)
		monitor.Error = err
		me.SendMonitorError(monitor, err)
		return generateError(err), nil, false
	}
	var found bool = false
	for _, element := range stat.Status {
		if element.Name == machine.MachineName {
			if element.State == "shutoff" {
				me.SendAndIncreaseMonitorEvent(monitor, "Starting Vagrant instance and running the attack")
				err := startVirtualMachine(vagrantClient)
				if err != nil {
					log.Println("Error starting VM, reason:", err)
					monitor.Error = err
					me.SendAndIncreaseMonitorEvent(monitor, err.Error())
					return generateError(err), nil, false
				}
			}
			found = true
			break
		}
	}
	if !found {
		me.SendAndIncreaseMonitorEvent(monitor, "Creating Vagrant instance and running the attack, this may take a while...")
		err := startVirtualMachine(vagrantClient)
		if err != nil {
			log.Println("Error starting VM, reason:", err)
			monitor.Error = err
			me.SendMonitorError(monitor, err)
			return generateError(err), nil, false
		}
	}

	me.SendAndIncreaseMonitorEvent(monitor, "Machine running, provisioning with ansible...")

	err = provisionVM(vagrantClient, monitor, machine)
	if err != nil {
		me.SendMonitorError(monitor, err)
		return generateError(err), nil, false
	}

	me.SendAndIncreaseMonitorEvent(monitor, "Preparing to retrieve results...")
	cmd_ssh := vagrantClient.SSHConfig()
	cmd_ssh.Run()
	sshpath := cmd_ssh.Configs[machine.MachineName].IdentityFile

	res, _ := retrieve_file(monitor, sshpath, attack, cmd_ssh.Configs[machine.MachineName].HostName)
	return res, vagrantClient, machine.Persistent
}

func retrieve_file(monitor *me.MonitorContext, path string, attack *as.AttackSchema, machine_ip string) ([]byte, error) {
	me.SendAndIncreaseMonitorEvent(monitor, "Retrieving results")
	config, _ := auth.PrivateKey("vagrant", path, ssh.InsecureIgnoreHostKey())
	client, err := ssh.Dial("tcp", machine_ip+":22", &config)

	if err != nil {
		log.Println("Couldn't establish a connection to the remote server ", err)
		monitor.Error = err
		me.SendAndIncreaseMonitorEvent(monitor, "Couldn't establish a connection to the remote server")
		return generateError(err), err
	}

	defer client.Close()

	for i := 0; i < len(attack.CliCommands); i++ {
		if i == 0 {
			err := os.Mkdir(results_dir+"/attack_"+attack.Id, 0755)
			if err != nil {
				log.Println("Warning: Could not create XML folder, reason: ", err)
				return generateError(err), err
			}
		}

		if attack.CliCommands[i].OutputType == "XML" {
			err = os.Mkdir(results_dir+"/attack_"+attack.Id+"/XML", 0755)
			if err != nil {
				log.Println("Warning: Could not create XML folder, reason: ", err)
				return generateError(err), err
			}

			f, err := os.OpenFile(results_dir+"/attack_"+attack.Id+"/XML/"+attack.CliCommands[i].OptionsId+"_xml",
				os.O_RDWR|os.O_CREATE, 0777)
			if err != nil {
				log.Println("Warning: Could not open the output file")
				return generateError(err), err
			}

			defer f.Close()

			err = getFile(client, "/home/vagrant/attack_"+attack.Id+"/"+attack.CliCommands[i].OptionsId+"_xml",
				results_dir+"/attack_"+attack.Id+"/XML/"+attack.CliCommands[i].OptionsId+"_xml")

			if err != nil {
				log.Println("Copy failed from remote, reason: ", err)
				return generateError(err), err
			}
		} else if attack.CliCommands[i].OutputType == "JSON" {
			log.Println("ESTOY ENTRANDO EN JSON")
			err = os.Mkdir(results_dir+"/attack_"+attack.Id+"/JSON", 0755)
			if err != nil {
				log.Println("Could not create JSON folder, reason: ", err)
				return generateError(err), err
			}

			f, err := os.OpenFile(results_dir+"/attack_"+attack.Id+"/JSON/"+attack.CliCommands[i].OptionsId+"_json",
				os.O_RDWR|os.O_CREATE, 0777)
			if err != nil {
				log.Println("Could not open the output file, reason: ", err)
				return generateError(err), err
			}

			defer f.Close()

			err = getFile(client, "/home/vagrant/attack_"+attack.Id+"/"+attack.CliCommands[i].OptionsId+"_json",
				results_dir+"/attack_"+attack.Id+"/JSON/"+attack.CliCommands[i].OptionsId+"_json")

			if err != nil {
				log.Println("Copy failed from remote, reason: ", err)
				return generateError(err), err
			}
		} else if attack.CliCommands[i].OutputType == "ZIP" {
			// Crear la ruta del directorio donde se guardará el archivo ZIP
			zipDirPath := filepath.Join(results_dir, "attack_"+attack.Id, "ARCHIVE", "ZIP")

			// Intentar crear el directorio si no existe
			err := os.MkdirAll(zipDirPath, 0755)
			if err != nil {
				log.Println("Could not create ZIP directory, reason: ", err)
				return generateError(err), err
			}

			// Construir la ruta del archivo ZIP
			zipFilePath := filepath.Join(zipDirPath, attack.CliCommands[i].OptionsId+"_zip.zip")

			// Usar getFile para descargar el archivo ZIP desde el servidor remoto
			err = getFile(client, "/home/vagrant/attack_"+attack.Id+"/"+attack.CliCommands[i].OptionsId+"_zip.zip", zipFilePath)
			if err != nil {
				log.Println("Error downloading ZIP file, reason: ", err)
				return generateError(err), err
			}

			// Log exitoso si no hay errores
			log.Println("ZIP file downloaded successfully to:", zipFilePath)
		}

		err = os.Mkdir(results_dir+"/attack_"+attack.Id+"/RAW", 0755)
		if err != nil {
			log.Println("Could not create RAW folder: ", err)
			return generateError(err), err
		}

		_, err = os.OpenFile(results_dir+"/attack_"+attack.Id+"/RAW/"+attack.CliCommands[i].OptionsId+"_output_"+strconv.Itoa(i+1),
			os.O_RDWR|os.O_CREATE, 0777)
		if err != nil {
			log.Println("Could not open the output file: ", err)
			return generateError(err), err
		}

		err = getFile(client, "/home/vagrant/attack_"+attack.Id+"/"+attack.CliCommands[i].OptionsId+"_output_"+strconv.Itoa(i+1),
			results_dir+"/attack_"+attack.Id+"/RAW/"+attack.CliCommands[i].OptionsId+"_output_"+strconv.Itoa(i+1))
		if err != nil {
			log.Println("Copy failed from remote", err)
			return generateError(err), err
		}
	}

	err = os.Mkdir(results_dir+"/attack_"+attack.Id+"/TIME", 0755)
	if err != nil {
		log.Println("Could not create TIME folder")
		return generateError(err), err
	}

	_, err = os.OpenFile(results_dir+"/attack_"+attack.Id+"/TIME/start_time", os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		log.Println("Could not open the start_time file")
		return generateError(err), err
	}

	err = getFile(client, "/home/vagrant/attack_"+attack.Id+"/start_time", results_dir+"/attack_"+attack.Id+"/TIME/start_time")
	if err != nil {
		log.Println("Copy failed from remote", err)
		return generateError(err), err
	}

	start, err := os.ReadFile(results_dir + "/attack_" + attack.Id + "/TIME/start_time")
	if err != nil {
		log.Println("Could not read start_time file.")
		return generateError(err), err
	}

	_, err = os.OpenFile(results_dir+"/attack_"+attack.Id+"/TIME/end_time", os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		log.Println("Could not open the end_time file")
		return generateError(err), err
	}

	err = getFile(client, "/home/vagrant/attack_"+attack.Id+"/end_time", results_dir+"/attack_"+attack.Id+"/TIME/end_time")
	if err != nil {
		log.Println("Copy failed from remote", err)
		return generateError(err), err
	}

	end, err := os.ReadFile(results_dir + "/attack_" + attack.Id + "/TIME/end_time")
	if err != nil {
		log.Println("Could not read end_time file.")
		return generateError(err), err
	}

	me.SendAndIncreaseMonitorEvent(monitor, "Creating final results")
	return CreateResultSchema(attack, string(start), string(end))
}
