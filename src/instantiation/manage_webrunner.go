package main

import (
	as "helpers/AttackSchema"
	me "helpers/MonitorEvents"
	"os"
	"strconv"
	"strings"
	"time"
	"vagrant-mod/Logs"
	"vagrant-mod/Requests"

	log "github.com/sirupsen/logrus"
)

type CommandLineInput struct {
	Type  string            `json:"type"`
	Flags map[string]string `json:"flags"`
}

func performAttackWebRunner(attack *as.AttackSchema, monitor *me.MonitorContext) ([]byte, error) {
	var err error

	if attack.CliCommands[0].Flags.Delimiter == "" {
		// Use old string format
		err = formatStringForWebRunner(attack)
	} else {
		// Use new JSON format
		err = formatJsonForWebRunner(attack)
	}

	if err != nil {
		log.Printf("Error: %s\n", err.Error())
		return []byte{}, err
	}

	currentTime := time.Now()
	start := currentTime.Format("2006/01/02 15-04-05.000000")

	Logs.AddLogEntryToAttack(attack, "Webrunner Attack "+attack.AttackName+" started at "+start)
	responseBody, err := Requests.MakeGenericHTTPRequest(attack)
	currentTime = time.Now()
	end := currentTime.Format("2006/01/02 15-04-05.000000")
	if err != nil {
		Logs.AddLogEntryToAttack(attack, "Error: "+err.Error())
		log.Error("Error: " + err.Error())
		return []byte{}, err
	}
	Logs.AddLogEntryToAttack(attack, "Webrunner Attack "+attack.AttackName+" finished "+end)
	return trigger_webrunner_file(attack, responseBody, start, end)
}

func trigger_webrunner_file(attack *as.AttackSchema, out_file []byte, start string, end string) ([]byte, error) {
	err := os.MkdirAll(results_dir+"/attack_"+attack.Id, 0755)
	if err != nil {
		msg := "Error: Could not create XML folder, reason: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	err = os.MkdirAll(results_dir+"/attack_"+attack.Id+"/JSON", 0755)
	if err != nil {
		msg := "Could not create JSON folder, reason: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	f, err := os.OpenFile(results_dir+"/attack_"+attack.Id+"/JSON/"+attack.CliCommands[0].OptionsId+"_json",
		os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		msg := "Could not open the output file, reason: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	defer f.Close()

	_, err = f.Write(out_file)
	if err != nil {
		msg := "Could not write data to the output file, reason: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	err = os.MkdirAll(results_dir+"/attack_"+attack.Id+"/RAW", 0755)
	if err != nil {
		msg := "Could not create RAW folder: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	f, err = os.OpenFile(results_dir+"/attack_"+attack.Id+"/RAW/"+attack.CliCommands[0].OptionsId+"_output_"+strconv.Itoa(1),
		os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		msg := "Could not open the output file: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	defer f.Close()

	_, err = f.Write(out_file)
	if err != nil {
		msg := "Could not write data to the output file, reason: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	err = os.MkdirAll(results_dir+"/attack_"+attack.Id+"/TIME", 0755)
	if err != nil {
		msg := "Could not create TIME folder"
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	f, err = os.OpenFile(results_dir+"/attack_"+attack.Id+"/TIME/start_time", os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		msg := "Could not open the start_time file"
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}
	defer f.Close()

	_, err = f.WriteString(start)
	if err != nil {
		msg := "Could not write data to the start file, reason: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	f, err = os.OpenFile(results_dir+"/attack_"+attack.Id+"/TIME/end_time", os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		msg := "Could not open the end_time file"
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}
	defer f.Close()

	_, err = f.WriteString(end)
	if err != nil {
		msg := "Could not write data to the end file, reason: " + err.Error()
		Logs.AddLogEntryToAttack(attack, msg)
		log.Error(msg)
		return []byte{}, err
	}

	return CreateResultSchema(attack, start, end)
}

func formatStringForWebRunner(attack *as.AttackSchema) error {
	options := attack.CliCommands[0].Options
	response, err := OptionsParser(options)

	if err != nil {
		log.Printf("Error: %s\n", err.Error())
		return err
	}

	for key, value := range response {
		// Replace in the Url field
		attack.Webrunner[0].Url = strings.Replace(attack.Webrunner[0].Url, key, value, -1)

		// Replace in the Type field
		attack.Webrunner[0].Type = strings.Replace(attack.Webrunner[0].Type, key, value, -1)

		// Replace in the QueryParams
		for key1 := range attack.Webrunner[0].QueryParams {
			attack.Webrunner[0].QueryParams[key1] = strings.Replace(attack.Webrunner[0].QueryParams[key1], key, value, -1)
		}

		// Replace in the Headers
		for key1 := range attack.Webrunner[0].Headers {
			attack.Webrunner[0].Headers[key1] = strings.Replace(attack.Webrunner[0].Headers[key1], key, value, -1)
		}
	}
	return nil
}

func formatJsonForWebRunner(attack *as.AttackSchema) error {
	arguments := attack.CliCommands[0].Flags

	for key, value := range arguments.Flags {
		// Replace in the Url field
		attack.Webrunner[0].Url = strings.Replace(attack.Webrunner[0].Url, "{"+key+"}", value, -1)

		// Replace in the Type field
		attack.Webrunner[0].Type = strings.Replace(attack.Webrunner[0].Type, "{"+key+"}", value, -1)

		// Replace in the QueryParams
		for key1 := range attack.Webrunner[0].QueryParams {
			attack.Webrunner[0].QueryParams[key1] = strings.Replace(attack.Webrunner[0].QueryParams[key1], "{"+key+"}", value, -1)
		}

		// Replace in the Headers
		for key1 := range attack.Webrunner[0].Headers {
			attack.Webrunner[0].Headers[key1] = strings.Replace(attack.Webrunner[0].Headers[key1], "{"+key+"}", value, -1)
		}
	}
	return nil
}
