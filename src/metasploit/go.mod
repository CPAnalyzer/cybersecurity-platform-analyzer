module Metasploit

go 1.20

require (
	github.com/fatih/structs v1.1.0 // indirect
	github.com/hupe1980/gomsf v0.0.7 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect

)
