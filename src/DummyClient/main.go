package main

import (
	"encoding/json"
	"flag"
	"fmt"
	as "helpers/AttackSchema"
	"helpers/DatabaseHelper"
	"helpers/GeneralHelper"
	"helpers/OptionsFile"
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"

	"gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
)

func main() {
	var pulsar_host *string
	var pulsar_port *int
	var action *string
	var creds *string
	var log_file *string
	var possible_actions = []string{"list", "postprocess", "details", "fix", "option"}

	default_log := "/tmp/dummyclient.log"

	pulsar_host = flag.String("pulsar", "localhost", "Pulsar host: e.g. 192.168.122.168")
	pulsar_port = flag.Int("pulsar-port", 6650, "Pulsar port: e.g. 6650")
	creds = flag.String("credentials", "", "JSON file with the credentials")
	log_file = flag.String("log", default_log, "Log file to generate")
	action = flag.String("action", "search", "Action to perform: list, postprocess, details, fix")
	flag.Parse()
	parameters := flag.Args()

	// open a file
	f, err := os.OpenFile(*log_file, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		fmt.Printf("error opening file: %v", err)
	}

	// don't forget to close it
	defer f.Close()

	// Output to stderr instead of stdout, could also be a file.
	log.SetOutput(f)

	pulsar_port_str := strconv.Itoa(*pulsar_port)
	client := PulsarLib.InitClient("pulsar://" + *pulsar_host + ":" + pulsar_port_str)
	defer (*client.Client).Close()

	found := false
	for _, ac := range possible_actions {
		if ac == *action {
			found = true
			break
		}
	}
	if !found {
		log.Println("Invalid action")
		return
	}

	session_id, err := GeneralHelper.LoginWithCredentialFile(client, *creds)
	if err != nil {
		log.Println("Error logging in: ", err)
		return
	}

	if *action == "details" {
		if len(parameters) != 1 {
			log.Println("Usage: details <attack_id>")
			return
		}
		attack_id := parameters[0]
		attack := fetchAttack(client, session_id, attack_id)
		printAttack(client, session_id, attack)
	}

	if *action == "list" {
		attack_list := fetchAttacks(client, session_id)
		listResults(attack_list)
	}

	if *action == "postprocess" {
		if len(parameters) != 1 {
			log.Println("Usage: postprocess <attack_id>")
			return
		}
		attack_id := parameters[0]
		result := postProcessResult(client, session_id, attack_id)
		result_id := result.ID
		result.ID = ""
		result_raw, _ := json.Marshal(result)
		DatabaseHelper.UpdateOneElementByKey(session_id, &client, map[string]interface{}{"_id": result_id}, "", string(result_raw))
	}
	if *action == "fix" {
		attack_list := fetchAttacks(client, session_id)
		fixAttacks(client, session_id, attack_list)
	}

	if *action == "option" {
		if len(parameters) != 1 {
			log.Println("Usage: option <option_id>")
			return
		}
		option_id := parameters[0]
		query := make(map[string]interface{})
		query["session_id"] = session_id
		query["optionsId"] = option_id
		res := PulsarLib.BuildMessage(query)

		option := PulsarLib.SendRequestSync(client, "ui-db.getOptions", res)
		var option_map OptionsFile.OptionsFile
		json.Unmarshal(option, &option_map)
		fmt.Println(option_map)
	}
	GeneralHelper.Logout(client, session_id)
}

func fixAttacks(client PulsarLib.PL_Client, session_id int, attack_list []as.Results) {
	for _, ar := range attack_list {
		attack := fetchAttack(client, session_id, ar.Id)
		if attack.ResultsId != "" {
			// Get the Result
			results, err := DatabaseHelper.GetEntryByID(session_id, &client, attack.ResultsId)
			if err != nil {
				log.Println("Error getting result: ", err)
				continue
			}

			log.Println("Fixing attack: ", attack.AttackName)
			DatabaseHelper.UpdateOneElementByID(session_id, &client, attack.Id, "results", results)
		}
	}
}

func fetchResult(client PulsarLib.PL_Client, session_id int, attack_id string) map[string]interface{} {
	// Search for all the attacks
	query := make(map[string]interface{})
	query["session_id"] = session_id
	query["attack_id"] = attack_id
	res := PulsarLib.BuildMessage(query)

	result_raw := PulsarLib.SendRequestSync(client, "ui-db.getResult", res)
	var result map[string]interface{}
	json.Unmarshal(result_raw, &result)
	return result
}

func postProcessResult(client PulsarLib.PL_Client, session_id int, attack_id string) as.AttackResult {
	// Search for all the attacks
	query := make(map[string]interface{})
	query["session_id"] = session_id
	query["attack_id"] = attack_id
	res := PulsarLib.BuildMessage(query)

	result_raw := PulsarLib.SendRequestSync(client, "ui-db.getResult", res)
	var result map[string]interface{}
	json.Unmarshal(result_raw, &result)
	result["session_id"] = session_id
	res = PulsarLib.BuildMessage(result)

	attack_postprocessed := PulsarLib.SendRequestSync(client, "attack-db.postProcessResult", res)
	var final_attack as.AttackResult
	json.Unmarshal(attack_postprocessed, &final_attack)

	return final_attack
}

func fetchAttack(client PulsarLib.PL_Client, session_id int, attack_id string) as.AttackSchema {
	// Search for all the attacks
	query := make(map[string]interface{})
	query["session_id"] = session_id
	query["attack_id"] = attack_id
	res := PulsarLib.BuildMessage(query)

	attacks := PulsarLib.SendRequestSync(client, "ui-db.getAttack", res)

	var attack as.AttackSchema
	json.Unmarshal(attacks, &attack)
	return attack
}

func fetchAttacks(client PulsarLib.PL_Client, session_id int) []as.Results {
	// Search for all the attacks
	query := make(map[string]interface{})
	query["session_id"] = session_id
	res := PulsarLib.BuildMessage(query)

	attacks := PulsarLib.SendRequestSync(client, "ui-db.getAttacks", res)

	var attack_list []as.Results
	json.Unmarshal(attacks, &attack_list)
	return attack_list
}

func printAttack(client PulsarLib.PL_Client, session_id int, attack as.AttackSchema) {
	fmt.Printf("%s:\t\"%s\"\n", "Name", attack.AttackName)
	fmt.Printf("%s:\t%s\n", "Tool", attack.TypeAttack)
	fmt.Printf("%s:\t%s\n", "Status", attack.State)
	if attack.State == string(as.Error) {
		fmt.Printf("%s:\t%s\n", "Error", attack.ErrorMsg)
		fmt.Printf("%s:\t%s\n", "Step", attack.StepNum)
	} else if attack.State == string(as.Executed) {
		res := fetchResult(client, session_id, attack.Id)
		fmt.Printf("%s:\t%s\n", "Result", attack.ResultsId)
		fmt.Printf("%s:\n", "Logs")
		logs, ok := res["logs"].([]interface{})
		if !ok || len(logs) == 0 {
			for _, v := range res["logs"].([]string) {
				fmt.Printf("\t%s\n", v)
			}
		}
		commandsResult, ok := res["commandsresult"].([]interface{})
		if !ok || len(commandsResult) == 0 {
			fmt.Println("Commands result is empty or not of expected type")
			return
		}
		firstCommand, ok := commandsResult[0].(map[string]interface{})
		if !ok {
			fmt.Println("First command result is not of expected type")
			return
		}
		fmt.Printf("%s:\t%s\n", "Commands", firstCommand["jsonresults"])
	}
}

func listResults(attack_list []as.Results) {
	for _, attack := range attack_list {
		fmt.Printf("Attack: \"%s\" ID: %s State: \"%s\"\n", attack.Name, attack.Id, attack.State)
	}
}
