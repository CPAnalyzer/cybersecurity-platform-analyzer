package main

import (
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"syscall"

	"confManager"
	lgk "hd-database/libGenKey"
	"hd-database/rawHelpers"

	"gitlab.com/HuntDownUPC/go-modules/db/DBUsers/v2"
	dbu "gitlab.com/HuntDownUPC/go-modules/db/DBUsers/v2"
	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
	"golang.org/x/crypto/ssh/terminal"
)

func main() {
	var env_json *string
	var user_json *string
	var mongo_host *string
	var mongo_port *int
	var username *string
	var password *string
	var err error

	env_json = flag.String("env", "", "Environment json file: e.g. /etc/huntdown/environment.json")
	mongo_host = flag.String("mongo", "", "MongoDB host: e.g. 192.168.122.168")
	mongo_port = flag.Int("mongo-port", 27017, "Mongo port: e.g. 27017")
	user_json = flag.String("users", "", "Users json file: e.g. ./users.json")
	username = flag.String("u", "", "Specify username")
	password = flag.String("p", "", "Specify username")

	flag.Parse()

	confEnvironment := confManager.LoadConfig(*env_json)

	if (*username != "" || *password != "") && *user_json != "" {
		log.Println("Conflict between -u, -p and -users")
		os.Exit(1)
	}

	var no_json bool = false
	if *username != "" {
		var bytePassword []byte
		if *password == "" {
			fmt.Print("Please enter password: ")
			bytePassword, err = terminal.ReadPassword(int(syscall.Stdin))
			if err != nil {
				fmt.Println("Error reading password:", err)
				os.Exit(1)
			}
			*password = string(bytePassword)
		}
		bytePassword = lgk.GenKey(*password)
		*password = hex.EncodeToString(bytePassword)
		no_json = true
	} else if _, err := os.Stat(*user_json); os.IsNotExist(err) {
		println("Error: users.json does not exist")
		os.Exit(1)
	}

	if *mongo_host == "" && confEnvironment.DatabaseHost == "" {
		confEnvironment.DatabaseHost = "localhost"
	} else if *mongo_host != "" {
		confEnvironment.DatabaseHost = *mongo_host
	}

	if confEnvironment.DatabasePort == "" {
		confEnvironment.DatabasePort = strconv.Itoa(*mongo_port)
	}

	var db_conn dbd.DB_Connection
	db_conn = rawHelpers.CreateDBConnection(confEnvironment)

	db_admin, _ := dbw.Connect(db_conn)
	if no_json {
		process_with_input(db_admin, *username, *password)
	} else {
		process_with_json(db_admin, *user_json)
	}

	dbw.Disconnect(db_admin)
}

func process_with_input(db_id dbd.Db_id, username string, pass_hash string) {
	var max int = 0
	// var users []dbd.User

	list, err := DBUsers.GetAllRoles(db_id)
	if err != nil {
		log.Println("Error getting roles:", err)
		os.Exit(1)
	}
	re := regexp.MustCompile(`client(\d+)`)
	for i := 0; i < len(list); i++ {
		matches := re.FindAllStringSubmatch(list[i], -1)
		number, err := strconv.Atoi(matches[0][1])
		if err == nil && number > max {
			max = number
		}
	}
	var new_client string = "client" + strconv.Itoa(max+1)

	var my_role dbd.Role = dbd.Role{Role: "readWrite", Db: new_client}
	new_user := dbd.User{Username: username,
		Password: pass_hash,
		Roles:    []dbd.Role{my_role},
		Client:   new_client,
	}
	res, _ := dbu.UserExists(db_id, new_user.Username)
	if res {
		dbu.DeleteUser(db_id, new_user.Username)
	}
	user_raw, _ := json.Marshal(new_user)
	dbu.Add_one_user_direct(db_id, user_raw)
}

func process_with_json(db_id dbd.Db_id, users_file string) {
	var users []dbd.User

	jsonFile, _ := os.Open(users_file)
	jsonLoaded, _ := io.ReadAll(jsonFile)
	jsonFile.Close()
	json.Unmarshal(jsonLoaded, &users)
	for i := 0; i < len(users); i++ {
		res, _ := dbu.UserExists(db_id, users[i].Username)
		if res {
			dbu.DeleteUser(db_id, users[i].Username)
		}
	}
	dbu.Add_user(db_id, users_file)
}
