package main

import (
	"confManager"
	"flag"
	"hd-database/rawHelpers"
	"log"
	"strconv"

	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
)

const presets_collection = "attacksPresets"
const preset_key = "preset"

func main() {
	var config_file *string
	var presets_file *string
	var mongo_host *string
	var mongo_port *int
	var keep_existing *bool

	config_file = flag.String("config", "/etc/huntdown/environment.json", "Environment file")
	presets_file = flag.String("presets", "./presets.json", "Presets file")
	mongo_host = flag.String("mongo", "", "MongoDB host: e.g. 192.168.122.168")
	mongo_port = flag.Int("mongo-port", 27017, "Mongo port: e.g. 27017")
	keep_existing = flag.Bool("k", false, "Keep existing machines")
	flag.Parse()

	confEnvironment := confManager.LoadConfig(*config_file)
	if *mongo_host == "" && confEnvironment.DatabaseHost == "" {
		confEnvironment.DatabaseHost = "localhost"
	} else if *mongo_host != "" {
		confEnvironment.DatabaseHost = *mongo_host
	}
	if confEnvironment.DatabasePort == "" {
		confEnvironment.DatabasePort = strconv.Itoa(*mongo_port)
	}
	if *keep_existing {
		rawHelpers.SetOverwritePolicy(rawHelpers.Skip)
	} else {
		rawHelpers.SetOverwritePolicy(rawHelpers.Delete)
	}

	db_conn := rawHelpers.CreateDBConnection(confEnvironment)

	session_id, _ := dbw.Connect(db_conn)

	client, err := dbw.GetDBContext(session_id)
	if err != nil {
		log.Fatalf("Could not get database context: %v", err)
	}
	client.Database = rawHelpers.GeneralDB

	client.Collection = presets_collection
	dbw.Change_index(&client, preset_key)

	err = rawHelpers.InsertJSONFileToDB(client, *presets_file, preset_key)

	if err != nil {
		log.Println("Failed to insert preset to general db")
		log.Println(err)
	}

	dbw.Disconnect(session_id)
}
