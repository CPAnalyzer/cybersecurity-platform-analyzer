package main

import (
	"encoding/json"
	DatabaseHelper "helpers/DatabaseHelper"
	GeneralHelper "helpers/GeneralHelper"

	log "github.com/sirupsen/logrus"
	PulsarLib "gitlab.com/HuntDownUPC/go-modules/PulsarLib/v2"
	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

type Secret struct {
	SecretKey   string `json:"secret_key"`
	Description string `json:"description"`
	Value       string `json:"value"`
}

func listSecrets(msg []byte) {
	var session_id int
	var message PulsarLib.MessageResponse
	var err error

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error getting database context ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoDBConnection))
		return
	}
	ctx, err := dbw.GetDBContext(dbd.Db_id(session_id))
	if err != nil {
		var err_msg GeneralHelper.ErrMessage
		err_msg.Success = false
		err_msg.Error = err.Error()
		err_msg.ErrorCode = GeneralHelper.NoDBConnection
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
		return
	}
	var filters []DatabaseHelper.Filter
	filter := DatabaseHelper.Filter{Field: "secret_key", Op: "$exists", Value: true}

	filters = append(filters, filter)

	list, err_msg := localQuery(ctx, filters)
	if !err_msg.Success {
		log.Error("Error in Query")
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
		return
	}
	PulsarLib.SendMessage(*comm_client, message.Id, list)
}

func getSecretByName(msg []byte) {
	var session_id int
	var message PulsarLib.MessageResponse
	var err error
	var secret_key string

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error getting database context ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoDBConnection))
		return
	}

	if val, ok := message.Msg["secret_key"]; ok {
		secret_key = val.(string)
	} else {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	list_map, err_msg := getLocalSecret(session_id, secret_key)
	if !err_msg.Success {
		log.Error("Error in Query")
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
		return
	}
	list, _ := json.Marshal(list_map)
	PulsarLib.SendMessage(*comm_client, message.Id, list)
}

func deleteSecret(msg []byte) {
	var session_id int
	var message PulsarLib.MessageResponse
	var err error
	var secret_key string

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error getting database context ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoDBConnection))
		return
	}

	if val, ok := message.Msg["secret_key"]; ok {
		secret_key = val.(string)
	} else {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}

	success, err_msg := deleteLocalSecret(session_id, secret_key)
	if !err_msg.Success {
		log.Error("Error in Deletion")
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
		return
	}
	res := make(map[string]interface{})
	res["success"] = success
	res_byte, _ := json.Marshal(res)
	PulsarLib.SendMessage(*comm_client, message.Id, res_byte)
}

func addSecret(msg []byte) {
	var session_id int
	var message PulsarLib.MessageResponse
	var err error
	var secret Secret

	json.Unmarshal(msg, &message)

	session_id, err = GeneralHelper.GetSession_id(message.Msg)
	if err != nil {
		log.Error("Error getting database context ", err)
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.NoDBConnection))
		return
	}

	if val, ok := message.Msg["secret"]; ok {
		err = GeneralHelper.MapToStruct(val.(map[string]interface{}), &secret)
		if err != nil {
			log.Error("Error malformed query ", err)
			PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
			return
		}
	} else {
		PulsarLib.SendMessage(*comm_client, message.Id, GeneralHelper.BuildLegacyErrorMessage(err, GeneralHelper.InvalidFormat))
		return
	}
	ids, err_msg := addLocalSecret(session_id, secret)
	if !err_msg.Success {
		log.Error("Error in Insertion")
		GeneralHelper.SendErrorMessage(*comm_client, message.Id, err_msg)
		return
	}
	var byte_ids []byte
	byte_ids, _ = json.Marshal(ids)
	PulsarLib.SendMessage(*comm_client, message.Id, byte_ids)
}
