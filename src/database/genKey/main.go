package main

import (
	"encoding/hex"
	"fmt"
	"os"

	lgk "hd-database/libGenKey"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Error specify passwd")
		os.Exit(-1)
	}
	dk := lgk.GenKey(os.Args[1])
	fmt.Println(hex.EncodeToString(dk))
}
