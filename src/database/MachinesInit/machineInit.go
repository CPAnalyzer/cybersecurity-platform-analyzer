package main

import (
	"confManager"
	"flag"
	"hd-database/rawHelpers"
	"log"
	"strconv"

	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
)

const machine_collection = "Machine_definitions"
const machine_key = "machinename"

func main() {
	var config_file *string
	var machines_file *string
	var mongo_host *string
	var mongo_port *int
	var keep_existing *bool

	config_file = flag.String("config", "/etc/huntdown/environment.json", "Machines file")
	machines_file = flag.String("machines", "./machines.json", "Machines file")
	mongo_host = flag.String("mongo", "", "MongoDB host: e.g. 192.168.122.168")
	mongo_port = flag.Int("mongo-port", 27017, "Mongo port: e.g. 27017")
	keep_existing = flag.Bool("k", false, "Keep existing machines")
	flag.Parse()

	confEnvironment := confManager.LoadConfig(*config_file)
	if *mongo_host == "" && confEnvironment.DatabaseHost == "" {
		confEnvironment.DatabaseHost = "localhost"
	} else if *mongo_host != "" {
		confEnvironment.DatabaseHost = *mongo_host
	}
	if confEnvironment.DatabasePort == "" {
		confEnvironment.DatabasePort = strconv.Itoa(*mongo_port)
	}
	if *keep_existing {
		rawHelpers.SetOverwritePolicy(rawHelpers.Skip)
	} else {
		rawHelpers.SetOverwritePolicy(rawHelpers.Delete)
	}

	db_conn := rawHelpers.CreateDBConnection(confEnvironment)

	session_id, _ := dbw.Connect(db_conn)

	client, err := dbw.GetDBContext(session_id)
	if err != nil {
		log.Fatalf("Could not get database context: %v", err)
	}
	client.Database = rawHelpers.GeneralDB

	client.Collection = machine_collection
	dbw.Change_index(&client, machine_key)

	err = rawHelpers.InsertJSONFileToDB(client, *machines_file, machine_key)

	if err != nil {
		log.Println("Failed to insert machines to general db")
		log.Println(err)
	}

	dbw.Disconnect(session_id)
}
