package classification

import (
	"encoding/json"
	"flag"
	"strconv"

	//"flag"
	"log"
	"os"

	//"strconv"
	"strings"

	"confManager"

	dbw "gitlab.com/HuntDownUPC/go-modules/db/DBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

type Classification struct {
	Name string `json:"name"`
}

func LoadConfig(config_file string) error {
	var err error
	if config_file == "" {
		err = confManager.LoadDefaultConfig()
	} else {
		err = confManager.LoadConfigFromFile(config_file)
	}
	if err != nil {
		return err
	}
	mongo_host := confManager.GetValue("mongo_host")
	pulsar_host := confManager.GetValue("pulsar_host")
	pulsar_port := confManager.GetValue("pulsar_port")
	mongo_port := confManager.GetValue("mongo_port")
	mongo_user := confManager.GetValue("mongo_user")
	mongo_password := confManager.GetValue("mongo_password")

	ConfigEnvironment.DatabaseUsername = mongo_user
	ConfigEnvironment.DatabasePassword = mongo_password
	ConfigEnvironment.DatabaseHost = mongo_host
	ConfigEnvironment.DatabasePort = mongo_port
	ConfigEnvironment.PulsarHost = pulsar_host
	ConfigEnvironment.PulsarPort = pulsar_port
	return nil
}

var ConfigEnvironment confManager.ConfigContext

func FillDBConnection(db_conn *dbd.DB_Connection) {
	db_conn.Host = ConfigEnvironment.DatabaseHost
	db_conn.Username = ConfigEnvironment.DatabaseUsername
	db_conn.Password = ConfigEnvironment.DatabasePassword
	db_conn.Port = ConfigEnvironment.DatabasePort
}

func main() {
	var env_json *string
	var mongo_host *string
	var mongo_port *int

	env_json = flag.String("env", "", "Environment json file: e.g. /etc/huntdown/environment.json")
	mongo_host = flag.String("mongo", "", "MongoDB host: e.g. 192.168.122.168")
	mongo_port = flag.Int("mongo-port", 27017, "Mongo port: e.g. 27017")

	flag.Parse()

	var err error
	err = LoadConfig(*env_json)
	if err != nil {
		log.Println("Error loading config file:", err)
		log.Println("Specify a config file with -env <file>")
		os.Exit(1)
	}

	if *mongo_host == "" && ConfigEnvironment.DatabaseHost == "" {
		ConfigEnvironment.DatabaseHost = "localhost"
	} else if *mongo_host != "" {
		ConfigEnvironment.DatabaseHost = *mongo_host
	}

	if ConfigEnvironment.DatabasePort == "" {
		ConfigEnvironment.DatabasePort = strconv.Itoa(*mongo_port)
	}

	var db_conn dbd.DB_Connection
	FillDBConnection(&db_conn)

	session_id, _ := dbw.Connect(db_conn)
	client, err := dbw.GetDBContext(session_id)

	mapClassification := FindClassification("../../attacks/hping3/hping3-attack.json", "hping3")

	update_DB(mapClassification, client)

	dbw.Disconnect(session_id)
}

// Comented
func FindClassification(file_name string, attack_name string) map[string]interface{} {
	file, err := os.ReadFile(file_name)
	if err != nil {
		log.Println("Error reading JSON file:", err)
	}

	var jsonData []map[string]interface{}

	if err := json.Unmarshal(file, &jsonData); err != nil {
		log.Println("Error unmarshalling JSON:", err)
		return nil
	}

	jsonClassification := make(map[string]interface{})

	for _, data := range jsonData {
		for key, value := range data {
			if strings.Contains(key, "classification") {
				switch v := value.(type) {
				case []interface{}:
					classificationjson := make(map[string]interface{})
					var lowclassjson []map[string]interface{}
					topclassjson := make(map[string]interface{})
					for _, elem := range v {
						var attackClassification []map[string]interface{}
						if reconMap, ok := elem.(map[string]interface{}); ok {
							for innerKey, innerValue := range reconMap {
								if reconnaissanceMaps, ok := innerValue.([]interface{}); ok {
									for _, elem := range reconnaissanceMaps {
										if reconMap, ok := elem.(map[string]interface{}); ok {
											for innerKey, innerValue2 := range reconMap {
												if innerKey == "name" {
													attackClassification = make([]map[string]interface{}, 0)
													attackClassification = append(attackClassification, map[string]interface{}{"name": attack_name})
													classificationjson[innerValue2.(string)] = attackClassification

												}
											}
										}
									}
								} else {
									log.Println("Value is not a slice")
								}
								if innerKey == "name" {
									attackClassification = make([]map[string]interface{}, 0)
									attackClassification = append(attackClassification, map[string]interface{}{"name": attack_name})
									classificationjson[innerValue.(string)] = attackClassification
								} else {
									lowclassjson = make([]map[string]interface{}, 0)
									lowclassjson = append(lowclassjson, classificationjson)
									topclassjson[innerKey] = lowclassjson
								}
							}
						}
					}
					if len(lowclassjson) == 0 {
						jsonClassification["classification"] = classificationjson
					} else {
						jsonClassification["classification"] = topclassjson
					}

				case map[string]interface{}:
					for innerKey, innerValue := range v {
						log.Println("Inner Key:", innerKey, "Inner Value:", innerValue)
					}
				default:
					log.Println("Unsupported type for reconnaissance value")
				}
			}
		}
	}
	log.Println(jsonClassification)
	return jsonClassification
}

func update_DB(mapData map[string]interface{}, client dbd.Db_Context) {
	client.Database = "general"
	client.Collection = "Classification"

	jsonData, err := json.Marshal(mapData)

	if err != nil {
		log.Println("Error converting to byte")
	}

	if insertResult, err := dbw.Insert_document(&client, jsonData); err == nil {
		log.Println("attack description inserted to general db")
		log.Println("Inserted document ID:", insertResult)
	} else {
		log.Println("Failed to insert attack description to general db")
		log.Println(err)
	}
}

func search_classification(client dbd.Db_Context) {

}
