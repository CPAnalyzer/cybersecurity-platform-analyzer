#!/bin/sh
### BEGIN INIT INFO
# Provides:          pulsar-standalone
# Required-Start:    $local_fs $remote_fs $network
# Required-Stop:     $local_fs $remote_fs $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Apache Pulsar Standalone Service
### END INIT INFO

# Adjust these paths according to your setup
PULSAR_USER="root"
PID_FILE="/var/run/pulsar/pulsar.pid"

# Function to start the Pulsar standalone service
start() {
    echo "Starting Apache Pulsar Standalone Service..."
    su -s /bin/sh -c "%APACHE_PULSAR_PREFIX%/bin/pulsar standalone" $PULSAR_USER >> /var/log/pulsar.log 2>&1 &
    echo $! > $PID_FILE
}

# Function to stop the Pulsar standalone service
stop() {
    echo "Stopping Apache Pulsar Standalone Service..."
    if [ -f $PID_FILE ]; then
        kill $(cat $PID_FILE)
        rm $PID_FILE
    else
        echo "Pulsar standalone service is not running."
    fi
}

# Function to check the status of the Pulsar standalone service
status() {
    if [ -f $PID_FILE ]; then
        echo "Apache Pulsar Standalone Service is running."
    else
        echo "Apache Pulsar Standalone Service is not running."
    fi
}

# Function to restart the Pulsar standalone service
restart() {
    stop
    sleep 2
    start
}

# Check the command-line arguments
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        restart
        ;;
    status)
        status
        ;;
    *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
        ;;
esac

exit 0