mongo_params=""
if [ "$MONGO_HOST" != "localhost" ]; then
    mongo_params="mongodb://$MONGO_HOST:$MONGO_PORT/admin"
fi

until mongo --eval "print(\"waiting\")" &> /dev/null; do
    sleep 1
done

output=$(mongo $mongo_params <<EOF
use admin
db.system.users.find({user:'$MONGO_USER'}).count()
EOF
)

if echo "$output" | grep "^admin> 1" > /dev/null; then
    echo "MongoDB properly configured"
else
    echo "Configuring MongoDB..."
    output=$(mongo $mongo_params <<EOF
use admin
db.createUser(
    {
        user: "$MONGO_USER",
        pwd: "$MONGO_PASSWORD",
        roles: [
            { role: "readWrite", db: "test" },
            { role: "read", db: "reporting" }
        ]
    }
)
EOF
    )
fi

echo "MongoDB running successfully!"
