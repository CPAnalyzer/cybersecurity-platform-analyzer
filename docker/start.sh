#!/bin/bash

libvirtd &

service mongodb start
service pulsar-standalone start

export PULSAR_VERSION="2.10.2"
export APACHE_PULSAR_PREFIX="/usr/local/apache-pulsar-$PULSAR_VERSION"

until [[ $($APACHE_PULSAR_PREFIX/bin/pulsar-admin broker-stats destinations 2> /dev/null) ]]
    do
        sleep 1
    done
echo "Pulsar running successfully!"

sleep 30

export HUNTDOWN_PREFIX="/usr/local/huntdown"
$HUNTDOWN_PREFIX/bin/hd-database &
$HUNTDOWN_PREFIX/bin/hd-instantiation &
$HUNTDOWN_PREFIX/bin/hd-attack-manager &

cd src/Interface
npm start &

cd backend
npm start