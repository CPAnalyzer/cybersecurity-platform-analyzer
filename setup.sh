#!/bin/bash

# shellcheck source=variables.sh

OUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $OUR_DIR/setup_scripts/variables.sh
source $OUR_DIR/setup_scripts/compile.sh
source $OUR_DIR/setup_scripts/mongo_funcs.sh
source $OUR_DIR/setup_scripts/vagrant_funcs.sh
source $OUR_DIR/setup_scripts/pulsar_funcs.sh
source $OUR_DIR/setup_scripts/installation_funcs.sh
source $OUR_DIR/setup_scripts/ui.sh

if [ -f "$HUNTDOWN_CONFIG/environment.json" ] ; then
    read -p "We just detected a previous Huntdown installation, do you want to use the environment.json file? [Y/n] " -n 1 -r
    echo ""
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
        get_environment_variables
    fi
fi

INSTALL_MONGO="no"
if [[ -z $MONGO_HOST || "$MONGO_HOST" == "localhost" ]] ; then
    INSTALL_MONGO="yes"
fi

INSTALL_PULSAR="no"
if [[ -z $PULSAR_HOST || "$PULSAR_HOST" == "localhost" ]] ; then
    INSTALL_PULSAR="yes"
fi
 
if [[ "$1" == "-nP" || "$2" == "-nP" ]] ; then
    INSTALL_PULSAR="no"
fi

if [[ -z $1  || ( "$1" != "-b" && "$2" != "-b" ) ]] ; then
    welcome_message
fi

# Mongo and Pulsar are already installed on an EC2 instance, check it is up and running
get_mongo_data
get_pulsar_data

create_base_dirs
copy_base_files
create_user
generate_environment_file

update_packages
vagrant_install
ansible_install

tryInstallNpmAndNode

tryInstallPulsarNodejs

if [ "$INSTALL_MONGO" == "yes" ] ; then
    tryInstallMongo
    prepareMongo
else
    print_ok_string "Skipping Mongo installation as requested"
fi

if [ "$INSTALL_PULSAR" == "yes" ] ; then
    install_pulsar
    configure_pulsar
else
    print_ok_string "Skipping Pulsar installation as requested"
fi

installUI
autostartUI
create_presets
make_all
add_mongo_users
make_install
create_vms
enable_services
start_services
install_check_script
