#!/bin/bash

mkdir -p ~/hd-mongo-db/data

cat > ~/hd-mongo-db/mongo_init.sh << EOF1
#!/bin/bash

mongo="mongosh"
MONGO_USER="user"
MONGO_PASSWORD="password"

    output=\$(\$mongo <<EOF
use admin
db.system.users.find({user:'\$MONGO_USER'}).count()
EOF
)

    if echo "\$output" | grep "^admin> 1" > /dev/null ; then
        echo "Mongodb properly configured"
    else
        echo "Configuring Mongodb..."
        output=\$(\$mongo <<EOF
use admin
db.createUser(
    {
        user:"\$MONGO_USER",
        pwd: "\$MONGO_PASSWORD",
        roles: [{ role: "readWrite", db: "test"},
                { role: "read", db: "reporting"}]
    }
)
EOF
)
    fi
    echo "MongoDB running successfully!"
EOF1