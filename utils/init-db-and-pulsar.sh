#!/bin/bash

MONGO_UNIT_NAME="mongo-database.service"
PULSAR_UNIT_NAME="pulsar-standalone.service"

function previous_setup(){
    echo "Installing Docker..."; 
    sudo apt update && sudo apt install -y docker docker.io; 
    sudo usermod -aG docker $USER;
}

function tryInstallMongo(){
    cat "mongo-db/systemd/$MONGO_UNIT_NAME.in" | sudo tee "/lib/systemd/system/$MONGO_UNIT_NAME" > /dev/null

    echo Preparing Mongo DB
    ./mongo-db/docker_run_mongo.sh

    echo "Starting Mongo DB..."
    sudo systemctl enable "$MONGO_UNIT_NAME"
    sudo systemctl start "$MONGO_UNIT_NAME"
}

function tryInstallPulsar () {
    cat "pulsar-standalone/systemd/$PULSAR_UNIT_NAME.in" | sudo tee "/lib/systemd/system/$PULSAR_UNIT_NAME" > /dev/null

    echo "Starting Pulsar..."
    sudo systemctl enable "$PULSAR_UNIT_NAME"
    sudo systemctl start "$PULSAR_UNIT_NAME"

    #Pulsar healthcheck
    until [[ $(sudo docker exec pulsar-service /pulsar/bin/pulsar-admin broker-stats destinations 2> /dev/null) ]]
        do
            sleep 1
        done
    echo "Pulsar running successfully!"
}

previous_setup
tryInstallMongo
tryInstallPulsar